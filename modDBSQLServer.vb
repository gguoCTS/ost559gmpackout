Imports System.Data.SqlClient
Imports System.Configuration

Public Class modDBSQLServer
    Private mConnection As SqlConnection

    'Used By Replace Part to find serial number in carton  16 MAY 2016  AMC
    Function LookupScannedPartByCartonAndSerialNumber(ByVal CartonNumber As String,
                                                      ByVal SerialNumber As String
                                                      ) As DataTable

        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name
        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dbDataTable As New DataTable

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_LkpScannedPartByCartonAndSerialNumber", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@cartonNumber", CartonNumber))
            dbcommand.Parameters.Add(New SqlParameter("@serialNumber", SerialNumber))
            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try

        Return dbDataTable

    End Function

    Function LookupSerialNumberinScannedParts(ByRef SerialNumber As String, ByRef DT As DataTable) As String
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dbDataTable As New DataTable
        Dim strData As String

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_LookupSerialNumberScanned", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Serial", SerialNumber))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

            strData = dbDataTable.Rows.Count
            If strData = "1" Then
                strData = "Found"
            Else
                strData = "Not Found"
            End If

            DT = dbDataTable

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try

        Return strData

    End Function

    Function LookupScannedPartsbyBOM(ByRef BOM As String) As DataTable
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dbDataTable As New DataTable

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_LookupBOMNumberScanned", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Bom", BOM))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If


            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)


            Return dbDataTable

        Catch ex As SqlException
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return Nothing
        Catch ex As Exception
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return Nothing

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try



    End Function

    Function LookupBoxDatabyScannedPart(ByRef SerialNumber As String) As DataTable
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dbDataTable As New DataTable

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_LookupBoxDatabyScannedPart", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@SerialNumber", SerialNumber))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

            Return dbDataTable

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try
    End Function

    Function LookupPartialBoxDatabyBoxSerial(ByRef BoxSerialNumber As String) As DataTable
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dbDataTable As New DataTable

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_LookupPartialBoxDatabyBoxSerial", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@BoxSerialNumber", BoxSerialNumber))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

            Return dbDataTable

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try

    End Function

    Function LookupCompleteBoxDatabyBoxSerial(ByRef BoxSerialNumber As String) As DataTable
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dbDataTable As New DataTable

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_LookupCompleteBoxDatabyBoxSerial", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@BoxSerialNumber", BoxSerialNumber))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

            Return dbDataTable

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try

    End Function

    Function GetLastCartonNumber() As String
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dbDataTable As New DataTable

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_GetLastCartonNumber", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

            Return dbDataTable.Rows(0).Item("CARTONNUM").ToString

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try
    End Function

    Sub UpdateLastCartonNumber(ByVal strNumber As String)
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name
        Dim dbcommand As SqlCommand = Nothing
        ' Dim dbAdpt As SqlDataAdapter
        Dim dbDataTable As New DataTable

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_UpdateLastCartonNumber", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@CartonNumber", strNumber))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbcommand.ExecuteNonQuery()

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try
    End Sub

    Function FindAllPartialCartons() As DataTable
        'Finds any partials for this BOM
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable
        Dim dbAdpt As SqlDataAdapter

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_FindAllPartialCartons", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

            Return dbDataTable

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)
        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try
    End Function

    Function FindPartialCarton(ByVal BOM As String) As DataTable
        'Finds any partials for this BOM
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable
        Dim dbAdpt As SqlDataAdapter

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_FindPartialCarton", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@BOM", BOM))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try

        Return dbDataTable


    End Function

    Function FindPartialCartonByNumber(ByVal BoxNumber As String) As DataTable
        'Finds any partials for this BOM
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable
        Dim dbAdpt As SqlDataAdapter

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_FindPartialCartonByNumber", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@SN", BoxNumber))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try

        Return dbDataTable

    End Function

    Function FindCompleteCartonByNumber(ByVal BoxNumber As String) As DataTable
        'Finds any partials for this BOM
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable
        Dim dbAdpt As SqlDataAdapter

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_FindCompleteCartonByNumber", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@SN", BoxNumber))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try

        Return dbDataTable

    End Function

    Function FindShippedCartonByNumber(ByVal BoxNumber As String) As DataTable
        'Finds any partials for this BOM

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable
        Dim dbAdpt As SqlDataAdapter

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_FindShippedCartonByNumber", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@SN", BoxNumber))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If


            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

            Return dbDataTable

        Catch ex As SqlException
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return Nothing

        Catch ex As Exception
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return Nothing

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try
    End Function

    Function FindCompleteCartonByNumberWithoutWaybill(ByVal BoxNumber As String) As DataTable
        'Finds any partials for this BOM
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable
        Dim dbAdpt As SqlDataAdapter

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_FindCompleteCartonByNumberWithoutWaybill", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@SN", BoxNumber))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

            Return dbDataTable

        Catch ex As SqlException
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return Nothing

        Catch ex As Exception
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return Nothing

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try
    End Function

    Function FindCartonByNumber(ByVal CartonNumber As String) As DataTable
        'Finds any partials for this BOM
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable
        Dim dbAdpt As SqlDataAdapter

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_LkpCartonByNumber", Connection)

            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@cartonNumber", CartonNumber))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try

        Return dbDataTable
    End Function

    Function InsertPartialCarton(ByVal strSerial As String,
            ByVal BOM As String, ByVal Cust_No As String, ByVal CustPartNum As String, ByVal Qty As String,
            ByVal strDateCode As String, ByVal PackTime As Date, ByVal BoxLabelFormat As String,
            ByVal dtScannedParts As DataTable) As Boolean

        'insert carton into carton history
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable
        Dim blntaskComplete As Boolean = False
        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_InsertPartialCarton_New", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Serial", strSerial))
            dbcommand.Parameters.Add(New SqlParameter("@BOM", BOM))
            dbcommand.Parameters.Add(New SqlParameter("@CustPartNum", CustPartNum))
            dbcommand.Parameters.Add(New SqlParameter("@BoxQty", Qty))
            dbcommand.Parameters.Add(New SqlParameter("@DateCode", strDateCode))
            dbcommand.Parameters.Add(New SqlParameter("@PackDate", PackTime))
            dbcommand.Parameters.Add(New SqlParameter("@BoxFormat", BoxLabelFormat))
            dbcommand.Parameters.Add(New SqlParameter("@CustNum", Cust_No))
            dbcommand.Parameters.Add(New SqlParameter("@tblScannedParts", dtScannedParts))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbcommand.ExecuteNonQuery()
            blntaskComplete = True

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try

        Return blntaskComplete
    End Function

    Function InsertCompletedCarton(ByVal Company As String, ByVal strSerial As String,
            ByVal BOM As String, ByVal Cust_No As String, ByVal CustPartNum As String, ByVal Qty As String,
            ByVal strDateCode As String, ByVal PackTime As Date, ByVal BoxLabelFormat As String,
            ByVal dtScannedParts As DataTable
             ) As Boolean

        'insert carton into carton history
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable
        Dim blntaskComplete As Boolean = False

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_InsertFullCarton_New", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Company", Company))
            dbcommand.Parameters.Add(New SqlParameter("@Serial", strSerial))
            dbcommand.Parameters.Add(New SqlParameter("@BOM", BOM))
            dbcommand.Parameters.Add(New SqlParameter("@CustPartNum", CustPartNum))
            dbcommand.Parameters.Add(New SqlParameter("@BoxQty", Qty))
            dbcommand.Parameters.Add(New SqlParameter("@DateCode", strDateCode))
            dbcommand.Parameters.Add(New SqlParameter("@PackDate", PackTime))
            dbcommand.Parameters.Add(New SqlParameter("@BoxFormat", BoxLabelFormat))
            dbcommand.Parameters.Add(New SqlParameter("@CustNum", Cust_No))
            dbcommand.Parameters.Add(New SqlParameter("@tblScannedParts", dtScannedParts))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbcommand.ExecuteNonQuery()

            blntaskComplete = True

        Catch ex As SqlException
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Catch ex As Exception
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try

        Return blntaskComplete
    End Function

    Function InsertCartonModificationHistory(ByVal PartSerial As String, ByVal BOM As String, ByVal CartonNumber As String, ByVal strDescription As String, ByVal ModificationTypeID As Integer) As Boolean
        'insert carton into carton history
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_InsertCartonModificationHistory", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@PartSerialNumber", PartSerial))
            dbcommand.Parameters.Add(New SqlParameter("@BOM", BOM))
            dbcommand.Parameters.Add(New SqlParameter("@ModificationDate", Now))
            dbcommand.Parameters.Add(New SqlParameter("@CartonNumber", CartonNumber))
            dbcommand.Parameters.Add(New SqlParameter("@Description", strDescription))
            dbcommand.Parameters.Add(New SqlParameter("@ModificationTypeID", ModificationTypeID))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbcommand.ExecuteNonQuery()

            Return True

        Catch ex As SqlException
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False

        Catch ex As Exception
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try
    End Function

    Function ReplacePart(ByVal DeleteSerialNumber As String,
                         ByVal InsertSerialNumber As String,
                         ByVal BOM As String,
                         ByVal DeleteFromCartonNumber As String,
                         ByVal InsertFromCartonNumber As String,
                         ByVal strDescription As String,
                         ByVal ModificationTypeID As Integer
                         ) As Boolean
        'insert carton into carton history
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable
        Dim blntaskComplete As Boolean = False

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_ReplacePartInCarton", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@deleteSerialNumber", DeleteSerialNumber))
            dbcommand.Parameters.Add(New SqlParameter("@insertSerialNumber", InsertSerialNumber))
            dbcommand.Parameters.Add(New SqlParameter("@BOM", BOM))
            dbcommand.Parameters.Add(New SqlParameter("@ModificationDate", Now))
            dbcommand.Parameters.Add(New SqlParameter("@deleteFromCartonNumber", DeleteFromCartonNumber))
            If InsertFromCartonNumber Is Nothing Then
                dbcommand.Parameters.Add(New SqlParameter("@insertFromCartonNumber", DBNull.Value))
            Else
                dbcommand.Parameters.Add(New SqlParameter("@insertFromCartonNumber", InsertFromCartonNumber))
            End If
            dbcommand.Parameters.Add(New SqlParameter("@Description", strDescription))
            dbcommand.Parameters.Add(New SqlParameter("@ModificationTypeID", ModificationTypeID))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbcommand.ExecuteNonQuery()
            blntaskComplete = True

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try

        Return blntaskComplete
    End Function

    Function TestLinkedServerConnection(ByVal CurrentConnection As String _
                                        , ByVal LinkedServer As String) As Boolean
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name
        Dim dbcommand As SqlCommand = Nothing
        Dim _returnValue As Boolean = False

        Try
            If ConfigurationManager.AppSettings("ApplicationDevelopmentTesting") = "true" Then
                _returnValue = True
            Else
                Connection = New SqlConnection(CurrentConnection)
                dbcommand = New SqlCommand("dbo.pspPOA_TestSQLServerConnection", Connection)
                dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
                dbcommand.Parameters.Add(New SqlParameter("@linkedServer", LinkedServer))
                Dim param As New SqlParameter("@connected", SqlDbType.Bit)
                param.Direction = ParameterDirection.Output
                dbcommand.Parameters.Add(param)

                If dbcommand.Connection.State <> ConnectionState.Open Then
                    dbcommand.Connection.Open()
                End If

                dbcommand.ExecuteNonQuery()
                _returnValue = dbcommand.Parameters("@connected").Value
            End If

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try

        Return _returnValue
    End Function

    Function MarkFullCartonAsOnAS400(ByVal carton As String) As Boolean
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable
        Dim intRowCount As Int32

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_MarkFullCartonAsOnAS400", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Serial", carton))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            intRowCount = dbcommand.ExecuteNonQuery()

            Return intRowCount

        Catch ex As SqlException
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Catch ex As Exception
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try
    End Function

    Function DecrementCompleteCarton(ByVal Carton As String) As String
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable
        Dim intRowCount As Int32

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_DecrementCompleteCarton", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Serial", Carton))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            intRowCount = dbcommand.ExecuteNonQuery()

            Return intRowCount

        Catch ex As SqlException
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False

        Catch ex As Exception
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try

    End Function

    Function DeleteCompleteCarton(ByVal Carton As String) As String
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable
        Dim intRowCount As Int32

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_DeleteCompleteCarton", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Serial", Carton))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            intRowCount = dbcommand.ExecuteNonQuery()

            Return intRowCount

        Catch ex As SqlException
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Catch ex As Exception
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try

    End Function

    Function DeleteCartonInfoForScannedPartInSCNDPART(ByVal Carton As String, ByVal Serial As String) As String
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable
        Dim intRowCount As Int32

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_DeleteCartonInfoForScannedPartInSCNDPART", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@CartonSerialNumber", Carton))
            dbcommand.Parameters.Add(New SqlParameter("@SerialNumber", Serial))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            intRowCount = dbcommand.ExecuteNonQuery()

            Return intRowCount

        Catch ex As SqlException
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Catch ex As Exception
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try
    End Function

    Function DeleteScannedPartInfoInSCNDPART(ByVal Serial As String) As String
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable
        Dim intRowCount As Int32

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_DeleteScannedPartInfoInSCNDPART", Connection)
            dbcommand.CommandTimeout = 60
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@SerialNumber", Serial))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            intRowCount = dbcommand.ExecuteNonQuery()

            Return intRowCount

        Catch ex As SqlException
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Catch ex As Exception
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try

    End Function

    Function InsertScannedPartInfoInSCNDPART(ByVal PartSerial As String, ByVal BoxSerial As String) As String
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable
        Dim intRowCount As Int32

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_InsertScannedPartInfoInSCNDPART", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@PartSerialNumber", PartSerial))
            dbcommand.Parameters.Add(New SqlParameter("@BoxSerialNumber", BoxSerial))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            intRowCount = dbcommand.ExecuteNonQuery()

            Return intRowCount

        Catch ex As SqlException
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Catch ex As Exception
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try
    End Function

    Function CreatePsuedoWaybill(ByVal Serial As String) As String
        'insert carton into carton history
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable
        Dim intRowCount As Int32

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_InsertPsuedoWaybill", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Serial", LabelData.CartonSerialNumber))


            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            intRowCount = dbcommand.ExecuteNonQuery()

            Return intRowCount

        Catch ex As SqlException
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Catch ex As Exception
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try
    End Function

    Function GetScannedPartsInCarton(ByVal strCartonSerial As String, ByRef dt As DataTable) As Boolean
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name
        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim _returnValue As Boolean = False

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_GetScannedPartsInCarton", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use

            dbcommand.Parameters.Add(New SqlParameter("@CartonSerialNumber", strCartonSerial))
            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dt)
            _returnValue = True

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try

        Return _returnValue
    End Function

    Function DeleteScannedPartsInCarton(ByVal strCartonSerial As String, ByVal strBOM As String) As Boolean
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim blnTaskComplete As Boolean = False

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_DeleteScannedPartsInCarton", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@CartonSerialNumber", strCartonSerial))
            dbcommand.Parameters.Add(New SqlParameter("@bom", strBOM))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbcommand.ExecuteNonQuery()
            blnTaskComplete = True

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try

        Return blnTaskComplete
    End Function

    Function DeleteScannedPartsInCarton(ByVal dtPartsToDelete As DataTable,
                                        ByVal blnReplace As Boolean
                                        ) As Boolean
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim blnTaskComplete As Boolean = False

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_DelScannedParts", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@dtPartsToDelete", dtPartsToDelete))
            dbcommand.Parameters.Add(New SqlParameter("@replace", blnReplace))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbcommand.ExecuteNonQuery()
            blnTaskComplete = True

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try

        Return blnTaskComplete
    End Function

    Function MarkedScannedPartsInCarton(ByVal CartonSerial As String, ByVal Cust_No As String) As Boolean
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_MarkScannedPartsInCarton", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@BOM", BOM))
            dbcommand.Parameters.Add(New SqlParameter("@CustNum", Cust_No))
            dbcommand.Parameters.Add(New SqlParameter("@CartonSerialNumber", CartonSerial))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbcommand.ExecuteNonQuery()

            Return True

        Catch ex As SqlException
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Catch ex As Exception
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try
    End Function

    Function DeletePartialCarton(ByRef BoxNumber As String) As Boolean
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_DeletePartialCarton", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Serial", BoxNumber))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbcommand.ExecuteNonQuery()

            Return True

        Catch ex As SqlException
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Catch ex As Exception
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try
    End Function

    Function DeleteFullCarton(ByRef BoxNumber As String) As Boolean
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_DeleteFullCarton", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Serial", BoxNumber))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbcommand.ExecuteNonQuery()

            Return True

        Catch ex As SqlException
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Catch ex As Exception
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try
    End Function

    Function UnmarkScannedPartsInCarton(ByRef BoxNumber As String) As Boolean
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_UnmarkScannedPartsInCarton", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@CartonSerialNumber", BoxNumber))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbcommand.ExecuteNonQuery()

            Return True

        Catch ex As SqlException
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Catch ex As Exception
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try
    End Function

    Function MoveScannedPartsInCarton(ByRef Dt As DataTable) As Boolean
        'Get scanned parts out of local scanned table and move to server with box number
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_MoveScannedPartsInCarton", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@tblScannedParts", Dt))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbcommand.ExecuteNonQuery()

            Return True

        Catch ex As SqlException
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Catch ex As Exception
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try


    End Function

    Function GetModificationTypes(ByRef dt As DataTable) As Boolean
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_GetModificationTypes", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If


            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dt)
            Return True

        Catch ex As SqlException
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Catch ex As Exception
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try
    End Function

    Function GetPartialByBOMandCustNo(ByVal BOM As String,
                                  ByVal strCurrentCustomerNumber As String
                                  ) As DataTable
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbAdpt As SqlDataAdapter
        Dim dt As New DataTable
        Dim dbcommand As SqlCommand = Nothing
        Dim blntaskComplete As Boolean = False

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_GetPartialByBOMandCustNo", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Bom", BOM))
            dbcommand.Parameters.Add(New SqlParameter("@Cust_No", strCurrentCustomerNumber))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dt)

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try

        Return dt


    End Function

    Function GetPartialByBOMandCarton(ByVal BOM As String,
                                    ByVal strCartonNumber As String,
                                    ByRef blnSendToAS400 As Boolean,
                                    frm As frmPack
                                    ) As DataTable
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbAdpt As SqlDataAdapter
        Dim dt As New DataTable
        Dim dbcommand As SqlCommand = Nothing

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_GetPartialByBOMandCartonNumber", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Bom", BOM))
            dbcommand.Parameters.Add(New SqlParameter("@cartonNumber", strCartonNumber))
            Dim param As New SqlParameter("@sendToAS400", SqlDbType.Bit)
            param.Direction = ParameterDirection.Output
            dbcommand.Parameters.Add(param)

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
      dbAdpt.Fill(dt)
      blnSendToAS400 = dbcommand.Parameters("@sendToAS400").Value
        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try

        Return dt
    End Function

    Function LookupPartialByBOMandCustNo(ByVal BOM As String,
                                         ByVal strCustomerNumber As String
                                        ) As Boolean
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim intPartialCount As Integer = 0
        Dim blnPartialFound As Boolean = False

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_LkpPartialByBOMandCustNo", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Bom", BOM))
            dbcommand.Parameters.Add(New SqlParameter("@Cust_No", strCustomerNumber))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            Int32.TryParse(dbcommand.ExecuteScalar(), intPartialCount)

            If intPartialCount > 0 Then
                blnPartialFound = True
            End If

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try

        Return blnPartialFound


    End Function

    Function LookupPartialByBOMandCartonNumber(ByVal BOM As String,
                                               ByVal strCartonNumber As String
                                              ) As DataTable
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbAdpt As SqlDataAdapter
        Dim dt As New DataTable
        Dim dbcommand As SqlCommand = Nothing
        Dim blntaskComplete As Boolean = False

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_LkpPartialByBOMandCartonNumber", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Bom", BOM))
            dbcommand.Parameters.Add(New SqlParameter("@cartonNumber", strCartonNumber))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dt)

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try

        Return dt

    End Function

    Public Property Connection() As SqlConnection
        Get
            Return mConnection
        End Get
        Set(ByVal value As SqlConnection)
            mConnection = value
        End Set
    End Property

    'Added function 05 MAY 2016  AMC
    Function GetLabelDataByPartNumber(ByVal Company As Short,
                                  ByVal PartNumber As String,
                                  ByVal strCurrentCustomerNumber As String) As DataTable

        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dbDataTable As New DataTable

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.GetLabelDataByPartNum", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Company", Company))
            dbcommand.Parameters.Add(New SqlParameter("@PartNum", PartNumber))
            dbcommand.Parameters.Add(New SqlParameter("@CustNum", strCurrentCustomerNumber))
            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

        Catch ex As SqlException
            Throw New Exception(ex.Message)

        Catch ex As Exception
            Throw New Exception(ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try

        Return dbDataTable
    End Function

    Function GetSystemProfileNames() As DataTable
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dt As New DataTable

        Try
            Connection = New SqlConnection(strDBSQLSERVER)
            dbcommand = New SqlCommand("dbo.pspPOA_GetSystemProfileNames", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dt)

        Catch ex As SqlException
            Throw New Exception(ex.Message)

        Catch ex As Exception
            Throw New Exception(ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try

        Return dt
    End Function

    Public Function GetDynamicParameterSetDataAdapter(ByVal strSystemProfileID As System.Guid) As SqlDataAdapter
        Dim dbCommand As SqlCommand = Nothing

        Dim da As SqlDataAdapter
        Dim pc As SqlParameterCollection
        Dim p As SqlParameter

        dbCommand = New SqlCommand("pspPOA_BOM_LookupSelect", Connection)
        dbCommand.CommandType = CommandType.StoredProcedure
        dbCommand.Parameters.Add(New SqlParameter("@systemProfileID", strSystemProfileID))

        da = New SqlDataAdapter(dbCommand)

        'Insert Command
        da.InsertCommand = New SqlCommand("pspPOA_BOM_LookupInsert", Connection)
        da.InsertCommand.CommandType = CommandType.StoredProcedure
        pc = da.InsertCommand.Parameters
        pc.Add("@bomLookupID", SqlDbType.UniqueIdentifier, 0, "BOM_LookupID")
        pc.Add("@systemProfileID", SqlDbType.UniqueIdentifier, 0, "SystemProfileID")
        pc.Add("@BOM", SqlDbType.NVarChar, 15, "BOM")
        pc.Add("@customerLocation", SqlDbType.NVarChar, 100, "CustomerLocation")
        pc.Add("@customerNumber", SqlDbType.Int, 0, "CustomerNumber")
        pc.Add("@scannedPartNumber", SqlDbType.NVarChar, 15, "ScannedPartNumber")
        pc.Add("@drawingNumber", SqlDbType.NVarChar, 15, "DrawingNumber")
        pc.Add("@active", SqlDbType.Bit, 0, "Active")

        'Update Command
        da.UpdateCommand = New SqlCommand("pspPOA_BOM_LookupUpdate", Connection)
        da.UpdateCommand.CommandType = CommandType.StoredProcedure
        pc = da.UpdateCommand.Parameters
        p = pc.Add("@bomLookupID", SqlDbType.UniqueIdentifier, 0, "BOM_LookupID")
        p.SourceVersion = DataRowVersion.Current
        p = pc.Add("@BOM", SqlDbType.NVarChar, 15, "BOM")
        p.SourceVersion = DataRowVersion.Current
        p = pc.Add("@customerLocation", SqlDbType.NVarChar, 100, "CustomerLocation")
        p.SourceVersion = DataRowVersion.Current
        p = pc.Add("@customerNumber", SqlDbType.Int, 0, "CustomerNumber")
        p.SourceVersion = DataRowVersion.Current
        p = pc.Add("@scannedPartNumber", SqlDbType.NVarChar, 15, "ScannedPartNumber")
        p.SourceVersion = DataRowVersion.Current
        p = pc.Add("@drawingNumber", SqlDbType.NVarChar, 15, "DrawingNumber")
        p.SourceVersion = DataRowVersion.Current
        p = pc.Add("@active", SqlDbType.Bit, 0, "Active")
        p.SourceVersion = DataRowVersion.Current

        'Delete(Command)
        da.DeleteCommand = New SqlCommand("pspPOA_BOM_LookupDelete", Connection)
        da.DeleteCommand.CommandType = CommandType.StoredProcedure
        pc = da.DeleteCommand.Parameters
        p = pc.Add("@bomLookupID", SqlDbType.UniqueIdentifier, 0, "BOM_LookupID")
        p.SourceVersion = DataRowVersion.Original

        Return da

    End Function



    'Function InsertScrapDataRecord(ByRef SerialNumber As String, ByRef BOM As String, _
    '                                        ByRef DateCode As String) As Boolean
    '    'output 'True ' for OK insert or 'False' for issue
    '    Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

    '    Dim dbcommand As SqlCommand = Nothing
    '    Dim dbDataTable As New DataTable

    '    Try
    '        Connection = New SqlConnection(strDBSQLSERVER)
    '        dbcommand = New SqlCommand("dbo.pspPOA_InsertScrapDataRecord", Connection)
    '        dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
    '        dbcommand.Parameters.Add(New SqlParameter("@Serial", SerialNumber))
    '        dbcommand.Parameters.Add(New SqlParameter("@Bom", BOM))
    '        dbcommand.Parameters.Add(New SqlParameter("@DateCode", DateCode))
    '        dbcommand.Parameters.Add(New SqlParameter("@DateScanned", Now))

    '        If dbcommand.Connection.State <> ConnectionState.Open Then
    '            dbcommand.Connection.Open()
    '        End If

    '        dbcommand.ExecuteNonQuery()

    '        Return True

    '    Catch ex As SqlException
    '        Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

    '    Catch ex As Exception
    '        Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

    '    Finally
    '        If Not dbcommand Is Nothing Then
    '            If dbcommand.Connection.State <> ConnectionState.Closed Then
    '                dbcommand.Connection.Close()
    '            End If
    '        End If
    '    End Try
    'End Function

    'Function DecrementPartialCarton(ByVal Carton As String) As String
    '    Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

    '    Dim dbcommand As SqlCommand = Nothing
    '    Dim dbDataTable As New DataTable
    '    Dim intRowCount As Int32

    '    Try
    '        Connection = New SqlConnection(strDBSQLSERVER)
    '        dbcommand = New SqlCommand("dbo.pspPOA_DecrementPartialCarton", Connection)
    '        dbcommand.CommandTimeout = 60
    '        dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
    '        dbcommand.Parameters.Add(New SqlParameter("@Serial", Carton))

    '        If dbcommand.Connection.State <> ConnectionState.Open Then
    '            dbcommand.Connection.Open()
    '        End If

    '        intRowCount = dbcommand.ExecuteNonQuery()

    '        Return intRowCount

    '    Catch ex As SqlException
    '        If dbcommand.Connection.State <> ConnectionState.Closed Then
    '            dbcommand.Connection.Close()
    '        End If
    '        '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
    '        Return False
    '    Catch ex As Exception
    '        If dbcommand.Connection.State <> ConnectionState.Closed Then
    '            dbcommand.Connection.Close()
    '        End If
    '        ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
    '        Return False

    '    Finally
    '        If Not dbcommand Is Nothing Then
    '            If dbcommand.Connection.State <> ConnectionState.Closed Then
    '                dbcommand.Connection.Close()
    '            End If
    '        End If

    '    End Try

    'End Function
End Class

