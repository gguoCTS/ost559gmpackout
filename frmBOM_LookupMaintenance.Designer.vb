<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBOM_LookupMaintenance
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblAssemblyLine = New System.Windows.Forms.Label()
        Me.cboSystemProfiles = New System.Windows.Forms.ComboBox()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.dgBOM = New System.Windows.Forms.DataGridView()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgBOM, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblAssemblyLine)
        Me.Panel1.Controls.Add(Me.cboSystemProfiles)
        Me.Panel1.Controls.Add(Me.btnClose)
        Me.Panel1.Controls.Add(Me.Panel2)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1079, 583)
        Me.Panel1.TabIndex = 1
        '
        'lblAssemblyLine
        '
        Me.lblAssemblyLine.AutoSize = True
        Me.lblAssemblyLine.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAssemblyLine.Location = New System.Drawing.Point(88, 25)
        Me.lblAssemblyLine.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblAssemblyLine.Name = "lblAssemblyLine"
        Me.lblAssemblyLine.Size = New System.Drawing.Size(140, 25)
        Me.lblAssemblyLine.TabIndex = 46
        Me.lblAssemblyLine.Text = "Assembly Line"
        '
        'cboSystemProfiles
        '
        Me.cboSystemProfiles.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSystemProfiles.FormattingEnabled = True
        Me.cboSystemProfiles.Location = New System.Drawing.Point(316, 22)
        Me.cboSystemProfiles.Margin = New System.Windows.Forms.Padding(4)
        Me.cboSystemProfiles.Name = "cboSystemProfiles"
        Me.cboSystemProfiles.Size = New System.Drawing.Size(361, 33)
        Me.cboSystemProfiles.TabIndex = 45
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Location = New System.Drawing.Point(926, 13)
        Me.btnClose.Margin = New System.Windows.Forms.Padding(4)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(137, 51)
        Me.btnClose.TabIndex = 42
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.Controls.Add(Me.dgBOM)
        Me.Panel2.Location = New System.Drawing.Point(4, 72)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1071, 507)
        Me.Panel2.TabIndex = 0
        '
        'dgBOM
        '
        Me.dgBOM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgBOM.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgBOM.Location = New System.Drawing.Point(0, 0)
        Me.dgBOM.Margin = New System.Windows.Forms.Padding(4)
        Me.dgBOM.Name = "dgBOM"
        Me.dgBOM.Size = New System.Drawing.Size(1071, 507)
        Me.dgBOM.TabIndex = 1
        '
        'frmBOM_LookupMaintenance
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1079, 583)
        Me.Controls.Add(Me.Panel1)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmBOM_LookupMaintenance"
        Me.Text = "BOM Lookup Definitions"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.dgBOM, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblAssemblyLine As System.Windows.Forms.Label
    Friend WithEvents cboSystemProfiles As System.Windows.Forms.ComboBox
    Friend WithEvents dgBOM As System.Windows.Forms.DataGridView
End Class
