﻿Imports System.Configuration

Public Class frmReprint

    Private Sub frmReprint_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call UpdateFormLanguage()
    End Sub


    Private Sub txtCarton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCarton.Click
        txtCarton.Clear()
        txtCarton.SelectAll()
        cmdReprint.Enabled = False
    End Sub


    Private Sub txtCarton_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCarton.KeyDown
        Dim DT As DataTable

        If e.KeyCode = Keys.Enter Then
            If Len(txtCarton.Text) > Parts(intCurrentProfile).BoxNumberLength Then
                lblBoxNumber.Text = Mid(txtCarton.Text, txtCarton.Text.Length - 5, 6)
            Else
                lblBoxNumber.Text = txtCarton.Text
            End If
            Try
                blnSqlServerRunning = dbSQLServer.TestLinkedServerConnection(strDBLOCALSQL, ConfigurationManager.AppSettings("SQLServer"))
                If blnSqlServerRunning Then
                    DT = dbSQLServer.FindCompleteCartonByNumber(lblBoxNumber.Text)
                    If DT.Rows.Count = 0 Then
                        'check partial
                        DT = dbSQLServer.LookupPartialBoxDatabyBoxSerial(lblBoxNumber.Text)
                        If DT.Rows.Count = 0 Then
                            DisplayUserMessage(UserMessageType.InputError, dictPhrases("ContainerNotFound"))
                        Else  'check for a partial container
                            lblPartNumber.Text = DT.Rows(0).Item("CustPartNum")
                            lblBOMNum.Text = DT.Rows(0).Item("BOM")
                            BOM = lblBOMNum.Text
                            lblPartQty.Text = DT.Rows(0).Item("BoxQty")
                            intCartonCount = lblPartQty.Text
                            cmdReprint.Enabled = True
                            strDateCode = DT.Rows(0).Item("DateCode")
                        End If
                    Else
                        lblPartNumber.Text = DT.Rows(0).Item("CustPartNum")
                        lblBOMNum.Text = DT.Rows(0).Item("BOM")
                        BOM = lblBOMNum.Text
                        lblPartQty.Text = DT.Rows(0).Item("BoxQty")
                        intCartonCount = lblPartQty.Text
                        cmdReprint.Enabled = True
                        strDateCode = DT.Rows(0).Item("DateCode")
                    End If
                Else
                    DisplayUserMessage(UserMessageType.DatabaseConnectionError, dictPhrases("UnableToReprintLabel") & vbCrLf & dictPhrases("SQLServerOffline"))
                End If

            Catch ex As Exception
                DisplayUserMessage(UserMessageType.ApplicationError, ex.Message)
            End Try
        End If
    End Sub


    Private Sub cmdReprint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdReprint.Click
        Call PrintLabel(False, lblBoxNumber.Text, Val(lblPartQty.Text), BOM, strDateCode)
    End Sub


    Private Sub txtPartFromBox_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPartFromBox.Click
        txtPartFromBox.Clear()
        txtPartFromBox.SelectAll()
        cmdReprint.Enabled = False
    End Sub


    Private Sub txtPartFromBox_KeyUp(sender As Object, e As KeyEventArgs) Handles txtPartFromBox.KeyUp
        Dim strData As String = ""
        Dim bcdate As String = ""
        Dim bcSerial As String = ""
        Dim bcPartNum As String = "" '
        Dim dt As DataTable

        If e.KeyCode = Keys.Enter Then
            strData = txtPartFromBox.Text
            txtPartFromBox.Text = ""
            If strData <> "" Then
                Try
                    blnSqlServerRunning = dbSQLServer.TestLinkedServerConnection(strDBLOCALSQL, ConfigurationManager.AppSettings("SQLServer"))
                    If blnSqlServerRunning Then
                        If Parts(intCurrentProfile).BCSerialLength = strData.Length Then
                            bcSerial = strData
                        Else
                            BOM = DecodeBarcode(Trim(strData), bcdate, bcSerial, bcPartNum)
                        End If
                        dt = dbSQLServer.LookupBoxDatabyScannedPart(bcSerial)
                        If dt.Rows.Count > 0 Then
                            lblBoxNumber.Text = dt.Rows(0).Item("CTNSERNUM")
                            dt = dbSQLServer.FindCompleteCartonByNumber(lblBoxNumber.Text)
                            If dt.Rows.Count = 0 Then 'check for a partial container
                                dt = dbSQLServer.LookupPartialBoxDatabyBoxSerial(lblBoxNumber.Text)
                                If dt.Rows.Count = 0 Then
                                    DisplayUserMessage(UserMessageType.InputError, dictPhrases("ContainerNotFound"))
                                Else
                                    lblPartNumber.Text = dt.Rows(0).Item("CustPartNum")
                                    lblBOMNum.Text = dt.Rows(0).Item("BOM")
                                    BOM = lblBOMNum.Text
                                    lblPartQty.Text = dt.Rows(0).Item("BoxQty")
                                    intCartonCount = lblPartQty.Text
                                    cmdReprint.Enabled = True
                                    strDateCode = dt.Rows(0).Item("DateCode")
                                End If
                            Else
                                lblPartNumber.Text = dt.Rows(0).Item("CustPartNum")
                                lblBOMNum.Text = dt.Rows(0).Item("BOM")
                                BOM = lblBOMNum.Text
                                lblPartQty.Text = dt.Rows(0).Item("BoxQty")
                                intCartonCount = lblPartQty.Text
                                cmdReprint.Enabled = True
                                strDateCode = dt.Rows(0).Item("DateCode")
                            End If
                        Else
                            DisplayUserMessage(UserMessageType.InputError, dictPhrases("ContainerNotFound"))
                        End If
                    End If
                Catch ex As Exception
                    DisplayUserMessage(UserMessageType.ApplicationError, ex.Message)
                End Try
            End If
        End If

    End Sub


    Private Sub UpdateFormLanguage()
        Me.Text = dictPhrases("frmReprint")
        lblScanPart.Text = dictPhrases("lblScanPart")
        lblScanOrTypeBoxNumber.Text = dictPhrases("lblScanOrTypeTheBoxNumber")
        lblBoxInfo.Text = dictPhrases("lblPackingList")
        cmdReprint.Text = dictPhrases("cmdReprint")
        lblBOMNumLabel.Text = dictPhrases("lblBOMNumLabel")
        lblPartNumberLabel.Text = dictPhrases("lblPartNumberLabel")
        lblPartQtyLabel.Text = dictPhrases("lblPartQtyLabel")
        lblBoxNumberLabel.Text = dictPhrases("lblBoxNumberLabel")
    End Sub


    'Private Sub Panel2_Paint(sender As Object, e As PaintEventArgs) Handles Panel2.Paint

    'End Sub

    'Private Sub txtPartFromBox_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs)
    '    Dim BCDate As String = ""
    '    Dim bcSerial As String = ""
    '    Dim DT As DataTable

    '    If e.KeyCode = Keys.Enter Then
    '        pnlBG.BackColor = Color.Gray
    '        'txtSerialNumberScanned.Text = ""
    '        'txtDateCodeScanned.Text = ""
    '        lblPartNumber.Text = ""

    '        Call DecodeBarcode(Trim(txtPartFromBox.Text), BCDate, bcSerial, BOM)

    '        lblPartNumber.Text = BOM
    '        'Lookup box part is in
    '        'TEST LINKED SERVER
    '        DT = dbSQLServer.LookupBoxDatabyScannedPart(bcSerial)
    '        lblBoxNumber.Text = DT.Rows(0).Item("CTNSERNUM")
    '        '*IF NOT FOUND ON SERVER TRY LOCAL DB

    '        DT = dbSQLServer.FindCompleteCartonByNumber(lblBoxNumber.Text)
    '        If DT.Rows.Count = 0 Then
    '            'check partial
    '            DT = dbSQLServer.LookupPartialBoxDatabyBoxSerial(lblBoxNumber.Text)
    '            If DT.Rows.Count = 0 Then

    '                frmMessageBox.Message = strLanguageWordsUsed(intLanguageSelected).p048_BoxNotFound
    '                frmMessageBox.Style = "Ok"
    '                frmMessageBox.Text = strLanguageWordsUsed(intLanguageSelected).p048_BoxNotFound
    '                frmMessageBox.ShowDialog()


    '            Else
    '                lblPartNumber.Text = DT.Rows(0).Item("CustPartNum")
    '                lblBOMNum.Text = DT.Rows(0).Item("BOM")
    '                lblPartQty.Text = DT.Rows(0).Item("BoxQty")
    '                cmdReprint.Enabled = True

    '            End If

    '        Else
    '            lblPartNumber.Text = DT.Rows(0).Item("CustPartNum")
    '            lblBOMNum.Text = DT.Rows(0).Item("BOM")
    '            lblPartQty.Text = DT.Rows(0).Item("BoxQty")
    '            cmdReprint.Enabled = True
    '        End If



    '    End If
    'End Sub
End Class