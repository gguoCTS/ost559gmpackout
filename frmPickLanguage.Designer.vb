﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmPickLanguage
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPickLanguage))
        Me.btnChineseFlag = New System.Windows.Forms.Button()
        Me.btnCzechFlag = New System.Windows.Forms.Button()
        Me.btnUSAFlag = New System.Windows.Forms.Button()
        Me.btnMexFlag = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnChineseFlag
        '
        Me.btnChineseFlag.Image = CType(resources.GetObject("btnChineseFlag.Image"), System.Drawing.Image)
        Me.btnChineseFlag.Location = New System.Drawing.Point(734, 32)
        Me.btnChineseFlag.Name = "btnChineseFlag"
        Me.btnChineseFlag.Size = New System.Drawing.Size(200, 172)
        Me.btnChineseFlag.TabIndex = 48
        Me.btnChineseFlag.UseVisualStyleBackColor = True
        '
        'btnCzechFlag
        '
        Me.btnCzechFlag.Image = CType(resources.GetObject("btnCzechFlag.Image"), System.Drawing.Image)
        Me.btnCzechFlag.Location = New System.Drawing.Point(525, 32)
        Me.btnCzechFlag.Name = "btnCzechFlag"
        Me.btnCzechFlag.Size = New System.Drawing.Size(186, 172)
        Me.btnCzechFlag.TabIndex = 47
        Me.btnCzechFlag.UseVisualStyleBackColor = True
        '
        'btnUSAFlag
        '
        Me.btnUSAFlag.Image = CType(resources.GetObject("btnUSAFlag.Image"), System.Drawing.Image)
        Me.btnUSAFlag.Location = New System.Drawing.Point(28, 32)
        Me.btnUSAFlag.Name = "btnUSAFlag"
        Me.btnUSAFlag.Size = New System.Drawing.Size(186, 172)
        Me.btnUSAFlag.TabIndex = 49
        Me.btnUSAFlag.UseVisualStyleBackColor = True
        '
        'btnMexFlag
        '
        Me.btnMexFlag.Image = CType(resources.GetObject("btnMexFlag.Image"), System.Drawing.Image)
        Me.btnMexFlag.Location = New System.Drawing.Point(262, 32)
        Me.btnMexFlag.Name = "btnMexFlag"
        Me.btnMexFlag.Size = New System.Drawing.Size(186, 172)
        Me.btnMexFlag.TabIndex = 50
        Me.btnMexFlag.UseVisualStyleBackColor = True
        '
        'frmPickLanguage
        '
        Me.ClientSize = New System.Drawing.Size(946, 228)
        Me.Controls.Add(Me.btnMexFlag)
        Me.Controls.Add(Me.btnUSAFlag)
        Me.Controls.Add(Me.btnChineseFlag)
        Me.Controls.Add(Me.btnCzechFlag)
        Me.Name = "frmPickLanguage"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnMexicanFlag As Button
    Friend WithEvents btnAmericanFlag As Button
    Friend WithEvents CzechFlag As Button
    Friend WithEvents ChineseFlag As Button
    Friend WithEvents btnChineseFlag As Button
    Friend WithEvents btnCzechFlag As Button
    Friend WithEvents btnUSAFlag As Button
    Friend WithEvents btnMexFlag As Button
End Class
