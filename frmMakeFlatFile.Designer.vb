﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMakeFlatFile
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnOK = New System.Windows.Forms.Button()
        Me.lblEnterPackoutData = New System.Windows.Forms.Label()
        Me.txtScanData = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'btnOK
        '
        Me.btnOK.Location = New System.Drawing.Point(203, 229)
        Me.btnOK.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.btnOK.Name = "btnOK"
        Me.btnOK.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.btnOK.Size = New System.Drawing.Size(137, 62)
        Me.btnOK.TabIndex = 1
        Me.btnOK.Text = "OK"
        Me.btnOK.UseVisualStyleBackColor = True
        '
        'lblEnterPackoutData
        '
        Me.lblEnterPackoutData.AutoSize = True
        Me.lblEnterPackoutData.Location = New System.Drawing.Point(16, 69)
        Me.lblEnterPackoutData.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblEnterPackoutData.Name = "lblEnterPackoutData"
        Me.lblEnterPackoutData.Size = New System.Drawing.Size(70, 17)
        Me.lblEnterPackoutData.TabIndex = 1
        Me.lblEnterPackoutData.Text = "Scan Part"
        '
        'txtScanData
        '
        Me.txtScanData.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtScanData.Location = New System.Drawing.Point(16, 100)
        Me.txtScanData.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.txtScanData.Name = "txtScanData"
        Me.txtScanData.Size = New System.Drawing.Size(511, 30)
        Me.txtScanData.TabIndex = 0
        '
        'frmMakeFlatFile
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(565, 322)
        Me.Controls.Add(Me.txtScanData)
        Me.Controls.Add(Me.lblEnterPackoutData)
        Me.Controls.Add(Me.btnOK)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "frmMakeFlatFile"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmMakeFlatFile"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnOK As System.Windows.Forms.Button
    Friend WithEvents lblEnterPackoutData As System.Windows.Forms.Label
    Friend WithEvents txtScanData As System.Windows.Forms.TextBox
End Class
