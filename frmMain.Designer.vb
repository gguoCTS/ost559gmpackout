<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.PackPartsButton = New System.Windows.Forms.ToolStripButton()
        Me.PackWithScale = New System.Windows.Forms.ToolStripButton()
        Me.btnRemovePart = New System.Windows.Forms.ToolStripButton()
        Me.ReprintLabelButton = New System.Windows.Forms.ToolStripButton()
        Me.ListPartsButton = New System.Windows.Forms.ToolStripButton()
        Me.Language = New System.Windows.Forms.ToolStripButton()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EditToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PickSiteCustomerPartAndLineToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CreatePackoutFlatFileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BOM_MaintenanceToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SerialPort1 = New System.IO.Ports.SerialPort(Me.components)
        Me.tmrUpdateSQLServer = New System.Windows.Forms.Timer(Me.components)
        Me.lblBOMDisplay = New System.Windows.Forms.Label()
        Me.btnChineseFlag = New System.Windows.Forms.Button()
        Me.btnCzechFlag = New System.Windows.Forms.Button()
        Me.btnMexicanFlag = New System.Windows.Forms.Button()
        Me.btnAmericanFlag = New System.Windows.Forms.Button()
        Me.UnpackPartFromBoxToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClearPartToBoxAssociationToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MarkPartAsScrapToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ClearPartFromDatabaseToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ShipPartialToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AxWindowsMediaPlayer1 = New AxWMPLib.AxWindowsMediaPlayer()
        Me.StatusStrip1.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.AxWindowsMediaPlayer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.ToolStripStatusLabel2})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 564)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Padding = New System.Windows.Forms.Padding(1, 0, 10, 0)
        Me.StatusStrip1.Size = New System.Drawing.Size(1154, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripStatusLabel1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(1, 17)
        Me.ToolStripStatusLabel1.Spring = True
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(1143, 17)
        Me.ToolStripStatusLabel2.Spring = True
        '
        'ToolStrip1
        '
        Me.ToolStrip1.ImageScalingSize = New System.Drawing.Size(64, 64)
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PackPartsButton, Me.PackWithScale, Me.btnRemovePart, Me.ReprintLabelButton, Me.ListPartsButton, Me.Language})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 36)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(1154, 94)
        Me.ToolStrip1.TabIndex = 2
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'PackPartsButton
        '
        Me.PackPartsButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PackPartsButton.Image = CType(resources.GetObject("PackPartsButton.Image"), System.Drawing.Image)
        Me.PackPartsButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.PackPartsButton.Name = "PackPartsButton"
        Me.PackPartsButton.Size = New System.Drawing.Size(96, 91)
        Me.PackPartsButton.Text = "Pack Parts"
        Me.PackPartsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'PackWithScale
        '
        Me.PackWithScale.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PackWithScale.Image = CType(resources.GetObject("PackWithScale.Image"), System.Drawing.Image)
        Me.PackWithScale.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.PackWithScale.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.PackWithScale.Name = "PackWithScale"
        Me.PackWithScale.Size = New System.Drawing.Size(147, 91)
        Me.PackWithScale.Text = "Pack Using Scale"
        Me.PackWithScale.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'btnRemovePart
        '
        Me.btnRemovePart.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRemovePart.Image = CType(resources.GetObject("btnRemovePart.Image"), System.Drawing.Image)
        Me.btnRemovePart.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnRemovePart.Name = "btnRemovePart"
        Me.btnRemovePart.Size = New System.Drawing.Size(116, 91)
        Me.btnRemovePart.Text = "Remove Part"
        Me.btnRemovePart.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ReprintLabelButton
        '
        Me.ReprintLabelButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ReprintLabelButton.Image = CType(resources.GetObject("ReprintLabelButton.Image"), System.Drawing.Image)
        Me.ReprintLabelButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ReprintLabelButton.Name = "ReprintLabelButton"
        Me.ReprintLabelButton.Size = New System.Drawing.Size(122, 91)
        Me.ReprintLabelButton.Text = "Reprint Label"
        Me.ReprintLabelButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'ListPartsButton
        '
        Me.ListPartsButton.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListPartsButton.Image = CType(resources.GetObject("ListPartsButton.Image"), System.Drawing.Image)
        Me.ListPartsButton.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ListPartsButton.Name = "ListPartsButton"
        Me.ListPartsButton.Size = New System.Drawing.Size(87, 91)
        Me.ListPartsButton.Text = "List Parts"
        Me.ListPartsButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'Language
        '
        Me.Language.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Language.Image = CType(resources.GetObject("Language.Image"), System.Drawing.Image)
        Me.Language.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.Language.Name = "Language"
        Me.Language.Size = New System.Drawing.Size(92, 91)
        Me.Language.Text = "Language"
        Me.Language.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageAboveText
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.EditToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(4, 2, 0, 2)
        Me.MenuStrip1.Size = New System.Drawing.Size(1154, 36)
        Me.MenuStrip1.TabIndex = 3
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1, Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(57, 32)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(117, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(120, 32)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'EditToolStripMenuItem
        '
        Me.EditToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PickSiteCustomerPartAndLineToolStripMenuItem, Me.CreatePackoutFlatFileToolStripMenuItem, Me.BOM_MaintenanceToolStripMenuItem})
        Me.EditToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EditToolStripMenuItem.Name = "EditToolStripMenuItem"
        Me.EditToolStripMenuItem.Size = New System.Drawing.Size(61, 32)
        Me.EditToolStripMenuItem.Text = "Edit"
        '
        'PickSiteCustomerPartAndLineToolStripMenuItem
        '
        Me.PickSiteCustomerPartAndLineToolStripMenuItem.Name = "PickSiteCustomerPartAndLineToolStripMenuItem"
        Me.PickSiteCustomerPartAndLineToolStripMenuItem.Size = New System.Drawing.Size(314, 32)
        Me.PickSiteCustomerPartAndLineToolStripMenuItem.Text = "BOM Selection Form"
        '
        'CreatePackoutFlatFileToolStripMenuItem
        '
        Me.CreatePackoutFlatFileToolStripMenuItem.Name = "CreatePackoutFlatFileToolStripMenuItem"
        Me.CreatePackoutFlatFileToolStripMenuItem.Size = New System.Drawing.Size(314, 32)
        Me.CreatePackoutFlatFileToolStripMenuItem.Text = "Create Packout Flat File"
        '
        'BOM_MaintenanceToolStripMenuItem
        '
        Me.BOM_MaintenanceToolStripMenuItem.Name = "BOM_MaintenanceToolStripMenuItem"
        Me.BOM_MaintenanceToolStripMenuItem.Size = New System.Drawing.Size(314, 32)
        Me.BOM_MaintenanceToolStripMenuItem.Text = "BOM Maintenance Form"
        '
        'SerialPort1
        '
        Me.SerialPort1.BaudRate = 115200
        Me.SerialPort1.PortName = "COM4"
        '
        'tmrUpdateSQLServer
        '
        Me.tmrUpdateSQLServer.Interval = 30000
        '
        'lblBOMDisplay
        '
        Me.lblBOMDisplay.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.lblBOMDisplay.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBOMDisplay.Location = New System.Drawing.Point(359, 6)
        Me.lblBOMDisplay.Name = "lblBOMDisplay"
        Me.lblBOMDisplay.Size = New System.Drawing.Size(248, 25)
        Me.lblBOMDisplay.TabIndex = 50
        Me.lblBOMDisplay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnChineseFlag
        '
        Me.btnChineseFlag.Image = CType(resources.GetObject("btnChineseFlag.Image"), System.Drawing.Image)
        Me.btnChineseFlag.Location = New System.Drawing.Point(713, 166)
        Me.btnChineseFlag.Name = "btnChineseFlag"
        Me.btnChineseFlag.Size = New System.Drawing.Size(66, 59)
        Me.btnChineseFlag.TabIndex = 46
        Me.btnChineseFlag.Text = "Button1"
        Me.btnChineseFlag.UseVisualStyleBackColor = True
        Me.btnChineseFlag.Visible = False
        '
        'btnCzechFlag
        '
        Me.btnCzechFlag.Image = CType(resources.GetObject("btnCzechFlag.Image"), System.Drawing.Image)
        Me.btnCzechFlag.Location = New System.Drawing.Point(626, 166)
        Me.btnCzechFlag.Name = "btnCzechFlag"
        Me.btnCzechFlag.Size = New System.Drawing.Size(66, 59)
        Me.btnCzechFlag.TabIndex = 45
        Me.btnCzechFlag.Text = "Button1"
        Me.btnCzechFlag.UseVisualStyleBackColor = True
        Me.btnCzechFlag.Visible = False
        '
        'btnMexicanFlag
        '
        Me.btnMexicanFlag.AutoSize = True
        Me.btnMexicanFlag.Image = CType(resources.GetObject("btnMexicanFlag.Image"), System.Drawing.Image)
        Me.btnMexicanFlag.Location = New System.Drawing.Point(554, 166)
        Me.btnMexicanFlag.Name = "btnMexicanFlag"
        Me.btnMexicanFlag.Size = New System.Drawing.Size(187, 102)
        Me.btnMexicanFlag.TabIndex = 44
        Me.btnMexicanFlag.Text = "Button1"
        Me.btnMexicanFlag.UseVisualStyleBackColor = True
        Me.btnMexicanFlag.Visible = False
        '
        'btnAmericanFlag
        '
        Me.btnAmericanFlag.AutoSize = True
        Me.btnAmericanFlag.Image = CType(resources.GetObject("btnAmericanFlag.Image"), System.Drawing.Image)
        Me.btnAmericanFlag.Location = New System.Drawing.Point(482, 166)
        Me.btnAmericanFlag.Name = "btnAmericanFlag"
        Me.btnAmericanFlag.Size = New System.Drawing.Size(246, 160)
        Me.btnAmericanFlag.TabIndex = 43
        Me.btnAmericanFlag.Text = "Button1"
        Me.btnAmericanFlag.UseVisualStyleBackColor = True
        Me.btnAmericanFlag.Visible = False
        '
        'UnpackPartFromBoxToolStripMenuItem
        '
        Me.UnpackPartFromBoxToolStripMenuItem.Name = "UnpackPartFromBoxToolStripMenuItem"
        Me.UnpackPartFromBoxToolStripMenuItem.Size = New System.Drawing.Size(258, 28)
        Me.UnpackPartFromBoxToolStripMenuItem.Text = "Unpack Part from Box"
        '
        'ClearPartToBoxAssociationToolStripMenuItem
        '
        Me.ClearPartToBoxAssociationToolStripMenuItem.Name = "ClearPartToBoxAssociationToolStripMenuItem"
        Me.ClearPartToBoxAssociationToolStripMenuItem.Size = New System.Drawing.Size(311, 28)
        Me.ClearPartToBoxAssociationToolStripMenuItem.Text = "Clear Part to Box Association"
        '
        'MarkPartAsScrapToolStripMenuItem
        '
        Me.MarkPartAsScrapToolStripMenuItem.Name = "MarkPartAsScrapToolStripMenuItem"
        Me.MarkPartAsScrapToolStripMenuItem.Size = New System.Drawing.Size(311, 28)
        Me.MarkPartAsScrapToolStripMenuItem.Text = "Mark Part as Scrap"
        '
        'ClearPartFromDatabaseToolStripMenuItem
        '
        Me.ClearPartFromDatabaseToolStripMenuItem.Name = "ClearPartFromDatabaseToolStripMenuItem"
        Me.ClearPartFromDatabaseToolStripMenuItem.Size = New System.Drawing.Size(311, 28)
        Me.ClearPartFromDatabaseToolStripMenuItem.Text = "Clear Part from Database"
        '
        'ShipPartialToolStripMenuItem
        '
        Me.ShipPartialToolStripMenuItem.Name = "ShipPartialToolStripMenuItem"
        Me.ShipPartialToolStripMenuItem.Size = New System.Drawing.Size(173, 28)
        Me.ShipPartialToolStripMenuItem.Text = "Ship Partial"
        '
        'AxWindowsMediaPlayer1
        '
        Me.AxWindowsMediaPlayer1.Enabled = True
        Me.AxWindowsMediaPlayer1.Location = New System.Drawing.Point(760, 0)
        Me.AxWindowsMediaPlayer1.Name = "AxWindowsMediaPlayer1"
        Me.AxWindowsMediaPlayer1.OcxState = CType(resources.GetObject("AxWindowsMediaPlayer1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxWindowsMediaPlayer1.Size = New System.Drawing.Size(139, 31)
        Me.AxWindowsMediaPlayer1.TabIndex = 41
        Me.AxWindowsMediaPlayer1.Visible = False
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1154, 586)
        Me.Controls.Add(Me.lblBOMDisplay)
        Me.Controls.Add(Me.btnChineseFlag)
        Me.Controls.Add(Me.btnCzechFlag)
        Me.Controls.Add(Me.btnMexicanFlag)
        Me.Controls.Add(Me.btnAmericanFlag)
        Me.Controls.Add(Me.AxWindowsMediaPlayer1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Packout Main Form"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.AxWindowsMediaPlayer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents AxWindowsMediaPlayer1 As AxWMPLib.AxWindowsMediaPlayer
    Friend WithEvents ReprintLabelButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents EditToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SerialPort1 As System.IO.Ports.SerialPort
    Friend WithEvents PickSiteCustomerPartAndLineToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CreatePackoutFlatFileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tmrUpdateSQLServer As System.Windows.Forms.Timer
    Friend WithEvents Language As ToolStripButton
    Friend WithEvents btnAmericanFlag As Button
  Friend WithEvents btnMexicanFlag As Button
  Friend WithEvents btnCzechFlag As Button
  Friend WithEvents btnChineseFlag As Button
    Friend WithEvents lblBOMDisplay As Label
    Friend WithEvents PackWithScale As System.Windows.Forms.ToolStripButton
    Friend WithEvents PackPartsButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents UnpackPartFromBoxToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClearPartToBoxAssociationToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MarkPartAsScrapToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ClearPartFromDatabaseToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ShipPartialToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnRemovePart As System.Windows.Forms.ToolStripButton
    Friend WithEvents ListPartsButton As System.Windows.Forms.ToolStripButton
    Friend WithEvents BOM_MaintenanceToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
