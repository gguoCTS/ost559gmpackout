Option Strict Off
Option Explicit On
Imports System.Data.SqlClient


Module Public_Renamed
    Public LoginNumber As String
    Public cn As New ADODB.Connection
    Public cn2 As New ADODB.Connection
    Public cn3 As New ADODB.Connection
    Public cn4 As New ADODB.Connection
    Public strDBSQLSERVER As String
    Public strDBAS400 As String 'cn2 US99ERp001
    Public strDBUS473579 As String
    Public strDBLOCALSQL As String
    Public TestMode As String
    Public WAV_Path As String
   
    Public BOM As String
    Public PrintQ As String
    Public BoxType As String

    Public DBCONNECTSQLExp As String


    Public strErrorSound As String
    Public strGoodSound As String
    Public strAlreadyScannedSound As String
    Public strNowPrintingSound As String
    Public strLabelWriterPath As String
    Public strPackoutFlatFile As String
  Public strAreYouSureSound As String
  Public strSqlServerName As String
  Public strRepackStation As String

  Public strLogin As String
    Public intNormalBoxQty As Integer
    Public intSpecialBoxQty As Integer
    Public HLoc As Object
    Public HLocNum As String 'Honda Location Name
    'Public CTSSite As Short
    Public intNumberOfProfiles As Integer
    Public intCurrentProfile As Integer = 1
    Public gstrCurrentProfileID As String 'renamed 04MAY2016  AMC
    Public gStrCurrentProfileName As String
  Public strCurrentBOM As String
  Public strCurrentPartNumber As String
  Public strCurrentCustomerNumber As String
  Public strCurrentDrawingNumber As String

  Public gblTestModeEnabled As Boolean


  Public gIntCurrentBomPosition As Integer

  Structure PartData
    Dim ID As String
    Dim BCStyle As String
    Dim PartEntryMethod As String
    Dim Name As String
    Dim CTSSite As String
    Dim MFGLoc As String
    Dim BCDateStart As Integer
    Dim BCDateLength As Integer
    Dim BCSerialStart As Integer
    Dim BCSerialLength As Integer
    Dim BCPartNumStart As Integer
    Dim BCPartNumLength As Integer
    Dim BoxPrefix As Integer
    Dim BoxNumberLength As Integer
    Dim SampleBarcode As String
    Dim UsesBOMSelectionForm As String
  End Structure

    Public Parts(20) As PartData


    Structure BOMDATA
        Dim CustomerNumber As String
        Dim BOM As String
        Dim ScannedPartNumber As String
        Dim DrawingNumber As String
        Dim SystemProfileID As String
        Dim CustomerLocation As String
    End Structure

  Public BOMList(0) As BOMDATA


  Public Sub SW_ERROR()
        With frmMain.AxWindowsMediaPlayer1
            .URL = WAV_Path & " \" & strErrorSound
            .Ctlcontrols.play()
        End With
    End Sub
    Public Sub Good_Scan()
        With frmMain.AxWindowsMediaPlayer1
            .URL = WAV_Path & " \" & strGoodSound
            .Ctlcontrols.play()
        End With
    End Sub

    Public Sub IAmPrinting()
        With frmMain.AxWindowsMediaPlayer1
            .URL = WAV_Path & " \" & strNowPrintingSound
            .Ctlcontrols.play()
        End With
    End Sub

    Public Sub Already()
        With frmMain.AxWindowsMediaPlayer1
            .URL = WAV_Path & " \" & strAlreadyScannedSound
            .Ctlcontrols.play()
        End With
    End Sub

End Module