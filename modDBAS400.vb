﻿Imports System.Data.SqlClient

Public Class modDBAS400
    Private mConnection As ADODB.Connection
    Private rs As New ADODB.Recordset

    Function GetLabelDefinitions() As DataTable 'ADODB.Recordset
        Dim dbcommand As New ADODB.Command
        Dim dbConnection As New ADODB.Connection

        If dbConnection.State = 0 Then
            dbConnection.ConnectionString = strDBAS400
            dbConnection.Open()
        End If

        Dim dbDataTable As New DataTable
        Dim rs As ADODB.Recordset
        Dim strText As String

        Try
            Connection = New ADODB.Connection
            Connection.ConnectionString = strDBAS400

            If Connection.State <> ConnectionState.Open Then
                Connection.Open()
            End If

            strText = "SELECT * FROM rfgencts.labeldef "
            strText = strText & "WHERE cmpno = " & Parts(intCurrentProfile).CTSSite & " "
            'strText = strText & "AND (Cust_No = 1804 or Cust_No= 4150)"

            '  dbcommand.CommandText = strText
            '  dbcommand.let_ActiveConnection(Connection)

            dbcommand.CommandText = strText
            dbcommand.let_ActiveConnection(Connection)
            rs = dbcommand.Execute

            'dbAdpt = New SqlDataAdapter(dbcommand)
            'dbAdpt.Fill(dbDataTable)


            Return dbDataTable

        Catch ex As SqlException
            If Connection.State <> ConnectionState.Closed Then
                Connection.Close()
            End If
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return Nothing
        Catch ex As Exception
            If Connection.State <> ConnectionState.Closed Then
                Connection.Close()
            End If
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return Nothing

        Finally


        End Try

    End Function

    Function SendCompletedCartonToAS400(ByVal CTSSite As String, ByVal strSerial As String,
            ByVal BOM As String, ByVal CustPartNum As String, ByVal Qty As String,
            ByVal strDateCode As String, ByVal PackTime As Date, ByVal BoxLabelFormat As String,
            ByVal Cust_No As String) As String

        Dim dbcommand As New ADODB.Command

        Dim dbDataTable As New DataTable
        Dim rs As ADODB.Recordset
        Dim strText As String

        Try
            Connection = New ADODB.Connection
            Connection.ConnectionString = strDBAS400

            If Connection.State <> ConnectionState.Open Then
                Connection.Open()
            End If

            'Write Carton Record
            strText = "INSERT INTO RFGENCTS.CARTONS(Company,Plant,Serial,Part_Number,Cust_No,"
            strText = strText & "Cust_Part,Ctn_Qty,Date_Code,Ctn_Status,Upload_Flg,Box_Lbl_Fmt) "
            strText = strText & "VALUES(" & CTSSite & ",0,'" & strSerial & "','" & BOM & "',"
            strText = strText & Cust_No & ",'" & CustPartNum & "'," & Qty & ","
            strText = strText & "'" & Left(strDateCode, 11) & "','P','0','" & BoxLabelFormat & "')"

            dbcommand.CommandText = strText
            dbcommand.let_ActiveConnection(Connection)
            rs = dbcommand.Execute

            Return "Good"

        Catch ex As SqlException
            If Connection.State <> ConnectionState.Closed Then
                Connection.Close()
            End If
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return "Bad" & " " & ex.ToString
        Catch ex As Exception
            If Connection.State <> ConnectionState.Closed Then
                Connection.Close()
            End If
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return "Bad" & " " & ex.ToString

        Finally


        End Try
    End Function

    Function GetCompletedCartonFromAS400(ByVal CTSSite As String, ByVal strSerial As String,
            ByVal BOM As String, ByVal CustPartNum As String, ByVal Qty As String,
            ByVal strDateCode As String, ByVal PackTime As Date, ByVal BoxLabelFormat As String,
            ByVal Cust_No As String) As ADODB.Recordset

        Dim dbcommand As New ADODB.Command

        Dim dbDataTable As New DataTable
        Dim rs As ADODB.Recordset

        Dim strText As String
        Try
            Connection = New ADODB.Connection
            Connection.ConnectionString = strDBAS400

            If Connection.State <> ConnectionState.Open Then
                Connection.Open()
            End If




            'Write Carton Record
            strText = "Select *  from RFGENCTS.CARTONS where Serial= '" & strSerial & "'"
            strText = strText & " and Company = '" & CTSSite & "' and Part_Number= '" & BOM & "'"
            strText = strText & " and Ctn_Qty = '" & Qty & "'"


           
            dbcommand.CommandText = strText
            dbcommand.let_ActiveConnection(Connection)
            rs = dbcommand.Execute

            Return rs

        Catch ex As SqlException
            If Connection.State <> ConnectionState.Closed Then
                Connection.Close()
            End If
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return Nothing
        Catch ex As Exception
            If Connection.State <> ConnectionState.Closed Then
                Connection.Close()
            End If
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return Nothing

        Finally


        End Try
    End Function

    Public Property Connection() As ADODB.Connection
        Get
            Return mConnection
        End Get
        Set(ByVal value As ADODB.Connection)
            mConnection = value
        End Set
    End Property

    Public Sub New()

    End Sub
End Class
