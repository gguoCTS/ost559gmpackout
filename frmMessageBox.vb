﻿Imports System.Windows.Forms

Public Class frmMessageBox
    Private mMessage As String
    Private mStyle As String

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.FormResult = "OK" ' System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.FormResult = "Cancel" 'System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub frmMessageBox_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        RichTextBox1.Text = Me.Message
        Call UpdateFormLanguage()
    End Sub

    Private Sub btnYes_Click(sender As Object, e As EventArgs) Handles btnYes.Click
        Me.FormResult = "Yes" ' System.Windows.Forms.DialogResult.Yes
        Me.Close()
    End Sub

    Private Sub btnNo_Click(sender As Object, e As EventArgs) Handles btnNo.Click
        Me.FormResult = "No" 'System.Windows.Forms.DialogResult.No
        Me.Close()
    End Sub
    Public Property FormResult() As String
        Get
            Return mMessage
        End Get
        Set(value As String)
            mMessage = value

        End Set
    End Property
    Public Property Message() As String
        Get
            Return mMessage
        End Get
        Set(value As String)
            mMessage = value

        End Set
    End Property
    Public Property Style() As String
        Get
            Return mStyle
        End Get
        Set(value As String)
            btnYes.Visible = False
            btnNo.Visible = False
            OK_Button.Visible = False
            Cancel_Button.Visible = False

            mStyle = value
            Select Case Style
                Case "YesNo"
                    btnYes.Visible = True
                    btnNo.Visible = True
                Case "YesNoCancel"
                    btnYes.Visible = True
                    btnNo.Visible = True
                    Cancel_Button.Visible = True
                Case "OkCancel"
                    OK_Button.Visible = True
                    Cancel_Button.Visible = True
                Case "Ok"
                    OK_Button.Visible = True
            End Select

        End Set
    End Property

    Private Sub UpdateFormLanguage()
        btnYes.Text = dictPhrases("Yes")
        btnNo.Text = dictPhrases("No")
        OK_Button.Text = dictPhrases("OK")
        Cancel_Button.Text = dictPhrases("Cancel")
    End Sub

End Class
