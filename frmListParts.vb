﻿Imports System.Configuration
Public Class frmListParts

    Dim tempColor As Color

    Private Sub txtCarton_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCarton.KeyPress
        Dim dtPackingList As New DataTable
        Dim dtContainer As New DataTable
        Dim strCurrentCartonNumber As String
        Dim intStartPosition As Integer = txtCarton.Text.ToUpper.IndexOf("S")
        strCurrentCartonNumber = Nothing

        Try
            If Asc(e.KeyChar) = 13 Then
                chkContainerFull.Checked = False
                chkContainerShipped.Checked = False
                dtPackingList.Rows.Clear()
                'enter carton
                If intStartPosition = -1 Then
                    strCurrentCartonNumber = Trim(txtCarton.Text)
                Else
                    strCurrentCartonNumber = txtCarton.Text.Substring(intStartPosition + 1)
                End If

                txtCarton.Text = ""

                GetPackingList(strCurrentCartonNumber, dtPackingList)
                If dtPackingList Is Nothing Or dtPackingList.Rows.Count = 0 Then
                    DisplayUserMessage(UserMessageType.InputError, dictPhrases("ContainerNotFound"))
                Else
                    lblContainerNumberValue.Text = strCurrentCartonNumber
                    lblBomValue.Text = dtPackingList.Rows(0).Item("BOM")
                    BOM = lblBomValue.Text

                    'try to find Partial Carton information
                    dtContainer = dbSQLServer.FindPartialCartonByNumber(strCurrentCartonNumber)
                    If dtContainer.Rows.Count > 0 Then 'load grid
                        lblBoxQty.Text = dtPackingList.Rows.Count
                    Else
                        Try
                            dtContainer = dbSQLServer.FindCompleteCartonByNumber(strCurrentCartonNumber)
                            If dtContainer.Rows.Count > 0 Then
                                lblBoxQty.Text = dtPackingList.Rows.Count
                                chkContainerFull.Checked = True

                                'check for shipped carton
                                dtContainer = dbSQLServer.FindShippedCartonByNumber(strCurrentCartonNumber)
                                If dtContainer.Rows.Count > 0 Then
                                    chkContainerShipped.Checked = True
                                End If
                            Else
                                DisplayUserMessage(UserMessageType.InputError, dictPhrases("ContainerNotFound"))
                            End If
                        Catch ex As Exception
                            DisplayUserMessage(UserMessageType.ApplicationError, ex.Message)
                        End Try
                    End If
                End If
            End If

        Catch ex As Exception
            DisplayUserMessage(UserMessageType.ApplicationError, ex.Message)
        End Try

    End Sub


    Private Sub frmListParts_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        tempColor = Me.BackColor
        Call UpdateFormLanguage()
    End Sub

    Private Sub txtCarton_Click(sender As Object, e As EventArgs) Handles txtCarton.Click
        Call ClearData()
        txtCarton.SelectAll()
    End Sub

    Sub ClearData()
        txtCarton.Clear()
        txtScanPart.Clear()
        dgPackingList.DataSource = String.Empty
    End Sub

    Sub UpdateFormLanguage()
        Me.Text = dictPhrases("frmListParts")
        lblScanOrTypeTheContainerNumber.Text = dictPhrases("lblScanOrTypeTheBoxNumber")
        lblScanPart.Text = dictPhrases("lblScanPart")
        lblOR.Text = dictPhrases("lblOR")
        lblPackingList.Text = dictPhrases("lblPackingList")
        lblContainerNumber.Text = dictPhrases("lblBoxNumberLabel")
        lblBOM.Text = dictPhrases("BOM")
        lblQuantity.Text = dictPhrases("lblContainerCountLabel")
        chkContainerFull.Text = dictPhrases("chkBoxFullContainer")
        chkContainerShipped.Text = dictPhrases("chkBoxShipped")
        cmdClose.Text = dictPhrases("Close")
    End Sub

    Private Sub GetPackingList(strContainerNumber As String, ByRef dtPackingList As DataTable)
        Try
            dbSQLServer.GetScannedPartsInCarton(strContainerNumber, dtPackingList)
            'TODO: Search local Machine as well
            'If dtPackingList.Rows.Count = 0 Then
            '    dtPackingList = dbLocalSQL.GetScannedPartList()
            'End If
            dgPackingList.DataSource = dtPackingList
            dgPackingList.Columns(0).Width = 50
            dgPackingList.Columns(1).Width = 155
            dgPackingList.Columns(2).Width = 140

        Catch ex As Exception
            Throw New Exception(ex.Message)
        End Try
    End Sub


    Private Sub txtScanPart_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtScanPart.KeyPress
        Dim strData As String = ""
        Dim bcdate As String = ""
        Dim bcPartNum As String = ""
        Dim bcSerial As String = ""
        Dim dtContainer As New DataTable
        Dim dtPackingList As New DataTable
        Try
            If Asc(e.KeyChar) = 13 Then
                chkContainerFull.Checked = False
                chkContainerShipped.Checked = False
                strData = txtScanPart.Text.Trim()

                If Parts(intCurrentProfile).BCSerialLength = strData.Length Then
                    bcSerial = strData
                Else
                    BOM = DecodeBarcode(Trim(strData), bcdate, bcSerial, bcPartNum)
                End If

                blnSqlServerRunning = dbSQLServer.TestLinkedServerConnection(strDBLOCALSQL, ConfigurationManager.AppSettings("SQLServer"))
                If blnSqlServerRunning Then
                    dtContainer = dbSQLServer.LookupBoxDatabyScannedPart(bcSerial)
                    If (dtContainer IsNot Nothing) And (dtContainer.Rows.Count > 0) Then
                        lblContainerNumberValue.Text = dtContainer.Rows(0).Item("CTNSERNUM").ToString
                        lblBomValue.Text = dtContainer.Rows(0).Item("BOM")
                        GetPackingList(lblContainerNumberValue.Text, dtPackingList)
                        lblBoxQty.Text = dtPackingList.Rows.Count
                    Else
                        DisplayUserMessage(UserMessageType.InputError, dictPhrases("ContainerNotFound"))
                    End If
                Else
                    DisplayUserMessage(UserMessageType.DatabaseConnectionError, dictPhrases("UnableToReprintLabel") & vbCrLf & dictPhrases("SQLServerOffline"))
                End If
            End If

        Catch ex As Exception
            DisplayUserMessage(UserMessageType.ApplicationError, ex.Message)
        End Try
    End Sub

    Private Sub txtScanPart_Click(sender As Object, e As EventArgs) Handles txtScanPart.Click
        ClearData()
        txtScanPart.SelectAll()
    End Sub

    Private Sub cmdClose_Click(sender As Object, e As EventArgs) Handles cmdClose.Click
        Me.Hide()
    End Sub

    Private Sub txtScanPart_TextChanged(sender As Object, e As EventArgs) Handles txtScanPart.TextChanged

    End Sub
End Class

