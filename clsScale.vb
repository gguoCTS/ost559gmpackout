﻿Imports System.Xml
Imports VB = Microsoft.VisualBasic

Public Class clsScale
#Region "Delegates =================================================="

#End Region

#Region "Events =================================================="

#End Region

#Region "Constructors =================================================="

    Public Sub New(ByVal configFilePath As String)
        Try

            Me.mstrClassConfigFilePath = configFilePath
            Me.mSerialPort = New System.IO.Ports.SerialPort

            'Read configuration settings from file
            GetConfigurationSettings()
            

            'Initialize Communication Settings
            Call InitializeCommunication()

        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
        End Try
    End Sub


#End Region

#Region "Private Member Variables =================================================="

    Private mstrClassConfigFilePath As String

    Private mSerialPort As System.IO.Ports.SerialPort
    Private mintSerialPortBaudRate As Integer
    Private mintSerialPortDataBits As Integer
    Private mintSerialPortHandshakeProtocol As IO.Ports.Handshake
    Private mstrSerialPortName As String
    Private mintSerialPortParity As IO.Ports.Parity
    Private mintSerialPortReadTimeout As Integer
    Private mintSerialPortStopBits As IO.Ports.StopBits
    Private mintSerialPortWriteTimeout As Integer
    Private mStopwatch As New clsStopWatch

#End Region

#Region "Public Constants, Structures, Enums =================================================="

#End Region

#Region "Properties =================================================="

    Public Property SerialPort() As System.IO.Ports.SerialPort
        Get
            Return mSerialPort
        End Get
        Set(ByVal value As System.IO.Ports.SerialPort)
            mSerialPort = value
        End Set
    End Property

    Public Property SerialPortBaudRate() As Integer
        Get
            Return Me.mintSerialPortBaudRate
        End Get
        Set(ByVal value As Integer)
            Me.mintSerialPortBaudRate = value
        End Set
    End Property

    Public Property SerialPortDataBits() As Integer
        Get
            Return Me.mintSerialPortDataBits
        End Get
        Set(ByVal value As Integer)
            Me.mintSerialPortDataBits = value
        End Set
    End Property

    Public Property SerialPortHandshakeProtocol() As IO.Ports.Handshake
        Get
            Return Me.mintSerialPortHandshakeProtocol
        End Get
        Set(ByVal value As IO.Ports.Handshake)
            Me.mintSerialPortHandshakeProtocol = value
        End Set
    End Property

    Public Property SerialPortParity() As IO.Ports.Parity
        Get
            Return Me.mintSerialPortParity
        End Get
        Set(ByVal value As IO.Ports.Parity)
            Me.mintSerialPortParity = value
        End Set
    End Property

    Public Property SerialPortReadTimeout() As Integer
        Get
            Return Me.mintSerialPortReadTimeout
        End Get
        Set(ByVal value As Integer)
            Me.mintSerialPortReadTimeout = value
        End Set
    End Property

    Public Property SerialPortStopBits() As IO.Ports.StopBits
        Get
            Return Me.mintSerialPortStopBits
        End Get
        Set(ByVal value As IO.Ports.StopBits)
            Me.mintSerialPortStopBits = value
        End Set
    End Property

    Public Property SerialPortWriteTimeout() As Integer
        Get
            Return Me.mintSerialPortWriteTimeout
        End Get
        Set(ByVal value As Integer)
            Me.mintSerialPortWriteTimeout = value
        End Set
    End Property

#End Region

#Region "Public Methods =================================================="

    Public Function GetResponseFromCommand(ByVal StrCommand As String) As String
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim strRead As String
        Dim strSerialData As String
        Try
            Me.mSerialPort.Write(StrCommand & vbLf)
            Me.mStopwatch.DelayTime(100)

            strRead = Me.mSerialPort.ReadLine
            strSerialData = Asc(Mid(strRead, strRead.Length - 1, 1))

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)
        Finally
        End Try
        Return strSerialData
    End Function


#End Region

#Region "Private Methods =================================================="

    Private Sub GetConfigurationSettings()
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Try
            Dim configReader As XmlTextReader
            Dim configSet As New DataSet
            Dim configTable As DataTable
            Dim configRow As DataRow
            Dim strClassName As String

            strClassName = Me.GetType.Name

            'Create the XML Reader
            configReader = New XmlTextReader(Me.mstrClassConfigFilePath & strClassName & ".cfg")

            'Clear data set that holds xml file contents
            configSet.Clear()
            'Read the file
            configSet.ReadXml(configReader)

            'Config File not hierarchical, only 1 table and 1 row, with many fields
            configTable = configSet.Tables(0)
            configRow = configTable.Rows(0)

            'Assign field values to variables
            Me.mstrSerialPortName = configRow("SerialPortName")
            Me.mintSerialPortBaudRate = configRow("SerialPortBaudRate")
            Me.mintSerialPortParity = DirectCast([Enum].Parse(GetType(IO.Ports.Parity), configRow("SerialPortParity")), Integer)
            Me.mintSerialPortDataBits = configRow("SerialPortDataBits")
            Me.mintSerialPortStopBits = DirectCast([Enum].Parse(GetType(IO.Ports.StopBits), configRow("SerialPortStopBits")), Integer)
            Me.mintSerialPortWriteTimeout = configRow("SerialPortWriteTimeoutMs")
            Me.mintSerialPortReadTimeout = configRow("SerialPortReadTimeoutMs")
            Me.mintSerialPortHandshakeProtocol = DirectCast([Enum].Parse(GetType(IO.Ports.Handshake), configRow("SerialPortHandshakeProtocol")), Integer)

            'Close reader, releases file
            configReader.Close()

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)
        Finally

        End Try
    End Sub

    Public Sub InitializeCommunication()
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name
        '
        '   PURPOSE: To establish RS232 Communication with device
        '
        '  INPUT(S): 
        '
        ' OUTPUT(S)

        Try

            'Make sure the port is closed
            If Me.SerialPort.IsOpen = True Then Me.SerialPort.Close()

            Me.SerialPort.PortName = Me.mstrSerialPortName 'Set the serial port #
            Me.SerialPort.BaudRate = Me.mintSerialPortBaudRate
            Me.SerialPort.Parity = Me.mintSerialPortParity
            Me.SerialPort.DataBits = Me.mintSerialPortDataBits
            Me.SerialPort.StopBits = Me.mintSerialPortStopBits
            Me.SerialPort.WriteTimeout = Me.mintSerialPortWriteTimeout
            Me.SerialPort.ReadTimeout = Me.mintSerialPortReadTimeout
            Me.SerialPort.Handshake = Me.mintSerialPortHandshakeProtocol 'Set flow control

            Me.SerialPort.Open()  'Open COM Port

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)
        Finally

        End Try
    End Sub


#End Region
End Class
