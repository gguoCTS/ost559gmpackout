Imports System.Configuration
Public Class frmPickBOM

    Dim intCurrentRow As Integer

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        Dim intData As Integer
        Dim strData As String
        '    intData = sender.ToString
        If DataGridView1.SelectedRows().Count > 0 Then
            intData = Val(DataGridView1.SelectedRows(0).Index)
            strData = DataGridView1.SelectedRows(0).Index
            gIntCurrentBomPosition = intCurrentRow + 1
            strCurrentBOM = DataGridView1(0, intData).Value ''BOMList(gIntCurrentBomPosition).BOM
            strCurrentCustomerNumber = DataGridView1(2, intData).Value ' BOMList(gIntCurrentBomPosition).CustomerNumber
            strCurrentPartNumber = DataGridView1(3, intData).Value 'BOMList(gIntCurrentBomPosition).ScannedPartNumber
            strCurrentDrawingNumber = DataGridView1(4, intData).Value ' BOMList(gIntCurrentBomPosition).DrawingNumber
            frmMain.lblBOMDisplay.Text = dictPhrases("BOM") & Space(2) & strCurrentBOM
            frmMain.ToolStripMenuItem1.PerformClick()
            Me.Close()
        Else
            DisplayUserMessage(UserMessageType.InputError, dictPhrases("BOM_SelectionRequired"))
        End If
    End Sub


    Private Sub frmPickBOM_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim intLoop As Integer

        Call UpdateFormLanguage()

        cboProfileName.Items.Clear()

        For intLoop = 1 To intNumberOfProfiles
            cboProfileName.Items.Add(Parts(intLoop).Name.Trim)
        Next

        If gStrCurrentProfileName = Nothing Then
            gStrCurrentProfileName = ConfigurationManager.AppSettings("DefaultProfile")
        End If

        cboProfileName.Text = gStrCurrentProfileName
        cboProfileName.SelectedIndex = cboProfileName.Items.IndexOf(gStrCurrentProfileName)
        UpdateBOM_List()
    End Sub


    Sub UpdateFormLanguage()
        lblBOM_Select.Text = dictPhrases("lblBOM_Select")
        lblProfile.Text = dictPhrases("lblProfile")
        Me.Text = dictPhrases("frmBOM_LookupMaintenance")
        OK.Text = dictPhrases("OK")
    End Sub


    Private Sub DataGridView1_CellClick(ByVal sender As Object, ByVal e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        If e.RowIndex >= 0 Then
            DataGridView1.Rows(e.RowIndex).Selected = True
            intCurrentRow = e.RowIndex
            OK.Enabled = True
        Else
            OK.Enabled = False
        End If
    End Sub


    Private Sub cboProfileName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboProfileName.SelectedIndexChanged
        Try
            UpdateBOM_List()
        Catch ex As Exception
            DisplayUserMessage(UserMessageType.ApplicationError, ex.Message)
        End Try
    End Sub

    Public Sub UpdateBOM_List()
        Dim intLoop As Integer
        intCurrentProfile = cboProfileName.SelectedIndex + 1
        gstrCurrentProfileID = Parts(cboProfileName.SelectedIndex + 1).ID
        gStrCurrentProfileName = cboProfileName.Text

        Call GetBOMListByProfile()

        DataGridView1.Rows.Clear()

        For intLoop = 1 To BOMList.Length - 1

            DataGridView1.Rows.Add(Trim(BOMList(intLoop).BOM),
                                   Trim(BOMList(intLoop).CustomerLocation),
                                   Trim(BOMList(intLoop).CustomerNumber),
                                   Trim(BOMList(intLoop).ScannedPartNumber),
                                   Trim(BOMList(intLoop).DrawingNumber)
                                    )
        Next

        DataGridView1.Item(0, 0).Selected = False
        OK.Enabled = False
    End Sub

    'Moved function inside form 05MAY2016
    Sub GetBOMListByProfile()

        Dim dtData As DataTable
        Dim intLoop As Integer

        If gStrCurrentProfileName <> Nothing Then
            dtData = dbLocalSQL.GetBOMListByProfile(Parts(intCurrentProfile).CTSSite, gStrCurrentProfileName)

            For intLoop = 0 To dtData.Rows.Count - 1
                ReDim Preserve BOMList(intLoop + 1)
                BOMList(intLoop + 1).CustomerNumber = dtData.Rows(intLoop).Item("CustomerNumber").ToString
                BOMList(intLoop + 1).BOM = dtData.Rows(intLoop).Item("BOM").ToString
                BOMList(intLoop + 1).ScannedPartNumber = dtData.Rows(intLoop).Item("ScannedPartNumber").ToString
                BOMList(intLoop + 1).DrawingNumber = dtData.Rows(intLoop).Item("DrawingNumber").ToString
                BOMList(intLoop + 1).SystemProfileID = dtData.Rows(intLoop).Item("SystemProfileID").ToString
                BOMList(intLoop + 1).CustomerLocation = dtData.Rows(intLoop).Item("CustomerLocation").ToString
            Next
        Else
            DisplayUserMessage(UserMessageType.InputError, dictPhrases("BOM_SelectionRequired"))
        End If
    End Sub
End Class