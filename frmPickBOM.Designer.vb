<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPickBOM
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub
    Friend WithEvents OK As System.Windows.Forms.Button

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.OK = New System.Windows.Forms.Button()
        Me.lblBOM_Select = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.BOM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustomerLocation = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CustomerNumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ScannedPartNumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DrawingNumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblProfile = New System.Windows.Forms.Label()
        Me.cboProfileName = New System.Windows.Forms.ComboBox()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'OK
        '
        Me.OK.Enabled = False
        Me.OK.Font = New System.Drawing.Font("Arial", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OK.Location = New System.Drawing.Point(827, 250)
        Me.OK.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.OK.Name = "OK"
        Me.OK.Size = New System.Drawing.Size(197, 105)
        Me.OK.TabIndex = 4
        Me.OK.Text = "btnOK"
        '
        'lblBOM_Select
        '
        Me.lblBOM_Select.AutoSize = True
        Me.lblBOM_Select.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBOM_Select.Location = New System.Drawing.Point(75, 96)
        Me.lblBOM_Select.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblBOM_Select.Name = "lblBOM_Select"
        Me.lblBOM_Select.Size = New System.Drawing.Size(108, 25)
        Me.lblBOM_Select.TabIndex = 6
        Me.lblBOM_Select.Text = "Pick BOM"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.BOM, Me.CustomerLocation, Me.CustomerNumber, Me.ScannedPartNumber, Me.DrawingNumber})
        Me.DataGridView1.Location = New System.Drawing.Point(63, 124)
        Me.DataGridView1.Margin = New System.Windows.Forms.Padding(4)
        Me.DataGridView1.MultiSelect = False
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(708, 587)
        Me.DataGridView1.TabIndex = 7
        '
        'BOM
        '
        Me.BOM.HeaderText = "BOM"
        Me.BOM.Name = "BOM"
        Me.BOM.Width = 64
        '
        'CustomerLocation
        '
        Me.CustomerLocation.HeaderText = "Customer Location"
        Me.CustomerLocation.Name = "CustomerLocation"
        Me.CustomerLocation.Width = 138
        '
        'CustomerNumber
        '
        Me.CustomerNumber.HeaderText = "Customer Number"
        Me.CustomerNumber.Name = "CustomerNumber"
        Me.CustomerNumber.Width = 134
        '
        'ScannedPartNumber
        '
        Me.ScannedPartNumber.HeaderText = "Scanned Part Number"
        Me.ScannedPartNumber.Name = "ScannedPartNumber"
        Me.ScannedPartNumber.Width = 113
        '
        'DrawingNumber
        '
        Me.DrawingNumber.HeaderText = "Drawing Number"
        Me.DrawingNumber.Name = "DrawingNumber"
        Me.DrawingNumber.Width = 126
        '
        'lblProfile
        '
        Me.lblProfile.AutoSize = True
        Me.lblProfile.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProfile.Location = New System.Drawing.Point(162, -1)
        Me.lblProfile.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblProfile.Name = "lblProfile"
        Me.lblProfile.Size = New System.Drawing.Size(138, 25)
        Me.lblProfile.TabIndex = 9
        Me.lblProfile.Text = "Pick a Profile"
        '
        'cboProfileName
        '
        Me.cboProfileName.FormattingEnabled = True
        Me.cboProfileName.Location = New System.Drawing.Point(167, 28)
        Me.cboProfileName.Margin = New System.Windows.Forms.Padding(4)
        Me.cboProfileName.Name = "cboProfileName"
        Me.cboProfileName.Size = New System.Drawing.Size(371, 24)
        Me.cboProfileName.TabIndex = 8
        '
        'frmPickBOM
        '
        Me.AcceptButton = Me.OK
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1109, 753)
        Me.Controls.Add(Me.lblProfile)
        Me.Controls.Add(Me.cboProfileName)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.lblBOM_Select)
        Me.Controls.Add(Me.OK)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPickBOM"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Pick BOM"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblBOM_Select As System.Windows.Forms.Label
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents BOM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CustomerLocation As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CustomerNumber As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ScannedPartNumber As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DrawingNumber As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblProfile As System.Windows.Forms.Label
    Friend WithEvents cboProfileName As System.Windows.Forms.ComboBox
End Class
