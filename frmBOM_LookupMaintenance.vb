Imports System
Imports System.Data.SqlClient

Public Class frmBOM_LookupMaintenance
    Private bInitializing As Boolean
    Private dt As New DataTable
    Private daBOM As SqlDataAdapter

    Private Sub frmBOM_LookupMaintenance_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            UpdateFormLanguage()
            cboSystemProfiles.ValueMember = "SystemProfileID"
            cboSystemProfiles.DisplayMember = "Name"
            Dim dt As DataTable = dbSQLServer.GetSystemProfileNames()
            cboSystemProfiles.DataSource = dt

        Catch ex As SqlException
            DisplayUserMessage(UserMessageType.ApplicationError, ex.Message)
        End Try
    End Sub

    Sub UpdateFormLanguage()
        Me.Text = dictPhrases("frmBOM_LookupMaintenance")
        lblAssemblyLine.Text = dictPhrases("lblAssemblyLine")
        btnClose.Text = dictPhrases("Close")
    End Sub


    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Dispose()
    End Sub


    Private Sub BindDynamicParameterSetGrid(SystemProfileID As System.Guid)
        bInitializing = True
        dt.Clear()
        daBOM = dbSQLServer.GetDynamicParameterSetDataAdapter(SystemProfileID)
        daBOM.Fill(dt)
        dgBOM.DataSource = dt

        dgBOM.Columns("BOM_LookupID").Visible = False
        dgBOM.Columns("SystemProfileID").Visible = False
        dgBOM.Columns("BOM").Visible = True
        dgBOM.Columns("CustomerLocation").Visible = True
        dgBOM.Columns("CustomerNumber").Visible = True
        dgBOM.Columns("ScannedPartNumber").Visible = True
        dgBOM.Columns("DrawingNumber").Visible = True
        dgBOM.Columns("Active").Visible = True

        bInitializing = False
    End Sub

    Private Sub UpdateBomLookupTable()
        Try
            daBOM.Update(dt)
            dgBOM.DataSource = Nothing
            BindDynamicParameterSetGrid(cboSystemProfiles.SelectedValue)
        Catch ex As SqlException
            DisplayUserMessage(UserMessageType.ApplicationError, ex.Message)
        End Try
    End Sub

    Private Sub cboSystemProfiles_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboSystemProfiles.SelectedIndexChanged
        If cboSystemProfiles.SelectedIndex > -1 Then
            BindDynamicParameterSetGrid(cboSystemProfiles.SelectedValue)
        Else
            dgBOM.DataSource = String.Empty
        End If
    End Sub

    Private Sub dgBOM_Leave(sender As Object, e As EventArgs) Handles dgBOM.Leave
        If Not bInitializing Then
            UpdateBomLookupTable()
        End If
    End Sub

    Private Sub dgBOM_KeyUp(sender As Object, e As KeyEventArgs) Handles dgBOM.KeyUp
        If e.KeyValue = Keys.Enter Then
            UpdateBomLookupTable()
        End If
    End Sub

    Private Sub dgBOM_UserAddedRow(sender As Object, e As DataGridViewRowEventArgs) Handles dgBOM.UserAddedRow
        dgBOM.Item("BOM_LookupID", e.Row.Index - 1).Value = Guid.NewGuid
        dgBOM.Item("SystemProfileID", e.Row.Index - 1).Value = cboSystemProfiles.SelectedValue
    End Sub

  Private Sub frmBOM_LookupMaintenance_MouseClick(sender As Object, e As MouseEventArgs) Handles Me.MouseClick

  End Sub

  Private Sub cboSystemProfiles_Click(sender As Object, e As EventArgs) Handles cboSystemProfiles.Click

  End Sub
End Class