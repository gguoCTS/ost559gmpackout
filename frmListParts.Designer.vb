﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListParts
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmListParts))
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.txtCarton = New System.Windows.Forms.TextBox()
        Me.lblScanOrTypeTheContainerNumber = New System.Windows.Forms.Label()
        Me.cmdClose = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.chkContainerShipped = New System.Windows.Forms.CheckBox()
        Me.chkContainerFull = New System.Windows.Forms.CheckBox()
        Me.lblContainerNumberValue = New System.Windows.Forms.Label()
        Me.lblContainerNumber = New System.Windows.Forms.Label()
        Me.lblBoxQty = New System.Windows.Forms.Label()
        Me.lblQuantity = New System.Windows.Forms.Label()
        Me.lblBomValue = New System.Windows.Forms.Label()
        Me.lblBOM = New System.Windows.Forms.Label()
        Me.lblPackingList = New System.Windows.Forms.Label()
        Me.dgPackingList = New System.Windows.Forms.DataGridView()
        Me.lblOR = New System.Windows.Forms.Label()
        Me.lblScanPart = New System.Windows.Forms.Label()
        Me.txtScanPart = New System.Windows.Forms.TextBox()
        Me.Panel1.SuspendLayout()
        CType(Me.dgPackingList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtCarton
        '
        Me.txtCarton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCarton.Location = New System.Drawing.Point(55, 74)
        Me.txtCarton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtCarton.Name = "txtCarton"
        Me.txtCarton.Size = New System.Drawing.Size(346, 30)
        Me.txtCarton.TabIndex = 1
        Me.txtCarton.Text = resources.GetString("txtCarton.Text")
        '
        'lblScanOrTypeTheContainerNumber
        '
        Me.lblScanOrTypeTheContainerNumber.AutoSize = True
        Me.lblScanOrTypeTheContainerNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScanOrTypeTheContainerNumber.Location = New System.Drawing.Point(52, 47)
        Me.lblScanOrTypeTheContainerNumber.Name = "lblScanOrTypeTheContainerNumber"
        Me.lblScanOrTypeTheContainerNumber.Size = New System.Drawing.Size(225, 25)
        Me.lblScanOrTypeTheContainerNumber.TabIndex = 2
        Me.lblScanOrTypeTheContainerNumber.Text = "Scan or Type Container "
        '
        'cmdClose
        '
        Me.cmdClose.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.cmdClose.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdClose.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdClose.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdClose.Location = New System.Drawing.Point(63, 447)
        Me.cmdClose.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.cmdClose.Name = "cmdClose"
        Me.cmdClose.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdClose.Size = New System.Drawing.Size(493, 59)
        Me.cmdClose.TabIndex = 14
        Me.cmdClose.Text = "Close Form"
        Me.cmdClose.UseVisualStyleBackColor = False
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.chkContainerShipped)
        Me.Panel1.Controls.Add(Me.chkContainerFull)
        Me.Panel1.Controls.Add(Me.lblContainerNumberValue)
        Me.Panel1.Controls.Add(Me.lblContainerNumber)
        Me.Panel1.Controls.Add(Me.lblBoxQty)
        Me.Panel1.Controls.Add(Me.lblQuantity)
        Me.Panel1.Controls.Add(Me.lblBomValue)
        Me.Panel1.Controls.Add(Me.lblBOM)
        Me.Panel1.Location = New System.Drawing.Point(63, 173)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(537, 219)
        Me.Panel1.TabIndex = 15
        '
        'chkContainerShipped
        '
        Me.chkContainerShipped.AutoSize = True
        Me.chkContainerShipped.Enabled = False
        Me.chkContainerShipped.Location = New System.Drawing.Point(279, 175)
        Me.chkContainerShipped.Margin = New System.Windows.Forms.Padding(4)
        Me.chkContainerShipped.Name = "chkContainerShipped"
        Me.chkContainerShipped.Size = New System.Drawing.Size(82, 21)
        Me.chkContainerShipped.TabIndex = 26
        Me.chkContainerShipped.Text = "Shipped"
        Me.chkContainerShipped.UseVisualStyleBackColor = True
        '
        'chkContainerFull
        '
        Me.chkContainerFull.AutoSize = True
        Me.chkContainerFull.Enabled = False
        Me.chkContainerFull.Location = New System.Drawing.Point(109, 175)
        Me.chkContainerFull.Margin = New System.Windows.Forms.Padding(4)
        Me.chkContainerFull.Name = "chkContainerFull"
        Me.chkContainerFull.Size = New System.Drawing.Size(117, 21)
        Me.chkContainerFull.TabIndex = 25
        Me.chkContainerFull.Text = "Full Container"
        Me.chkContainerFull.UseVisualStyleBackColor = True
        '
        'lblContainerNumberValue
        '
        Me.lblContainerNumberValue.BackColor = System.Drawing.Color.White
        Me.lblContainerNumberValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblContainerNumberValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContainerNumberValue.Location = New System.Drawing.Point(278, 35)
        Me.lblContainerNumberValue.Name = "lblContainerNumberValue"
        Me.lblContainerNumberValue.Size = New System.Drawing.Size(219, 27)
        Me.lblContainerNumberValue.TabIndex = 24
        Me.lblContainerNumberValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblContainerNumber
        '
        Me.lblContainerNumber.AutoSize = True
        Me.lblContainerNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContainerNumber.Location = New System.Drawing.Point(13, 35)
        Me.lblContainerNumber.Name = "lblContainerNumber"
        Me.lblContainerNumber.Size = New System.Drawing.Size(171, 25)
        Me.lblContainerNumber.TabIndex = 23
        Me.lblContainerNumber.Text = "Container Number"
        '
        'lblBoxQty
        '
        Me.lblBoxQty.BackColor = System.Drawing.Color.White
        Me.lblBoxQty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBoxQty.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBoxQty.Location = New System.Drawing.Point(278, 113)
        Me.lblBoxQty.Name = "lblBoxQty"
        Me.lblBoxQty.Size = New System.Drawing.Size(219, 27)
        Me.lblBoxQty.TabIndex = 20
        Me.lblBoxQty.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblQuantity
        '
        Me.lblQuantity.AutoSize = True
        Me.lblQuantity.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQuantity.Location = New System.Drawing.Point(13, 113)
        Me.lblQuantity.Name = "lblQuantity"
        Me.lblQuantity.Size = New System.Drawing.Size(85, 25)
        Me.lblQuantity.TabIndex = 19
        Me.lblQuantity.Text = "Quantity"
        '
        'lblBomValue
        '
        Me.lblBomValue.BackColor = System.Drawing.Color.White
        Me.lblBomValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBomValue.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBomValue.Location = New System.Drawing.Point(278, 72)
        Me.lblBomValue.Name = "lblBomValue"
        Me.lblBomValue.Size = New System.Drawing.Size(219, 27)
        Me.lblBomValue.TabIndex = 18
        Me.lblBomValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblBOM
        '
        Me.lblBOM.AutoSize = True
        Me.lblBOM.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBOM.Location = New System.Drawing.Point(13, 72)
        Me.lblBOM.Name = "lblBOM"
        Me.lblBOM.Size = New System.Drawing.Size(58, 25)
        Me.lblBOM.TabIndex = 17
        Me.lblBOM.Text = "BOM"
        '
        'lblPackingList
        '
        Me.lblPackingList.AutoSize = True
        Me.lblPackingList.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPackingList.Location = New System.Drawing.Point(688, 136)
        Me.lblPackingList.Name = "lblPackingList"
        Me.lblPackingList.Size = New System.Drawing.Size(117, 25)
        Me.lblPackingList.TabIndex = 16
        Me.lblPackingList.Text = "Packing List"
        '
        'dgPackingList
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgPackingList.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgPackingList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgPackingList.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgPackingList.Location = New System.Drawing.Point(651, 170)
        Me.dgPackingList.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.dgPackingList.Name = "dgPackingList"
        Me.dgPackingList.RowTemplate.Height = 24
        Me.dgPackingList.Size = New System.Drawing.Size(502, 335)
        Me.dgPackingList.TabIndex = 17
        Me.dgPackingList.TabStop = False
        '
        'lblOR
        '
        Me.lblOR.AutoSize = True
        Me.lblOR.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOR.Location = New System.Drawing.Point(474, 47)
        Me.lblOR.Name = "lblOR"
        Me.lblOR.Size = New System.Drawing.Size(41, 25)
        Me.lblOR.TabIndex = 18
        Me.lblOR.Text = "OR"
        '
        'lblScanPart
        '
        Me.lblScanPart.AutoSize = True
        Me.lblScanPart.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScanPart.Location = New System.Drawing.Point(571, 47)
        Me.lblScanPart.Name = "lblScanPart"
        Me.lblScanPart.Size = New System.Drawing.Size(103, 25)
        Me.lblScanPart.TabIndex = 19
        Me.lblScanPart.Text = "Scan Part "
        '
        'txtScanPart
        '
        Me.txtScanPart.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtScanPart.Location = New System.Drawing.Point(572, 74)
        Me.txtScanPart.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtScanPart.Name = "txtScanPart"
        Me.txtScanPart.Size = New System.Drawing.Size(331, 30)
        Me.txtScanPart.TabIndex = 2
        Me.txtScanPart.Text = resources.GetString("txtScanPart.Text")
        '
        'frmListParts
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1207, 527)
        Me.Controls.Add(Me.txtScanPart)
        Me.Controls.Add(Me.lblScanPart)
        Me.Controls.Add(Me.lblOR)
        Me.Controls.Add(Me.dgPackingList)
        Me.Controls.Add(Me.lblPackingList)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.cmdClose)
        Me.Controls.Add(Me.txtCarton)
        Me.Controls.Add(Me.lblScanOrTypeTheContainerNumber)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Name = "frmListParts"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "List Parts in Container"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgPackingList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtCarton As System.Windows.Forms.TextBox
    Friend WithEvents lblScanOrTypeTheContainerNumber As System.Windows.Forms.Label
    Public WithEvents cmdClose As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblBOM As System.Windows.Forms.Label
    Friend WithEvents lblPackingList As System.Windows.Forms.Label
    Friend WithEvents lblContainerNumberValue As System.Windows.Forms.Label
    Friend WithEvents lblContainerNumber As System.Windows.Forms.Label
    Friend WithEvents lblBoxQty As System.Windows.Forms.Label
    Friend WithEvents lblQuantity As System.Windows.Forms.Label
    Friend WithEvents lblBomValue As System.Windows.Forms.Label
    Friend WithEvents dgPackingList As System.Windows.Forms.DataGridView
    Friend WithEvents chkContainerShipped As System.Windows.Forms.CheckBox
    Friend WithEvents chkContainerFull As System.Windows.Forms.CheckBox
    Friend WithEvents lblOR As System.Windows.Forms.Label
    Friend WithEvents lblScanPart As System.Windows.Forms.Label
    Friend WithEvents txtScanPart As System.Windows.Forms.TextBox
End Class
