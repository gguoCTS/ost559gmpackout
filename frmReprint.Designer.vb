﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReprint
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReprint))
        Me.pnlBG = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtPartFromBox = New System.Windows.Forms.TextBox()
        Me.lblBoxInfo = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblBoxNumber = New System.Windows.Forms.Label()
        Me.lblBoxNumberLabel = New System.Windows.Forms.Label()
        Me.lblBOMNum = New System.Windows.Forms.Label()
        Me.lblBOMNumLabel = New System.Windows.Forms.Label()
        Me.lblPartQty = New System.Windows.Forms.Label()
        Me.lblPartQtyLabel = New System.Windows.Forms.Label()
        Me.lblPartNumber = New System.Windows.Forms.Label()
        Me.lblPartNumberLabel = New System.Windows.Forms.Label()
        Me.lblScanOrTypeBoxNumber = New System.Windows.Forms.Label()
        Me.cmdReprint = New System.Windows.Forms.Button()
        Me.txtCarton = New System.Windows.Forms.TextBox()
        Me.lblScanPart = New System.Windows.Forms.Label()
        Me.pnlBG.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlBG
        '
        Me.pnlBG.Controls.Add(Me.Panel2)
        Me.pnlBG.Location = New System.Drawing.Point(57, 38)
        Me.pnlBG.Margin = New System.Windows.Forms.Padding(4)
        Me.pnlBG.Name = "pnlBG"
        Me.pnlBG.Size = New System.Drawing.Size(994, 497)
        Me.pnlBG.TabIndex = 17
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.txtPartFromBox)
        Me.Panel2.Controls.Add(Me.lblBoxInfo)
        Me.Panel2.Controls.Add(Me.Panel1)
        Me.Panel2.Controls.Add(Me.lblScanOrTypeBoxNumber)
        Me.Panel2.Controls.Add(Me.cmdReprint)
        Me.Panel2.Controls.Add(Me.txtCarton)
        Me.Panel2.Controls.Add(Me.lblScanPart)
        Me.Panel2.Location = New System.Drawing.Point(37, 32)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(919, 428)
        Me.Panel2.TabIndex = 17
        '
        'txtPartFromBox
        '
        Me.txtPartFromBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPartFromBox.Location = New System.Drawing.Point(36, 244)
        Me.txtPartFromBox.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtPartFromBox.Name = "txtPartFromBox"
        Me.txtPartFromBox.Size = New System.Drawing.Size(300, 30)
        Me.txtPartFromBox.TabIndex = 25
        Me.txtPartFromBox.Text = resources.GetString("txtPartFromBox.Text")
        Me.txtPartFromBox.WordWrap = False
        '
        'lblBoxInfo
        '
        Me.lblBoxInfo.AutoSize = True
        Me.lblBoxInfo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBoxInfo.Location = New System.Drawing.Point(486, 20)
        Me.lblBoxInfo.Name = "lblBoxInfo"
        Me.lblBoxInfo.Size = New System.Drawing.Size(117, 25)
        Me.lblBoxInfo.TabIndex = 24
        Me.lblBoxInfo.Text = "Packing List"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.lblBoxNumber)
        Me.Panel1.Controls.Add(Me.lblBoxNumberLabel)
        Me.Panel1.Controls.Add(Me.lblBOMNum)
        Me.Panel1.Controls.Add(Me.lblBOMNumLabel)
        Me.Panel1.Controls.Add(Me.lblPartQty)
        Me.Panel1.Controls.Add(Me.lblPartQtyLabel)
        Me.Panel1.Controls.Add(Me.lblPartNumber)
        Me.Panel1.Controls.Add(Me.lblPartNumberLabel)
        Me.Panel1.Location = New System.Drawing.Point(469, 49)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(404, 244)
        Me.Panel1.TabIndex = 23
        '
        'lblBoxNumber
        '
        Me.lblBoxNumber.BackColor = System.Drawing.Color.White
        Me.lblBoxNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBoxNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBoxNumber.Location = New System.Drawing.Point(236, 199)
        Me.lblBoxNumber.Name = "lblBoxNumber"
        Me.lblBoxNumber.Size = New System.Drawing.Size(148, 27)
        Me.lblBoxNumber.TabIndex = 24
        Me.lblBoxNumber.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblBoxNumberLabel
        '
        Me.lblBoxNumberLabel.AutoSize = True
        Me.lblBoxNumberLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBoxNumberLabel.Location = New System.Drawing.Point(12, 201)
        Me.lblBoxNumberLabel.Name = "lblBoxNumberLabel"
        Me.lblBoxNumberLabel.Size = New System.Drawing.Size(171, 25)
        Me.lblBoxNumberLabel.TabIndex = 23
        Me.lblBoxNumberLabel.Text = "Container Number"
        '
        'lblBOMNum
        '
        Me.lblBOMNum.BackColor = System.Drawing.Color.White
        Me.lblBOMNum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBOMNum.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBOMNum.Location = New System.Drawing.Point(236, 21)
        Me.lblBOMNum.Name = "lblBOMNum"
        Me.lblBOMNum.Size = New System.Drawing.Size(148, 27)
        Me.lblBOMNum.TabIndex = 22
        Me.lblBOMNum.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblBOMNumLabel
        '
        Me.lblBOMNumLabel.AutoSize = True
        Me.lblBOMNumLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBOMNumLabel.Location = New System.Drawing.Point(12, 21)
        Me.lblBOMNumLabel.Name = "lblBOMNumLabel"
        Me.lblBOMNumLabel.Size = New System.Drawing.Size(132, 25)
        Me.lblBOMNumLabel.TabIndex = 21
        Me.lblBOMNumLabel.Text = "BOM Number"
        '
        'lblPartQty
        '
        Me.lblPartQty.BackColor = System.Drawing.Color.White
        Me.lblPartQty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblPartQty.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPartQty.Location = New System.Drawing.Point(236, 112)
        Me.lblPartQty.Name = "lblPartQty"
        Me.lblPartQty.Size = New System.Drawing.Size(148, 27)
        Me.lblPartQty.TabIndex = 20
        Me.lblPartQty.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPartQtyLabel
        '
        Me.lblPartQtyLabel.AutoSize = True
        Me.lblPartQtyLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPartQtyLabel.Location = New System.Drawing.Point(12, 112)
        Me.lblPartQtyLabel.Name = "lblPartQtyLabel"
        Me.lblPartQtyLabel.Size = New System.Drawing.Size(125, 25)
        Me.lblPartQtyLabel.TabIndex = 19
        Me.lblPartQtyLabel.Text = "Part Quantity"
        '
        'lblPartNumber
        '
        Me.lblPartNumber.BackColor = System.Drawing.Color.White
        Me.lblPartNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblPartNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPartNumber.Location = New System.Drawing.Point(236, 68)
        Me.lblPartNumber.Name = "lblPartNumber"
        Me.lblPartNumber.Size = New System.Drawing.Size(148, 27)
        Me.lblPartNumber.TabIndex = 18
        Me.lblPartNumber.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblPartNumberLabel
        '
        Me.lblPartNumberLabel.AutoSize = True
        Me.lblPartNumberLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPartNumberLabel.Location = New System.Drawing.Point(12, 68)
        Me.lblPartNumberLabel.Name = "lblPartNumberLabel"
        Me.lblPartNumberLabel.Size = New System.Drawing.Size(121, 25)
        Me.lblPartNumberLabel.TabIndex = 17
        Me.lblPartNumberLabel.Text = "Part Number"
        '
        'lblScanOrTypeBoxNumber
        '
        Me.lblScanOrTypeBoxNumber.AutoSize = True
        Me.lblScanOrTypeBoxNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScanOrTypeBoxNumber.Location = New System.Drawing.Point(31, 42)
        Me.lblScanOrTypeBoxNumber.Name = "lblScanOrTypeBoxNumber"
        Me.lblScanOrTypeBoxNumber.Size = New System.Drawing.Size(220, 25)
        Me.lblScanOrTypeBoxNumber.TabIndex = 17
        Me.lblScanOrTypeBoxNumber.Text = "Scan or Type Container"
        '
        'cmdReprint
        '
        Me.cmdReprint.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.cmdReprint.Cursor = System.Windows.Forms.Cursors.Default
        Me.cmdReprint.Enabled = False
        Me.cmdReprint.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdReprint.ForeColor = System.Drawing.SystemColors.ControlText
        Me.cmdReprint.Location = New System.Drawing.Point(149, 348)
        Me.cmdReprint.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.cmdReprint.Name = "cmdReprint"
        Me.cmdReprint.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.cmdReprint.Size = New System.Drawing.Size(449, 59)
        Me.cmdReprint.TabIndex = 22
        Me.cmdReprint.Text = "PRINT"
        Me.cmdReprint.UseVisualStyleBackColor = False
        '
        'txtCarton
        '
        Me.txtCarton.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCarton.Location = New System.Drawing.Point(36, 71)
        Me.txtCarton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtCarton.Name = "txtCarton"
        Me.txtCarton.Size = New System.Drawing.Size(300, 30)
        Me.txtCarton.TabIndex = 18
        Me.txtCarton.Text = resources.GetString("txtCarton.Text")
        Me.txtCarton.WordWrap = False
        '
        'lblScanPart
        '
        Me.lblScanPart.AutoSize = True
        Me.lblScanPart.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScanPart.Location = New System.Drawing.Point(31, 215)
        Me.lblScanPart.Name = "lblScanPart"
        Me.lblScanPart.Size = New System.Drawing.Size(231, 25)
        Me.lblScanPart.TabIndex = 20
        Me.lblScanPart.Text = "Scan Part from Container"
        '
        'frmReprint
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1098, 572)
        Me.Controls.Add(Me.pnlBG)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Name = "frmReprint"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reprint Container Label"
        Me.pnlBG.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlBG As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents lblBoxInfo As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblBoxNumber As System.Windows.Forms.Label
    Friend WithEvents lblBoxNumberLabel As System.Windows.Forms.Label
    Friend WithEvents lblBOMNum As System.Windows.Forms.Label
    Friend WithEvents lblBOMNumLabel As System.Windows.Forms.Label
    Friend WithEvents lblPartQty As System.Windows.Forms.Label
    Friend WithEvents lblPartQtyLabel As System.Windows.Forms.Label
    Friend WithEvents lblPartNumber As System.Windows.Forms.Label
    Friend WithEvents lblPartNumberLabel As System.Windows.Forms.Label
    Friend WithEvents lblScanOrTypeBoxNumber As System.Windows.Forms.Label
    Public WithEvents cmdReprint As System.Windows.Forms.Button
    Friend WithEvents txtCarton As System.Windows.Forms.TextBox
    Friend WithEvents lblScanPart As System.Windows.Forms.Label
    Friend WithEvents txtPartFromBox As System.Windows.Forms.TextBox
End Class
