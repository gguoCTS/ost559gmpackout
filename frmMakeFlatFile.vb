﻿Imports System.IO
Public Class frmMakeFlatFile

    Private Sub btnOk_Click(sender As Object, e As EventArgs) Handles btnOK.Click
        Dim blnFileExists As Boolean = False
        Dim fs As FileStream = Nothing
        Dim bw As StreamWriter = Nothing


        fs = New FileStream(strPackoutFlatFile, FileMode.Create)
        bw = New StreamWriter(fs)
        bw.WriteLine(txtScanData.Text)

        bw.Close()
        fs.Close()
        txtScanData.Text = ""
        Me.Hide()
    End Sub



    Private Sub frmMakeFlatFile_GotFocus(sender As Object, e As EventArgs) Handles Me.GotFocus
        Call UpdateFormLanguage()
        Call ClearData()
    End Sub



    Private Sub txtScanData_Click(sender As Object, e As EventArgs) Handles txtScanData.Click
        Call ClearData()

    End Sub

    Sub ClearData()
        Dim strData As String = ""

        strData = txtScanData.Text
        txtScanData.Text = ""
        If strData = "" Then Exit Sub
    End Sub

    Private Sub frmMakeFlatFile_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Call UpdateFormLanguage()
    End Sub
    Sub UpdateFormLanguage()
        btnOK.Text = dictPhrases("OK")
        lblEnterPackoutData.Text = dictPhrases("lblScanPart")
    End Sub
End Class