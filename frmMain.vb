Option Strict Off
Option Explicit On
Imports System.Configuration

Public Class frmMain
    Dim intLoop As Integer
    Dim frmPack2 As New frmPack 'Packout Form
    Dim frmPackWithScale2 As New frmPackWithScale 'Packout Form
    Dim frmListParts2 As New frmListParts
    Dim frmReprint2 As New frmReprint
    Dim frmRemoveParts2 As New frmRemoveParts

    'Dim frmPrintPartial2 As New frmPrintPartial_DELETE
    'Dim frmShipPartial2 As New frmShipPartial_DELETE
    'Dim frmUpdate2 As New frmUpdate_DELETE
    'Dim frmPartials2 As New frmPartials_DELETE

    Private Sub frmMain_Load_1(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            strComputerName = System.Environment.GetEnvironmentVariable("COMPUTERNAME")
            LoadConfigSettings()
            dbLocalSQL = New modDBLocalSQL
            dbSQLServer = New modDBSQLServer
            dbAS400 = New modDBAS400

            LoadLanguages(ConfigurationManager.AppSettings("DefaultLanguage"))

            If PrevInstance() Then
                DisplayUserMessage(UserMessageType.ApplicationError, dictPhrases("ProgramAlreadyRunning"))
                Application.Exit()
                End
            End If

            GetSystemProfiles()
            LoadModificationTypes()
            UpdateFormLanguage()
            UpdateDefinitionTablesFromServer()

            If My.Settings.UsesBOMSelectionForm Then
                frmPickBOM.ShowDialog()
            End If
            '  ToolStripStatusLabel1.Text = "Using " & Parts(intCurrentProfile).Name & " Profile"
            'Call LoadDontUseList()
            UpdateLabelBank()
            strBoxType = "Normal"  'TODO: Move this onto the form  02MAY2016 AMC
            PackWithScale.Visible = gBlnScale 'Display / hide button if implementation includes a scale. 

            If strCurrentBOM = String.Empty Then
                DisplayUserMessage(UserMessageType.InputError, dictPhrases("BOM_SelectionRequired"))
            End If
        Catch ex As Exception
            DisplayUserMessage(UserMessageType.ApplicationError, ex.Message)
        End Try
    End Sub

    Private Sub UpdateDefinitionTablesFromServer()
        Dim lclStrSqlServerName As String = ConfigurationManager.AppSettings("SQLServer")
        Dim lclStrSqlServerDatabaseName As String = ConfigurationManager.AppSettings("ServerDatabaseName")

        blnSqlServerRunning = dbSQLServer.TestLinkedServerConnection(strDBLOCALSQL, lclStrSqlServerName)
        If blnSqlServerRunning Then
            dbLocalSQL.UpdateSystemProfiles(lclStrSqlServerName, lclStrSqlServerDatabaseName)
            dbLocalSQL.UpdateBOMList(lclStrSqlServerName, lclStrSqlServerDatabaseName)
            dbLocalSQL.UpdateLabelData(lclStrSqlServerName, lclStrSqlServerDatabaseName)
        End If

    End Sub


    Function PrevInstance() As Boolean
        If UBound(Diagnostics.Process.GetProcessesByName _
           (Diagnostics.Process.GetCurrentProcess.ProcessName)) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function


    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Application.Exit()
    End Sub


    Sub LoadConfigSettings()
        Dim strTemp As String
        Dim strTemp02 As String
    '  Me.Text = ConfigurationManager.AppSettings("ProgramName")
    strDBSQLSERVER = ConfigurationManager.ConnectionStrings("dbNetworkSQLServer").ConnectionString
    strTemp = ConfigurationManager.ConnectionStrings("dbLocalSQLServer1").ConnectionString
        strTemp02 = ConfigurationManager.ConnectionStrings("dbLocalSQLServer2").ConnectionString
        strDBLOCALSQL = strTemp & strComputerName & strTemp02

        ' CTSSite = ConfigurationManager.AppSettings("CTSSite") '47 ' Elkhart
        WAV_Path = ConfigurationManager.AppSettings("SoundFilePath")
        strErrorSound = ConfigurationManager.AppSettings("ErrorSoundFile")
        strGoodSound = ConfigurationManager.AppSettings("GoodSoundFile")
        strAlreadyScannedSound = ConfigurationManager.AppSettings("AlreadyScannedSoundFile")
        strNowPrintingSound = ConfigurationManager.AppSettings("NowPrintingSoundFile")
        strLabelWriterPath = ConfigurationManager.AppSettings("LabelWriterPath")
        strPackoutFlatFile = ConfigurationManager.AppSettings("FlatFilePath")
        strAreYouSureSound = ConfigurationManager.AppSettings("AreYouSureSoundFile")
        strSqlServerName = ConfigurationManager.AppSettings("SQLServer")
        strRepackStation = ConfigurationManager.AppSettings("RepackStation")
        gBlnScale = Convert.ToBoolean(ConfigurationManager.AppSettings("Scale"))
    End Sub


    Private Sub TestGoodScanToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Call PlayGoodSound()
    End Sub


    Private Sub TestAlreadyScannedToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Call PlayAlreadyScannedSound()
    End Sub


    Private Sub TestIAmPrintingToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Call PlayIAmPrintingSound()
    End Sub


    'Private Sub TestSomethingsWrongToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Call PlaySomethingsWrongSound()

    'End Sub


    Private Sub PackPartsButton_ButtonClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PackPartsButton.Click
        'strBoxType = "Normal"
        If frmPack2.IsDisposed Then
            frmPack2 = New frmPack
        End If
        frmPack2.MdiParent = Me
        frmPack2.Show()
        frmPack2.BringToFront()

        'ToolStripStatusLabel1.Text = strLanguageWordsUsed(intLanguageSelected).p008_PackParts
        'If strBoxType = "Normal" Then
        '    ToolStripStatusLabel1.Text = ToolStripStatusLabel1.Text '& " " & strLanguageWordsUsed(intLanguageSelected).p026_NormalQuantity
        '    frmPack2.Text = strLanguageWordsUsed(intLanguageSelected).p075_PackoutApplication '& " " & strLanguageWordsUsed(intLanguageSelected).p026_NormalQuantity
        'Else
        '    ToolStripStatusLabel1.Text = ToolStripStatusLabel1.Text & " " & strLanguageWordsUsed(intLanguageSelected).p035_AlternateQuantity
        '    frmPack2.Text = strLanguageWordsUsed(intLanguageSelected).p075_PackoutApplication '& " " & strLanguageWordsUsed(intLanguageSelected).p035_AlternateQuantity
        'End If
    End Sub


    Private Sub ReprintLabelButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReprintLabelButton.Click
        If frmReprint2.IsDisposed Then
            frmReprint2 = New frmReprint
        End If
        frmReprint2.MdiParent = Me
        frmReprint2.Show()
        frmReprint2.BringToFront()

        ToolStripStatusLabel1.Text = dictPhrases("btnReprint")
    End Sub


    Private Sub ListPartsButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListPartsButton.Click
        If frmListParts2.IsDisposed Then
            frmListParts2 = New frmListParts
        End If
        frmListParts2.MdiParent = Me
        frmListParts2.Show()
        frmListParts2.BringToFront()

        ToolStripStatusLabel1.Text = dictPhrases("ListPartsButton")
    End Sub


    'Private Sub UpdateDataButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    '    If frmUpdate2.IsDisposed Then
    '        frmUpdate2 = New frmUpdate_DELETE
    '    End If
    '    frmUpdate2.MdiParent = Me
    '    frmUpdate2.Show()
    '    frmUpdate2.BringToFront()

    '    ToolStripStatusLabel1.Text = strLanguageWordsUsed(intLanguageSelected).p015_UpdateData ' Update Data Form"
    'End Sub


    'Private Sub CombineTwoPartialsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    'End Sub


    Private Sub TestAreYouSureToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Call PlayAreYouSureSound()
    End Sub


    Private Sub PickSiteCustomerPartAndLineToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PickSiteCustomerPartAndLineToolStripMenuItem.Click
        'frmPickProfile.ShowDialog()  Removed 05MAY2016 AMC
        'Call GetBOMListByProfile()   Removed 05MAY2016 AMC
        'If Parts(intCurrentProfile).UsesBOMSelectionForm Then
        If frmPack2.IsMdiChild Then
            frmPack2.Close()
        End If

        frmPickBOM.ShowDialog()

        If gBlnScale = False Then
            If frmPack2.IsDisposed Then
                frmPack2 = New frmPack
            End If
            frmPack2.MdiParent = Me
            frmPack2.Show()
            frmPack2.BringToFront()
        Else
            If frmPackWithScale2.IsDisposed Then
                frmPackWithScale2 = New frmPackWithScale
            End If
            frmPackWithScale2.MdiParent = Me
            frmPackWithScale2.Show()
            frmPackWithScale2.BringToFront()
        End If
        ' End If
    End Sub


    Private Sub CreatePackoutFlatFileToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CreatePackoutFlatFileToolStripMenuItem.Click
        frmMakeFlatFile.ShowDialog()
    End Sub


    Sub GetSystemProfiles()
        Dim blnResult As Boolean
        Dim dt As New DataTable
        '  Dim strResult As String
        'now local
        blnResult = dbLocalSQL.GetSystemProfiles(dt)

        intNumberOfProfiles = dt.Rows.Count
        ReDim Parts(intNumberOfProfiles)

        For intLoop = 1 To intNumberOfProfiles
            Parts(intLoop).ID = dt.Rows(intLoop - 1).Item("ID").ToString
            Parts(intLoop).Name = dt.Rows(intLoop - 1).Item("Name") 'ConfigurationManager.AppSettings("Profile" & intLoop & "Name")
            Parts(intLoop).PartEntryMethod = Trim(dt.Rows(intLoop - 1).Item("PartEntryMethod")) 'ConfigurationManager.AppSettings("Profile" & intLoop & "PartEntryMethod")
            Parts(intLoop).CTSSite = dt.Rows(intLoop - 1).Item("CTSSite") 'ConfigurationManager.AppSettings("Profile" & intLoop & "CTSSite")
            Parts(intLoop).MFGLoc = dt.Rows(intLoop - 1).Item("MFGLoc") ' ConfigurationManager.AppSettings("Profile" & intLoop & "MFGLoc")
            Parts(intLoop).SampleBarcode = dt.Rows(intLoop - 1).Item("SampleBarcode")
            Parts(intLoop).BCDateStart = dt.Rows(intLoop - 1).Item("BCDateStart") ' ConfigurationManager.AppSettings("Profile" & intLoop & "BCDateStart")
            Parts(intLoop).BCDateLength = dt.Rows(intLoop - 1).Item("BCDateLength") 'ConfigurationManager.AppSettings("Profile" & intLoop & "BCDateLength")
            Parts(intLoop).BCSerialStart = dt.Rows(intLoop - 1).Item("BCSerialStart") ' ConfigurationManager.AppSettings("Profile" & intLoop & "BCSerialStart")
            Parts(intLoop).BCSerialLength = dt.Rows(intLoop - 1).Item("BCSerialLength") 'ConfigurationManager.AppSettings("Profile" & intLoop & "BCSerialLength")
            Parts(intLoop).BCPartNumStart = dt.Rows(intLoop - 1).Item("BCPartNumStart") ' ConfigurationManager.AppSettings("Profile" & intLoop & "BCPartNumStart")
            Parts(intLoop).BCPartNumLength = dt.Rows(intLoop - 1).Item("BCPartNumLength") 'ConfigurationManager.AppSettings("Profile" & intLoop & "BCPartNumLength")
            Parts(intLoop).BCStyle = dt.Rows(intLoop - 1).Item("BCStyle") 'ConfigurationManager.AppSettings("Profile" & intLoop & "BCStyle")
            Parts(intLoop).BoxPrefix = dt.Rows(intLoop - 1).Item("BoxPrefix") ' ConfigurationManager.AppSettings("Profile" & intLoop & "BoxPrefix")
            Parts(intLoop).BoxNumberLength = dt.Rows(intLoop - 1).Item("BoxNumberLength") ' ConfigurationManager.AppSettings("Profile" & intLoop & "BoxNumberLength")
            ' strResult = dt.Rows(intLoop - 1).Item("UsesBOMSelectionForm")

            'If strResult = "True" Or strResult = "Yes" Then
            '  Parts(intLoop).UsesBOMSelectionForm = True
            'Else
            '  Parts(intLoop).UsesBOMSelectionForm = False
            'End If
        Next

    End Sub


    Private Sub LoadModificationTypes()
        Dim intLoop As Integer
        Dim blnResult As Boolean

        Dim dt As New DataTable

        blnResult = dbSQLServer.GetModificationTypes(dt)

        ReDim ModificationTypes(dt.Rows.Count)
        For intLoop = 0 To dt.Rows.Count - 1
            ModificationTypes(intLoop + 1) = dt.Rows(intLoop).Item("ModificationType")
        Next

    End Sub



    Private Sub Language_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Language.Click
        Dim intLanguageSelected As Integer

        frmPickLanguage.ShowDialog()
        intLanguageSelected = frmPickLanguage.LanguageSelected
        If intLanguageSelected > 0 Then
            LoadLanguages(intLanguageSelected)
        End If
        UpdateFormLanguage()
    End Sub


    Sub UpdateFormLanguage()
        'ToolStripStatusLabel1.Text = strLanguageWordsUsed(intLanguageSelected).p069_Using & Parts(intCurrentProfile).Name &
        '                                strLanguageWordsUsed(intLanguageSelected).p070_Profile

        'ToolStripStatusLabel2.Text = strLanguageWordsUsed(intLanguageSelected).p071_PleaseSelectAnActionFromAbove

        Me.Text = dictPhrases("frmMain")
        lblBOMDisplay.Text = dictPhrases("BOM") + Space(2) + strCurrentBOM
        PackPartsButton.Text = dictPhrases("PackPartsButton")
        ReprintLabelButton.Text = dictPhrases("btnReprint")
        btnRemovePart.Text = dictPhrases("btnRemovePart")
        ListPartsButton.Text = dictPhrases("ListPartsButton")
        Language.Text = dictPhrases("Language")
        ExitToolStripMenuItem.Text = dictPhrases("Close")
        EditToolStripMenuItem.Text = "Edit"
        PickSiteCustomerPartAndLineToolStripMenuItem.Text = dictPhrases("frmPickBOM")
        CreatePackoutFlatFileToolStripMenuItem.Text = dictPhrases("frmMakeFlatFile")
        BOM_MaintenanceToolStripMenuItem.Text = dictPhrases("frmBOM_LookupMaintenance")
        'AlternateQuantityToolStripMenuItem.Text = strLanguageWordsUsed(intLanguageSelected).p035_AlternateQuantity
        'PrintPartial.Text = strLanguageWordsUsed(intLanguageSelected).p009_PrintPartial
        'ShipPartialToolStripMenuItem.Text = strLanguageWordsUsed(intLanguageSelected).p040_ShipPartial
        'ReplacePartWithScrap.Text = strLanguageWordsUsed(intLanguageSelected).p010_RemoveAllPartsFromPartial  'TODO: Translate
        'UnpackBoxButton.Text = strLanguageWordsUsed(intLanguageSelected).p012_UnpackBox
        'UnpackPartFromBoxToolStripMenuItem.Text = strLanguageWordsUsed(intLanguageSelected).p056_UnpackPartFromBox
        'ClearPartToBoxAssociationToolStripMenuItem.Text = strLanguageWordsUsed(intLanguageSelected).p057_ClearPartToBoxAssociation
        'MarkPartAsScrapToolStripMenuItem.Text = strLanguageWordsUsed(intLanguageSelected).p058_MarkPartAsScrapClearPartFromDatabase
        'ClearPartFromDatabaseToolStripMenuItem.Text = strLanguageWordsUsed(intLanguageSelected).p057_ClearPartToBoxAssociation & " " & strLanguageWordsUsed(intLanguageSelected).p059_AllowingItToBeJoinedToAnotherBox
        'ListPartialBoxesToolStripMenuItem.Text = strLanguageWordsUsed(intLanguageSelected).p053_ThereIsAPartialBoxAvailableToUseForThisBillOfMaterials
        'UpdateButton.Text = strLanguageWordsUsed(intLanguageSelected).p015_UpdateData
        'FileToolStripMenuItem.Text = strLanguageWordsUsed(intLanguageSelected).p006_File
        'TestSoundsToolStripMenuItem.Text = strLanguageWordsUsed(intLanguageSelected).p016_TestSounds
        'CreatePsuedoWaybillToolStripMenuItem.Text = strLanguageWordsUsed(intLanguageSelected).p025_CreatePsuedoWaybill

        'Removed 04MAY2016  AMC
        'AS400StatusToolStripMenuItem.Text = strLanguageWordsUsed(intLanguageSelected).p073_AS400Status & blnAS400Running
        'SQLServerStatusToolStripMenuItem.Text = strLanguageWordsUsed(intLanguageSelected).p074_SQLServerStatus & blnSqlServerRunning
        Select Case intLanguageSelected
            Case 1
                Language.Image = btnAmericanFlag.Image
            Case 2
                Language.Image = btnMexicanFlag.Image
            Case 3
                Language.Image = btnCzechFlag.Image
            Case 4
                Language.Image = btnChineseFlag.Image
        End Select
    End Sub


    Private Sub btnRemovePart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemovePart.Click
        If gBlnScale = False Then
            If frmPack2.IsMdiChild Then
                DisplayUserMessage(UserMessageType.Information, dictPhrases("PackoutFormMustBeClosedToRemoveParts"))
            Else
                If frmRemoveParts2.IsDisposed Then
                    frmRemoveParts2 = New frmRemoveParts
                End If

                frmRemoveParts2.MdiParent = Me
                'frmRemoveParts.ReplacementType = "1" 'Scrap Part and Replace in Box
                frmRemoveParts2.Show()
                frmRemoveParts2.BringToFront()
            End If
        Else
            If frmPackWithScale2.IsMdiChild Then
                DisplayUserMessage(UserMessageType.Information, dictPhrases("PackoutFormMustBeClosedToRemoveParts"))
            Else
                If frmRemoveParts2.IsDisposed Then
                    frmRemoveParts2 = New frmRemoveParts
                End If

                frmRemoveParts2.MdiParent = Me
                'frmRemoveParts.ReplacementType = "1" 'Scrap Part and Replace in Box
                frmRemoveParts2.Show()
                frmRemoveParts2.BringToFront()
            End If
        End If
    End Sub

    Private Sub PackWithScale_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PackWithScale.Click
        strBoxType = "Normal"

        If frmPackWithScale2.IsDisposed Then
            frmPackWithScale2 = New frmPackWithScale
        End If
        frmPackWithScale2.MdiParent = Me
        frmPackWithScale2.Show()
        frmPackWithScale.BringToFront()

        ToolStripStatusLabel1.Text = dictPhrases("PackParts")
        If strBoxType = "Normal" Then
            'ToolStripStatusLabel1.Text = ToolStripStatusLabel1.Text '& " " & strLanguageWordsUsed(intLanguageSelected).p026_NormalQuantity
            frmPackWithScale2.Text = dictPhrases("PackParts") '& " " & strLanguageWordsUsed(intLanguageSelected).p026_NormalQuantity
        Else
            'ToolStripStatusLabel1.Text = ToolStripStatusLabel1.Text '& " " & strLanguageWordsUsed(intLanguageSelected).p035_AlternateQuantity
            frmPackWithScale2.Text = dictPhrases("PackParts") '& " " & strLanguageWordsUsed(intLanguageSelected).p035_AlternateQuantity
        End If
    End Sub

    Private Sub BOM_MaintenanceToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BOM_MaintenanceToolStripMenuItem.Click
        frmBOM_LookupMaintenance.ShowDialog()
    End Sub


    'Private Sub ListPartialBoxesToolStripMenuItem_Click(sender As Object, e As EventArgs)
    '    If frmPartials2.IsDisposed Then
    '        frmPartials2 = New frmPartials_DELETE
    '    End If
    '    frmPartials2.MdiParent = Me
    '    frmPartials2.Show()
    '    frmPartials2.BringToFront()

    '    ToolStripStatusLabel1.Text = strLanguageWordsUsed(intLanguageSelected).p053_ThereIsAPartialBoxAvailableToUseForThisBillOfMaterials
    'End Sub

    'Private Sub SetTestModeToolStripMenuItem_Click(sender As Object, e As EventArgs)

    '    frmPasswordDialog.ShowDialog()

    '    If frmPasswordDialog.DialogResult = Windows.Forms.DialogResult.OK Then

    '        If gblTestModeEnabled Then
    '            gblTestModeEnabled = False

    '            'SetTestModeToolStripMenuItem.Checked = False
    '            ' Call SetColors()
    '            '  lblTestStatusBits.Visible = True
    '            '  lblTestStatusBitsEnabled.Visible = True
    '        Else
    '            gblTestModeEnabled = True

    '            'SetTestModeToolStripMenuItem.Checked = True
    '            '    Call SetColors()
    '            ' lblTestStatusBits.Visible = False
    '            ' lblTestStatusBitsEnabled.Visible = False
    '        End If
    '    End If
    'End Sub


    'Private Sub ListPartsButton_ButtonClick(sender As Object, e As EventArgs)

    '    If frmListParts2.IsDisposed Then
    '        frmListParts2 = New frmListParts
    '    End If
    '    frmListParts2.MdiParent = Me
    '    frmListParts2.Show()
    '    frmListParts2.BringToFront()

    '    ToolStripStatusLabel1.Text = strLanguageWordsUsed(intLanguageSelected).p014_ListParts
    'End Sub


    'Private Sub ReplacePartWithScrap_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ReplacePartWithScrap.Click
    '    If frmReplacePart2.IsDisposed Then
    '        frmReplacePart2 = New frmReplacePart
    '    End If

    '    frmReplacePart2.MdiParent = Me
    '    frmReplacePart.ReplacementType = "1" 'Scrap Part and Replace in Box
    '    frmReplacePart2.Show()
    '    frmReplacePart2.BringToFront()

    '    ToolStripStatusLabel1.Text = "SCRAP PART WITH REPLACE"
    'End Sub


    'Private Sub PrintPartial_ButtonClick(ByVal sender As Object, ByVal e As EventArgs) Handles PrintPartial.ButtonClick

    '    'If frmPrintPartial2.IsDisposed Then
    '    '    frmPrintPartial2 = New frmPrintPartial
    '    'End If
    '    'frmPrintPartial2.MdiParent = Me
    '    'frmPrintPartial2.Show()
    '    'frmPrintPartial2.BringToFront()

    '    'ToolStripStatusLabel1.Text = strLanguageWordsUsed(intLanguageSelected).p009_PrintPartial '"Showing Print Partial Form"
    '    Dim blnResult As Boolean
    '    Dim strBOMSelected As String
    '    Dim intQtySelected As Integer
    '    Dim tmpForm As Form

    '    If frmPack2.IsMdiChild Then
    '        tmpForm = Me.ActiveMdiChild
    '        strBOMSelected = tmpForm.Controls(0).Controls(0).Controls("lblBomScannedValue").Text
    '        intQtySelected = Val(tmpForm.Controls(0).Controls(0).Controls("lblPartsScannedQty").Text)

    '        If strBOMSelected = "" Then
    '            PlaySomethingsWrongSound()
    '            MessageBox.Show("BOM Required. ", UserMessageType.ApplicationError, MessageBoxButtons.OK, MessageBoxIcon.Error)
    '        Else
    '            If intQtySelected < 1 Then
    '                PlaySomethingsWrongSound()
    '                MessageBox.Show("Parts Scanned Required. ", UserMessageType.ApplicationError, MessageBoxButtons.OK, MessageBoxIcon.Error)
    '            Else
    '                blnResult = CompleteBox(strCurrentCartonNumber, False, strBOMSelected, intQtySelected)
    '            End If

    '        End If

    '    Else
    '        PlaySomethingsWrongSound()
    '        MessageBox.Show("frmPack Not Loaded.  Click on Pack Parts Button. ", UserMessageType.ApplicationError, MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End If


    'End Sub


    'Private Sub MarkPartAsScrapToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles MarkPartAsScrapToolStripMenuItem.Click
    '    If frmRemoveSinglePartFromBox2.IsDisposed Then
    '        frmRemoveSinglePartFromBox2 = New frmRemoveSinglePartFromBox_DELETE
    '    End If
    '    frmRemoveSinglePartFromBox2.MdiParent = Me
    '    frmRemoveSinglePartFromBox2.Show()
    '    frmRemoveSinglePartFromBox2.BringToFront()
    '    frmRemoveSinglePartFromBox2.Text = strLanguageWordsUsed(intLanguageSelected).p076_RemoveASinglePartFromBox & ": " & strLanguageWordsUsed(intLanguageSelected).p058_MarkPartAsScrapClearPartFromDatabase
    '    ToolStripStatusLabel1.Text = strLanguageWordsUsed(intLanguageSelected).p076_RemoveASinglePartFromBox
    '    strRemovePartial = "MarkPartAsScrap"
    'End Sub

    'Private Sub ClearPartFromDatabaseToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ClearPartFromDatabaseToolStripMenuItem.Click
    '    If frmRemoveSinglePartFromBox2.IsDisposed Then
    '        frmRemoveSinglePartFromBox2 = New frmRemoveSinglePartFromBox_DELETE
    '    End If
    '    frmRemoveSinglePartFromBox2.MdiParent = Me
    '    frmRemoveSinglePartFromBox2.Show()
    '    frmRemoveSinglePartFromBox2.BringToFront()
    '    frmRemoveSinglePartFromBox2.Text = strLanguageWordsUsed(intLanguageSelected).p076_RemoveASinglePartFromBox & "; " & strLanguageWordsUsed(intLanguageSelected).p057_ClearPartToBoxAssociation
    '    ToolStripStatusLabel1.Text = strLanguageWordsUsed(intLanguageSelected).p076_RemoveASinglePartFromBox
    '    strRemovePartial = "ClearPartfromDatabase"
    'End Sub

    'Private Sub ReplacePartButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    If frmReplacePart2.IsDisposed Then
    '        frmReplacePart2 = New frmReplacePart_DELETE
    '    End If

    '    frmReplacePart2.MdiParent = Me
    '    frmReplacePart_DELETE.ReplacementType = "2" 'Replace Only
    '    frmReplacePart2.Show()
    '    frmReplacePart2.BringToFront()

    '    ToolStripStatusLabel1.Text = strLanguageWordsUsed(intLanguageSelected).p013_ReplacePart
    'End Sub

    'Private Sub ClearPartToBoxAssociationToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ClearPartToBoxAssociationToolStripMenuItem.Click
    '    'remove part from box
    '    If frmRemoveSinglePartFromBox2.IsDisposed Then
    '        frmRemoveSinglePartFromBox2 = New frmRemoveSinglePartFromBox_DELETE
    '    End If
    '    frmRemoveSinglePartFromBox2.MdiParent = Me
    '    frmRemoveSinglePartFromBox2.Show()
    '    frmRemoveSinglePartFromBox2.BringToFront()

    '    frmRemoveSinglePartFromBox2.Text = strLanguageWordsUsed(intLanguageSelected).p076_RemoveASinglePartFromBox & ": " & strLanguageWordsUsed(intLanguageSelected).p057_ClearPartToBoxAssociation
    '    ToolStripStatusLabel1.Text = strLanguageWordsUsed(intLanguageSelected).p076_RemoveASinglePartFromBox
    '    strRemovePartial = "ClearAssociation"
    'End Sub


    'Private Sub RemoveAllFromPartial_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ReplacePartWithScrap.Click

    '    If frmRemovePartial2.IsDisposed Then
    '        frmRemovePartial2 = New frmRemovePartial
    '    End If
    '    frmRemovePartial2.MdiParent = Me
    '    frmRemovePartial2.Show()
    '    frmRemovePartial2.BringToFront()

    '    ToolStripStatusLabel1.Text = strLanguageWordsUsed(intLanguageSelected).p010_RemoveAllPartsFromPartial
    'End Sub


    'GetBOMList Function Not used in application  AMC 15 APR 2016
    'Sub GetBOMList()
    '    Dim dtData As DataTable

    '    dtData = dbLocalSQL.GetBOMList

    '    For intLoop = 1 To dtData.Rows.Count - 1
    '        ReDim Preserve BOMList(intLoop)
    '        BOMList(intLoop).CustomerNumber = dtData.Rows(intLoop).Item("CustomerNumber").ToString
    '        BOMList(intLoop).BOM = dtData.Rows(intLoop).Item("BOM").ToString
    '        BOMList(intLoop).ScannedPartNumber = dtData.Rows(intLoop).Item("ScannedPartNumber").ToString
    '        BOMList(intLoop).DrawingNumber = dtData.Rows(intLoop).Item("DrawingNumber").ToString
    '        BOMList(intLoop).SystemProfileID = dtData.Rows(intLoop).Item("SystemProfileID").ToString
    '    Next

    'End Sub
End Class