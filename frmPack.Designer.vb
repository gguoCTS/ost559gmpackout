<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPack
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.dgPackingList = New System.Windows.Forms.DataGridView()
        Me.lblPackingList = New System.Windows.Forms.Label()
        Me.lblBoxQty = New System.Windows.Forms.Label()
        Me.lblPartsScannedQty = New System.Windows.Forms.Label()
        Me.lblBomScannedValue = New System.Windows.Forms.Label()
        Me.lblDateCodeScannedValue = New System.Windows.Forms.Label()
        Me.lblSerialNumberScannedValue = New System.Windows.Forms.Label()
        Me.lblStdCartonQuantity = New System.Windows.Forms.Label()
        Me.lblScanPart = New System.Windows.Forms.Label()
        Me.txtScanData = New System.Windows.Forms.TextBox()
        Me.lblQuantityPackedHdr = New System.Windows.Forms.Label()
        Me.lblBOMScanned = New System.Windows.Forms.Label()
        Me.lblDateCodeScanned = New System.Windows.Forms.Label()
        Me.lblSerialNumberScanned = New System.Windows.Forms.Label()
        Me.pnlBG = New System.Windows.Forms.Panel()
        Me.lblStdContainerQty = New System.Windows.Forms.Label()
        Me.cboStandardBoxQty = New System.Windows.Forms.ComboBox()
        Me.tmrCheckForFile = New System.Windows.Forms.Timer(Me.components)
        Me.btnShipPartial = New System.Windows.Forms.Button()
        Me.btnSavePartial = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        CType(Me.dgPackingList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlBG.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.dgPackingList)
        Me.Panel1.Controls.Add(Me.lblPackingList)
        Me.Panel1.Controls.Add(Me.lblBoxQty)
        Me.Panel1.Controls.Add(Me.lblPartsScannedQty)
        Me.Panel1.Controls.Add(Me.lblBomScannedValue)
        Me.Panel1.Controls.Add(Me.lblDateCodeScannedValue)
        Me.Panel1.Controls.Add(Me.lblSerialNumberScannedValue)
        Me.Panel1.Controls.Add(Me.lblStdCartonQuantity)
        Me.Panel1.Controls.Add(Me.lblScanPart)
        Me.Panel1.Controls.Add(Me.txtScanData)
        Me.Panel1.Controls.Add(Me.lblQuantityPackedHdr)
        Me.Panel1.Controls.Add(Me.lblBOMScanned)
        Me.Panel1.Controls.Add(Me.lblDateCodeScanned)
        Me.Panel1.Controls.Add(Me.lblSerialNumberScanned)
        Me.Panel1.Location = New System.Drawing.Point(25, 46)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1255, 438)
        Me.Panel1.TabIndex = 13
        '
        'dgPackingList
        '
        Me.dgPackingList.AllowUserToAddRows = False
        Me.dgPackingList.AllowUserToDeleteRows = False
        Me.dgPackingList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgPackingList.Location = New System.Drawing.Point(695, 53)
        Me.dgPackingList.Name = "dgPackingList"
        Me.dgPackingList.ReadOnly = True
        Me.dgPackingList.RowTemplate.Height = 24
        Me.dgPackingList.Size = New System.Drawing.Size(523, 367)
        Me.dgPackingList.TabIndex = 31
        Me.dgPackingList.TabStop = False
        '
        'lblPackingList
        '
        Me.lblPackingList.AutoSize = True
        Me.lblPackingList.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPackingList.Location = New System.Drawing.Point(731, 25)
        Me.lblPackingList.Name = "lblPackingList"
        Me.lblPackingList.Size = New System.Drawing.Size(117, 25)
        Me.lblPackingList.TabIndex = 30
        Me.lblPackingList.Text = "Packing List"
        '
        'lblBoxQty
        '
        Me.lblBoxQty.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lblBoxQty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBoxQty.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBoxQty.Font = New System.Drawing.Font("Arial", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBoxQty.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBoxQty.Location = New System.Drawing.Point(421, 334)
        Me.lblBoxQty.Name = "lblBoxQty"
        Me.lblBoxQty.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBoxQty.Size = New System.Drawing.Size(137, 48)
        Me.lblBoxQty.TabIndex = 29
        Me.lblBoxQty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblPartsScannedQty
        '
        Me.lblPartsScannedQty.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lblPartsScannedQty.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblPartsScannedQty.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblPartsScannedQty.Font = New System.Drawing.Font("Arial", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPartsScannedQty.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblPartsScannedQty.Location = New System.Drawing.Point(88, 339)
        Me.lblPartsScannedQty.Name = "lblPartsScannedQty"
        Me.lblPartsScannedQty.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblPartsScannedQty.Size = New System.Drawing.Size(137, 48)
        Me.lblPartsScannedQty.TabIndex = 28
        Me.lblPartsScannedQty.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBomScannedValue
        '
        Me.lblBomScannedValue.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lblBomScannedValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBomScannedValue.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBomScannedValue.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBomScannedValue.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBomScannedValue.Location = New System.Drawing.Point(289, 191)
        Me.lblBomScannedValue.Name = "lblBomScannedValue"
        Me.lblBomScannedValue.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBomScannedValue.Size = New System.Drawing.Size(255, 33)
        Me.lblBomScannedValue.TabIndex = 27
        '
        'lblDateCodeScannedValue
        '
        Me.lblDateCodeScannedValue.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lblDateCodeScannedValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblDateCodeScannedValue.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDateCodeScannedValue.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateCodeScannedValue.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDateCodeScannedValue.Location = New System.Drawing.Point(289, 140)
        Me.lblDateCodeScannedValue.Name = "lblDateCodeScannedValue"
        Me.lblDateCodeScannedValue.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDateCodeScannedValue.Size = New System.Drawing.Size(255, 30)
        Me.lblDateCodeScannedValue.TabIndex = 26
        '
        'lblSerialNumberScannedValue
        '
        Me.lblSerialNumberScannedValue.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.lblSerialNumberScannedValue.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblSerialNumberScannedValue.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSerialNumberScannedValue.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSerialNumberScannedValue.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSerialNumberScannedValue.Location = New System.Drawing.Point(289, 91)
        Me.lblSerialNumberScannedValue.Name = "lblSerialNumberScannedValue"
        Me.lblSerialNumberScannedValue.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSerialNumberScannedValue.Size = New System.Drawing.Size(255, 33)
        Me.lblSerialNumberScannedValue.TabIndex = 25
        '
        'lblStdCartonQuantity
        '
        Me.lblStdCartonQuantity.BackColor = System.Drawing.SystemColors.Control
        Me.lblStdCartonQuantity.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblStdCartonQuantity.Font = New System.Drawing.Font("Arial", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStdCartonQuantity.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblStdCartonQuantity.Location = New System.Drawing.Point(351, 303)
        Me.lblStdCartonQuantity.Name = "lblStdCartonQuantity"
        Me.lblStdCartonQuantity.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblStdCartonQuantity.Size = New System.Drawing.Size(305, 28)
        Me.lblStdCartonQuantity.TabIndex = 23
        Me.lblStdCartonQuantity.Text = "Standard Carton Quantity"
        Me.lblStdCartonQuantity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblScanPart
        '
        Me.lblScanPart.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScanPart.Location = New System.Drawing.Point(29, 39)
        Me.lblScanPart.Name = "lblScanPart"
        Me.lblScanPart.Size = New System.Drawing.Size(250, 25)
        Me.lblScanPart.TabIndex = 18
        Me.lblScanPart.Text = "Scan Part"
        '
        'txtScanData
        '
        Me.txtScanData.AcceptsReturn = True
        Me.txtScanData.BackColor = System.Drawing.SystemColors.Window
        Me.txtScanData.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtScanData.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtScanData.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtScanData.Location = New System.Drawing.Point(289, 39)
        Me.txtScanData.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtScanData.MaxLength = 0
        Me.txtScanData.Name = "txtScanData"
        Me.txtScanData.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtScanData.Size = New System.Drawing.Size(388, 26)
        Me.txtScanData.TabIndex = 1
        '
        'lblQuantityPackedHdr
        '
        Me.lblQuantityPackedHdr.BackColor = System.Drawing.SystemColors.Control
        Me.lblQuantityPackedHdr.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblQuantityPackedHdr.Font = New System.Drawing.Font("Arial", 13.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblQuantityPackedHdr.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblQuantityPackedHdr.Location = New System.Drawing.Point(55, 306)
        Me.lblQuantityPackedHdr.Name = "lblQuantityPackedHdr"
        Me.lblQuantityPackedHdr.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblQuantityPackedHdr.Size = New System.Drawing.Size(213, 28)
        Me.lblQuantityPackedHdr.TabIndex = 17
        Me.lblQuantityPackedHdr.Text = "Quantity Packed "
        Me.lblQuantityPackedHdr.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblBOMScanned
        '
        Me.lblBOMScanned.BackColor = System.Drawing.SystemColors.Control
        Me.lblBOMScanned.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblBOMScanned.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBOMScanned.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblBOMScanned.Location = New System.Drawing.Point(31, 191)
        Me.lblBOMScanned.Name = "lblBOMScanned"
        Me.lblBOMScanned.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblBOMScanned.Size = New System.Drawing.Size(250, 25)
        Me.lblBOMScanned.TabIndex = 16
        Me.lblBOMScanned.Text = "BOM Scanned: "
        '
        'lblDateCodeScanned
        '
        Me.lblDateCodeScanned.BackColor = System.Drawing.SystemColors.Control
        Me.lblDateCodeScanned.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblDateCodeScanned.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDateCodeScanned.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblDateCodeScanned.Location = New System.Drawing.Point(31, 140)
        Me.lblDateCodeScanned.Name = "lblDateCodeScanned"
        Me.lblDateCodeScanned.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblDateCodeScanned.Size = New System.Drawing.Size(250, 25)
        Me.lblDateCodeScanned.TabIndex = 14
        Me.lblDateCodeScanned.Text = "Date Code Scanned "
        '
        'lblSerialNumberScanned
        '
        Me.lblSerialNumberScanned.BackColor = System.Drawing.SystemColors.Control
        Me.lblSerialNumberScanned.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSerialNumberScanned.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSerialNumberScanned.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSerialNumberScanned.Location = New System.Drawing.Point(31, 91)
        Me.lblSerialNumberScanned.Name = "lblSerialNumberScanned"
        Me.lblSerialNumberScanned.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSerialNumberScanned.Size = New System.Drawing.Size(250, 25)
        Me.lblSerialNumberScanned.TabIndex = 13
        Me.lblSerialNumberScanned.Text = "Serial Number Scanned "
        '
        'pnlBG
        '
        Me.pnlBG.Controls.Add(Me.lblStdContainerQty)
        Me.pnlBG.Controls.Add(Me.cboStandardBoxQty)
        Me.pnlBG.Controls.Add(Me.Panel1)
        Me.pnlBG.Location = New System.Drawing.Point(3, 11)
        Me.pnlBG.Margin = New System.Windows.Forms.Padding(4)
        Me.pnlBG.Name = "pnlBG"
        Me.pnlBG.Size = New System.Drawing.Size(1310, 516)
        Me.pnlBG.TabIndex = 14
        '
        'lblStdContainerQty
        '
        Me.lblStdContainerQty.AutoSize = True
        Me.lblStdContainerQty.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStdContainerQty.Location = New System.Drawing.Point(342, 7)
        Me.lblStdContainerQty.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblStdContainerQty.Name = "lblStdContainerQty"
        Me.lblStdContainerQty.Size = New System.Drawing.Size(149, 25)
        Me.lblStdContainerQty.TabIndex = 33
        Me.lblStdContainerQty.Text = "Carton Quantity"
        Me.lblStdContainerQty.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cboStandardBoxQty
        '
        Me.cboStandardBoxQty.DisplayMember = "BoxQtyLabel"
        Me.cboStandardBoxQty.FormattingEnabled = True
        Me.cboStandardBoxQty.Location = New System.Drawing.Point(639, 9)
        Me.cboStandardBoxQty.Margin = New System.Windows.Forms.Padding(4)
        Me.cboStandardBoxQty.Name = "cboStandardBoxQty"
        Me.cboStandardBoxQty.Size = New System.Drawing.Size(300, 24)
        Me.cboStandardBoxQty.TabIndex = 32
        Me.cboStandardBoxQty.TabStop = False
        Me.cboStandardBoxQty.ValueMember = "BoxQuantity"
        '
        'tmrCheckForFile
        '
        Me.tmrCheckForFile.Interval = 1500
        '
        'btnShipPartial
        '
        Me.btnShipPartial.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnShipPartial.Location = New System.Drawing.Point(648, 532)
        Me.btnShipPartial.Margin = New System.Windows.Forms.Padding(4)
        Me.btnShipPartial.Name = "btnShipPartial"
        Me.btnShipPartial.Size = New System.Drawing.Size(340, 50)
        Me.btnShipPartial.TabIndex = 17
        Me.btnShipPartial.Text = "Send to Shipping"
        Me.btnShipPartial.UseVisualStyleBackColor = True
        '
        'btnSavePartial
        '
        Me.btnSavePartial.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSavePartial.Location = New System.Drawing.Point(76, 532)
        Me.btnSavePartial.Margin = New System.Windows.Forms.Padding(4)
        Me.btnSavePartial.Name = "btnSavePartial"
        Me.btnSavePartial.Size = New System.Drawing.Size(340, 49)
        Me.btnSavePartial.TabIndex = 16
        Me.btnSavePartial.Text = "Save Partial"
        Me.btnSavePartial.UseVisualStyleBackColor = True
        '
        'frmPack
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1315, 589)
        Me.Controls.Add(Me.btnShipPartial)
        Me.Controls.Add(Me.btnSavePartial)
        Me.Controls.Add(Me.pnlBG)
        Me.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.Name = "frmPack"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Pack Parts Form"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.dgPackingList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlBG.ResumeLayout(False)
        Me.pnlBG.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblScanPart As System.Windows.Forms.Label
    Public WithEvents txtScanData As System.Windows.Forms.TextBox
    Public WithEvents lblQuantityPackedHdr As System.Windows.Forms.Label
    Public WithEvents lblBOMScanned As System.Windows.Forms.Label
    Public WithEvents lblDateCodeScanned As System.Windows.Forms.Label
    Public WithEvents lblSerialNumberScanned As System.Windows.Forms.Label
    Public WithEvents lblStdCartonQuantity As System.Windows.Forms.Label
    Friend WithEvents pnlBG As System.Windows.Forms.Panel
    Friend WithEvents tmrCheckForFile As System.Windows.Forms.Timer
    Public WithEvents lblBomScannedValue As Label
    Public WithEvents lblDateCodeScannedValue As Label
    Public WithEvents lblSerialNumberScannedValue As Label
    Public WithEvents lblBoxQty As System.Windows.Forms.Label
    Public WithEvents lblPartsScannedQty As System.Windows.Forms.Label
    Friend WithEvents btnShipPartial As System.Windows.Forms.Button
    Friend WithEvents btnSavePartial As System.Windows.Forms.Button
    Friend WithEvents lblPackingList As System.Windows.Forms.Label
    Friend WithEvents lblStdContainerQty As System.Windows.Forms.Label
    Friend WithEvents cboStandardBoxQty As System.Windows.Forms.ComboBox
    Friend WithEvents dgPackingList As System.Windows.Forms.DataGridView
End Class
