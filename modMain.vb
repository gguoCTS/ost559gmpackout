Imports System.Configuration
Imports VB = Microsoft.VisualBasic
Imports System.Collections.Generic
Imports System.IO

Module modMain
    Public dbLocalSQL As modDBLocalSQL
    Public dbSQLServer As modDBSQLServer
    Public dbAS400 As modDBAS400
    Public intCartonCount As Integer
    Public strMaxDateCode As String
    Public strMinDateCode As String
    Public strDateCode As String
    Public strBoxType As String
    Public WithEvents KillTimer As New Timer
    Public gblnKillTimeDone As Boolean
    Public strLabelInfo As String
    Public strRemovePartial As String
    Public ModificationTypes() As String
    Public strComputerName As String
    Public blnSqlServerRunning As Boolean
    Public blnAS400Running As Boolean
    Public strDontUseArray() As String
    Public strLanguageUsed As String = "English"
    Public intLanguageSelected As Integer = 1
    Public gBlnScale As Boolean = False
    Public dictPhrases As New Dictionary(Of String, String)()
    Public LabelRIGHT As Object = Nothing

    Public Structure LabelDat
        Dim Qty As String
        Dim BOM As String
        Dim CustPartNum As String
        Dim CustPartNum2 As String
        Dim Supplier_ID As String
        Dim LabelDesc1 As String
        Dim LabelDesc2 As String
        Dim LabelDesc3 As String
        Dim ECN As String
        Dim BoxLabelFormat As String
        Dim LabelCarton As String
        Dim DockLocation As String
        Dim DropZone As String
        Dim CartonSerialNumber As String
        Dim Cust_No As String
        Dim DateCode As String
    End Structure

    Enum UserMessageType
        ApplicationError
        DatabaseConnectionError
        InputError
        Information
    End Enum


    Public LabelData As LabelDat
    Public BoxInfo As LabelDat

    Public PreviousBOM As String
    Public gstrTempData As String

    Sub PlayGoodSound()
        With frmMain.AxWindowsMediaPlayer1
            .URL = WAV_Path & "\" & strLanguageUsed & "\" & strGoodSound
            'killtime(100)
            '.Ctlcontrols.play()
        End With
    End Sub

    Sub PlayAlreadyScannedSound()

        With frmMain.AxWindowsMediaPlayer1
            .URL = WAV_Path & "\" & strLanguageUsed & "\" & strAlreadyScannedSound
            '.Ctlcontrols.play()
        End With

    End Sub

    Sub PlayAreYouSureSound()

        With frmMain.AxWindowsMediaPlayer1
            .URL = WAV_Path & "\" & strLanguageUsed & "\" & strAreYouSureSound
            '.Ctlcontrols.play()
        End With

    End Sub

    Sub PlayIAmPrintingSound()

        With frmMain.AxWindowsMediaPlayer1
            .URL = WAV_Path & "\" & strLanguageUsed & "\" & strNowPrintingSound
            '.Ctlcontrols.play()
        End With

    End Sub

    Sub PlaySomethingsWrongSound()

        With frmMain.AxWindowsMediaPlayer1
            .URL = WAV_Path & "\" & strLanguageUsed & "\" & strErrorSound
            '.Ctlcontrols.play()
        End With

    End Sub

    Public Function DecodeBarcode(ByVal ScanData As String, ByRef BCDate As String,
                              ByRef BCSerial As String, ByRef BCPartNum As String) As String

        Dim strResult As String = ""

        If ScanData.Length < 10 Then
            Return strResult
            Exit Function
        End If

        BCDate = Trim(Mid(ScanData, Parts(intCurrentProfile).BCDateStart, Parts(intCurrentProfile).BCDateLength))
        BCSerial = Trim(Mid(ScanData, Parts(intCurrentProfile).BCSerialStart, Parts(intCurrentProfile).BCSerialLength))
        If Parts(intCurrentProfile).BCPartNumStart = 0 Then
            'part number not in barcode
            BCPartNum = "NotScanned"
        Else
            BCPartNum = Trim(Mid(ScanData, Parts(intCurrentProfile).BCPartNumStart, Parts(intCurrentProfile).BCPartNumLength))
        End If

        Select Case Trim(Parts(intCurrentProfile).BCStyle)
            Case "PCustomerNumberSemi1TSerialSemiDCDatecode"
                If Mid(BCPartNum, 1, 1) = 0 Then BCPartNum = Mid(BCPartNum, 2, BCPartNum.Length)
                '  strResult = Trim(dbLocalSQL.GetBOMNumberFromCustomerPartNumber(BCPartNum))
                strResult = strCurrentBOM
            Case "CTSPartNumberDashedDateCodeSerial", "CTSPartNumberDashedDateCodeSpaceSerial"
                'get BOM from local Database drawing association table
                strResult = Trim(dbLocalSQL.GetBOMNumberFromCTSPartNumber(BCPartNum))
            Case "DateCodeTesterSerialNumber"
                strResult = strCurrentBOM 'from Selection???
                BCPartNum = RemoveDashes(BCPartNum)
            Case "DateCodeTesterSerialNumberDashed"
                strResult = strCurrentBOM 'from Selection???
                ' BCPartNum = RemoveDashes(BCPartNum)
            Case "PNCustomerNumberSemiSNSerialSemiDCDatecode"
                If Mid(BCPartNum, 1, 1) = 0 Then BCPartNum = Mid(BCPartNum, 2, BCPartNum.Length)
                strResult = Trim(dbLocalSQL.GetBOMNumberFromCustomerPartNumber(BCPartNum))
            Case Else
                strResult = ""
        End Select

        Return strResult

    End Function

    Function PrintLabel(ByRef local As Boolean, ByRef strCartonNumber As String, ByRef intQty As Integer, ByRef strBOM As String, ByVal strDateCode As String) As Boolean
        Dim blnTaskComplete As Boolean = False
        Dim dt As DataTable
        Dim blnApplicationDevelopmentTesting As Boolean = ConfigurationManager.AppSettings("ApplicationDevelopmentTesting")
        If blnApplicationDevelopmentTesting = False Then
            Call PlayIAmPrintingSound()
            System.Windows.Forms.Application.DoEvents()
            dt = New DataTable

            Try
                dt = dbLocalSQL.GetLabelDataByPartNumber(Parts(intCurrentProfile).CTSSite, strBOM, strCurrentCustomerNumber)
                If (dt.Rows.Count > 0) Then
                    LabelData.Qty = intQty 'intCartonCount
                    'LabelData.BOM = strBOM
                    LabelData.CartonSerialNumber = strCartonNumber
                    LabelData.DateCode = strDateCode
                    LabelData.CustPartNum = dt.Rows(0).Item("Cust_Part").ToString
                    LabelData.CustPartNum2 = dt.Rows(0).Item("Cust_Part2").ToString
                    LabelData.Supplier_ID = dt.Rows(0).Item("Supplier_ID").ToString
                    LabelData.LabelDesc1 = dt.Rows(0).Item("Label_Desc_1").ToString
                    LabelData.LabelDesc2 = dt.Rows(0).Item("Label_Desc_2").ToString
                    LabelData.LabelDesc3 = dt.Rows(0).Item("Label_Desc_3").ToString
                    LabelData.ECN = dt.Rows(0).Item("ECN").ToString
                    LabelData.BoxLabelFormat = dt.Rows(0).Item("Box_Lbl_Fmt").ToString
                    LabelData.LabelCarton = dt.Rows(0).Item("LBL_CTN").ToString
                    LabelData.DockLocation = dt.Rows(0).Item("Dock_Location").ToString
                    LabelData.DropZone = dt.Rows(0).Item("Drop_Zone").ToString
                    LabelData.Cust_No = dt.Rows(0).Item("Cust_No").ToString

                    Select Case LabelData.BoxLabelFormat
                        Case "CUBX47I3"  'Cummins
                            Call PrepareCUBX47I3Label()

                        Case "CPBX47I3"  'Cat??
                            Call PrepareCPBX47I3Label()

                        Case "GNBX47I3"  '706, 660
                            Call PrepareGNBX47I3Label()

                        Case "HABX47I3"  '706, 660
                            Call PrepareHABX47I3Label()

                        Case "NCBX14I3"  '706, 660
                            Call PrepareNCBX14I3Label()

                        Case "TYBX47I3"  '706, 660
                            Call PrepareTYBX47I3Label()
                    End Select

                    Call PrintLabelWriteLabel()
                    blnTaskComplete = True
                Else
                    DisplayUserMessage(UserMessageType.ApplicationError, dictPhrases("ErrorPrintingLabel"))
                End If

            Catch ex As Exception
                DisplayUserMessage(UserMessageType.ApplicationError, ex.Message)
            End Try
        Else
            DisplayUserMessage(UserMessageType.Information, "Simulated Label Printing. Carton Number: " + strCartonNumber)
            blnTaskComplete = True
        End If
        Return blnTaskComplete
    End Function

    Public Sub KillTime(ByVal Time As Double)
        gblnKillTimeDone = False
        KillTimer.Interval = Time
        KillTimer.Start()
        Do
            System.Windows.Forms.Application.DoEvents()
        Loop Until gblnKillTimeDone
        KillTimer.Stop()
    End Sub

    Public Sub KillTimerCallBack(ByVal source As Object, ByVal e As Timers.ElapsedEventArgs)
        gblnKillTimeDone = True
    End Sub

    Sub PrepareCPBX47I3Label()
        Dim q34 As String
        strLabelInfo = ""

        q34 = Chr(34) & "," & Chr(34) ' Double quote - comma - double quote

        strLabelInfo = Chr(34) & "BOM: " & RTrim(BOM) & q34
        strLabelInfo = strLabelInfo & RTrim(strDateCode) & q34
        strLabelInfo = strLabelInfo & RTrim(Mid(LabelData.CustPartNum, 1, 8)) & q34
        strLabelInfo = strLabelInfo & "P" & RTrim(Mid(LabelData.CustPartNum, 1, 8)) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.Qty) & q34
        strLabelInfo = strLabelInfo & "Q" & RTrim(LabelData.Qty) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.Supplier_ID) & q34
        strLabelInfo = strLabelInfo & "V" & RTrim(LabelData.Supplier_ID) & q34
        strLabelInfo = strLabelInfo & q34 & q34 'PO and code PO# not available
        strLabelInfo = strLabelInfo & RTrim(LabelData.CartonSerialNumber) & q34
        strLabelInfo = strLabelInfo & "3S" & RTrim(LabelData.CartonSerialNumber) & q34


        '"[)>061L16-51p0337-8842Q101T130994LUS2PV082864729KPO1234563S155976"
        '-------------
        'PDF417 code |
        '|-------------------------------------------------------------------------|
        strLabelInfo = strLabelInfo & "[)>061L16-51"
        strLabelInfo = strLabelInfo & "P" & RTrim(LabelData.CustPartNum)
        strLabelInfo = strLabelInfo & "Q" & RTrim(LabelData.Qty)
        strLabelInfo = strLabelInfo & "1T" & RTrim(strDateCode)
        strLabelInfo = strLabelInfo & "4LUS2P" 'country of origin/ec#
        strLabelInfo = strLabelInfo & "V" ' & RTrim(LabelData.Supplier_ID)
        strLabelInfo = strLabelInfo & "K" ' no PO# given
        strLabelInfo = strLabelInfo & "3S" & RTrim(LabelData.CartonSerialNumber) & Chr(34)
        ' strLabelInfo = strLabelInfo & "3S" & RTrim(LabelData.CartonSerialNumber) & Chr(34)
        '|-------------------------------------------------------------------------|

        ' strLabelInfo = strLabelInfo & RTrim(Mid(LabelData.CustPartNum, 1, 8)) & q34
    End Sub

    Sub PrepareCUBX47I3Label()
        Dim q34 As String
        strLabelInfo = ""

        q34 = Chr(34) & "," & Chr(34) 'Double quote - comma - double quote

        strLabelInfo = Chr(34) & RTrim(LabelData.CustPartNum) & q34
        strLabelInfo = strLabelInfo & "P" & RTrim(LabelData.CustPartNum) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.Qty) & q34
        strLabelInfo = strLabelInfo & "Q" & RTrim(LabelData.Qty) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.Supplier_ID) & q34
        strLabelInfo = strLabelInfo & "V" & RTrim(LabelData.Supplier_ID) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.CartonSerialNumber) & q34
        strLabelInfo = strLabelInfo & "S" & RTrim(LabelData.CartonSerialNumber) & q34
        strLabelInfo = strLabelInfo & q34 & q34 '2nd part number not needed for Cummins
        strLabelInfo = strLabelInfo & RTrim(LabelData.LabelDesc1) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.LabelDesc2) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.LabelDesc3) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.DateCode) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.BOM) & Chr(34)

    End Sub

    Sub PrepareGNBX47I3Label() 'generic

        Dim q34 As String
        strLabelInfo = ""

        q34 = Chr(34) & "," & Chr(34) 'Double quote - comma - double quote

        strLabelInfo = Chr(34) & RTrim(LabelData.CustPartNum) & q34
        strLabelInfo = strLabelInfo & "P" & RTrim(LabelData.CustPartNum) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.Qty) & q34
        strLabelInfo = strLabelInfo & "Q" & RTrim(LabelData.Qty) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.Supplier_ID) & q34
        strLabelInfo = strLabelInfo & "V" & RTrim(LabelData.Supplier_ID) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.CartonSerialNumber) & q34
        strLabelInfo = strLabelInfo & "S" & RTrim(LabelData.CartonSerialNumber) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.CustPartNum2) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.CustPartNum2) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.LabelDesc1) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.LabelDesc2) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.LabelDesc3) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.DateCode) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.BOM) & Chr(34)
    End Sub

    Sub PrepareHABX47I3Label() 'generic
        'Dim q34 As String
        strLabelInfo = ""

        ' q34 = Chr(34) & "," & Chr(34) 'Double quote - comma - double quote

        strLabelInfo = Chr(34) & Mid(LabelData.CustPartNum, 1, 5) & "-" & Mid(LabelData.CustPartNum, 6, 3) _
                      & "-" & Mid(LabelData.CustPartNum, 10, 4) & "-" & Mid(LabelData.CustPartNum, 14, 2) & Chr(34)


        strLabelInfo = strLabelInfo & "," & Chr(34) & "P" & RTrim(LabelData.CustPartNum) & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & "C" & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & RTrim(LabelData.LabelDesc1) & " " & RTrim(LabelData.LabelDesc2) & " " & RTrim(LabelData.LabelDesc3) & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & RTrim(LabelData.BOM) & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & RTrim(LabelData.DateCode) & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & "S" & RTrim(LabelData.CartonSerialNumber) & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & Mid(LabelData.ECN, 1, 2) & "-" & Mid(LabelData.ECN, 3, 2) & "-" & Mid(LabelData.ECN, 5, 2) & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & "2P" & LabelData.ECN & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & RTrim(LabelData.Qty) & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & "Q" & RTrim(LabelData.Qty) & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & RTrim(LabelData.Supplier_ID) & "-" & "00" & LabelData.CartonSerialNumber & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & "1S" & RTrim(LabelData.Supplier_ID) & "00" & LabelData.CartonSerialNumber & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & "Made In " & LabelData.DockLocation & Chr(34)


        'strLabelInfo = strLabelInfo & "V" & RTrim(LabelData.Supplier_ID) & q34
        'strLabelInfo = strLabelInfo & RTrim(LabelData.CartonSerialNumber) & q34

        'strLabelInfo = strLabelInfo & RTrim(LabelData.CustPartNum2) & q34
        'strLabelInfo = strLabelInfo & RTrim(LabelData.CustPartNum2) & q34

        'strLabelInfo = strLabelInfo & RTrim(LabelData.DateCode) & q34

    End Sub

    'This build is different than MX12M5790 because the label format won't work for the 551
    'TODO: Find out what is the correct format accross product lines.
    Sub PrepareNCBX14I3Label() 'generic
        'Dim q34 As String
        strLabelInfo = ""

        ' q34 = Chr(34) & "," & Chr(34) 'Double quote - comma - double quote

        strLabelInfo = Chr(34) & Mid(LabelData.CustPartNum, 1, 5) & "-" & Mid(LabelData.CustPartNum, 6, 3) _
                      & "-" & Mid(LabelData.CustPartNum, 10, 4) & "-" & Mid(LabelData.CustPartNum, 14, 2) & Chr(34)


        strLabelInfo = strLabelInfo & "," & Chr(34) & "P" & RTrim(LabelData.CustPartNum) & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & "C" & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & RTrim(LabelData.LabelDesc1) & " " & RTrim(LabelData.LabelDesc2) & " " & RTrim(LabelData.LabelDesc3) & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & RTrim(LabelData.BOM) & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & RTrim(LabelData.DateCode) & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & "S" & RTrim(LabelData.CartonSerialNumber) & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & Mid(LabelData.ECN, 1, 2) & "-" & Mid(LabelData.ECN, 3, 2) & "-" & Mid(LabelData.ECN, 5, 2) & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & "2P" & LabelData.ECN & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & RTrim(LabelData.Qty) & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & "Q" & RTrim(LabelData.Qty) & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & RTrim(LabelData.Supplier_ID) & "-" & "00" & LabelData.CartonSerialNumber & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & "1S" & RTrim(LabelData.Supplier_ID) & "00" & LabelData.CartonSerialNumber & Chr(34)
        strLabelInfo = strLabelInfo & "," & Chr(34) & "Made In " & LabelData.DockLocation & Chr(34)


        'strLabelInfo = strLabelInfo & "V" & RTrim(LabelData.Supplier_ID) & q34
        'strLabelInfo = strLabelInfo & RTrim(LabelData.CartonSerialNumber) & q34

        'strLabelInfo = strLabelInfo & RTrim(LabelData.CustPartNum2) & q34
        'strLabelInfo = strLabelInfo & RTrim(LabelData.CustPartNum2) & q34

        'strLabelInfo = strLabelInfo & RTrim(LabelData.DateCode) & q34
    End Sub

    Sub PrepareTYBX47I3Label() 'generic

        Dim q34 As String
        strLabelInfo = ""

        q34 = Chr(34) & "," & Chr(34) 'Double quote - comma - double quote

        strLabelInfo = Chr(34) & RTrim(LabelData.CustPartNum) & q34
        strLabelInfo = strLabelInfo & "P" & RTrim(LabelData.CustPartNum) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.Qty) & q34
        strLabelInfo = strLabelInfo & "Q" & RTrim(LabelData.Qty) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.Supplier_ID) & q34
        strLabelInfo = strLabelInfo & "V" & RTrim(LabelData.Supplier_ID) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.CartonSerialNumber) & q34
        strLabelInfo = strLabelInfo & "S" & RTrim(LabelData.CartonSerialNumber) & q34
        ' strLabelInfo = strLabelInfo & RTrim(LabelData.CustPartNum2) & q34
        ' strLabelInfo = strLabelInfo & RTrim(LabelData.CustPartNum2) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.LabelDesc1) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.LabelDesc2) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.LabelDesc3) & q34
        strLabelInfo = strLabelInfo & RTrim(strDateCode) & q34
        strLabelInfo = strLabelInfo & RTrim(LabelData.BOM) & Chr(34)
    End Sub

    Sub PrintLabelWriteLabel()
        Dim LabelRIGHT As Object
        Dim LabelPrintQty As Integer
        Dim strPath As String
        Dim intLoop As Integer

        'Dim getTime As System.Diagnostics.Stopwatch = New Stopwatch()
        'getTime.Start()

        LabelPrintQty = Val(LabelData.LabelCarton)

        'Write to data file
        strPath = "c:\ee's\Packout Labels\" & LabelData.BoxLabelFormat & ".DAT"
        FileOpen(1, strPath, OpenMode.Output)

        For intLoop = 1 To LabelPrintQty
            PrintLine(1, strLabelInfo)
        Next

        FileClose(1)

        If (LabelRIGHT Is Nothing) Then
            LabelRIGHT = CreateObject("Lrw3.Control")
            strPath = "c:\ee's\Packout Labels\" & LabelData.BoxLabelFormat & ".LBZ"
            LabelRIGHT.SetTemplate(strPath)
        End If
        'getTime.Stop()
        'System.Windows.Forms.MessageBox.Show("Time cost IN LABELRIGHT: " & getTime.ElapsedMilliseconds / 1000)

        'getTime.Start()

        'Send Print Job
        '  tmp = LRData & xBoxLabelFormat & ".LBZ"

        With LabelRIGHT

            '.SetTemplate(strPath)
            'DoEvents
            '.SetParams("/NL")
            .PrintJob()
            'DoEvents

        End With
        'getTime.Stop()
        'System.Windows.Forms.MessageBox.Show("Time to finish printing: " & getTime.ElapsedMilliseconds / 1000)

    End Sub

    'Added strCartonNumber parameter to the function  23 MAY 2016  AMC
    Function CompleteBox(ByVal strCartonNumber As String,
                        ByRef blnCompletedBox As Boolean,
                        ByRef blnSendToAS400 As Boolean,
                        ByRef BOM As String,
                        ByRef intQTY As Integer,
                        ByRef strDateCode As String
                        ) As Boolean
        Try
            'Dim strSerial As String  'Made this a parameter of the function  23 MAY 2016  AMC
            Dim blnResult As Boolean = False
            Dim intLoop As Integer
            Dim dtScannedParts As New DataTable
            Dim strBoxFormat As String
            Dim blnTaskComplete As Boolean = False

            If String.IsNullOrEmpty(strCartonNumber) Or strCartonNumber = "0" Then
                'Get Carton Serial Number
                strCartonNumber = dbLocalSQL.GetTopSerialNumber 'new box serial #
            End If
            'Get Carton Label format 
            strBoxFormat = dbLocalSQL.LookupBoxFormat(BOM)

            'Get datatable of carton contents
            blnResult = dbLocalSQL.GetScannedPartsForBOM(BOM, dtScannedParts)

            If blnResult Then
                For intLoop = 0 To dtScannedParts.Rows.Count - 1
                    dtScannedParts.Rows(intLoop).Item("CTNSERNUM") = Trim(strCartonNumber)
                    dtScannedParts.Rows(intLoop).Item("CUSTNUM") = Trim(strCurrentCustomerNumber)
                Next

                'Now Test for Linked Server Connection to remote DB Server
                'blnSqlServerRunning = dbSQLServer.TestLinkedServerConnection(strDBLOCALSQL, ConfigurationManager.AppSettings("SQLServer"))
                'If blnSqlServerRunning Then
                '	If blnCompletedBox Then
                '		blnResult = dbSQLServer.InsertCompletedCarton(Parts(intCurrentProfile).CTSSite, strCartonNumber, BOM, strCurrentCustomerNumber, strCurrentPartNumber, intQTY, strDateCode,
                '																   Now, strBoxFormat, dtScannedParts)
                '	Else
                '		blnResult = dbSQLServer.InsertPartialCarton(strCartonNumber, BOM, strCurrentCustomerNumber, strCurrentPartNumber, intQTY, strDateCode,
                '														 Now, strBoxFormat, dtScannedParts)
                '	End If

                '	If blnResult Then
                '		'Delete records from local table SCNDPART associated with inserted carton
                '		dbLocalSQL.DeleteScannedParts()
                '	End If
                'Else 'Save data locally.  Server Job will pull up when connection restored
                '	If blnCompletedBox Then
                '		blnResult = dbLocalSQL.SQlServerInsertCompletedCarton(Parts(intCurrentProfile).CTSSite, strCartonNumber, BOM, strCurrentCustomerNumber, strCurrentPartNumber, intQTY, strDateCode,
                '																   Now, strBoxFormat, blnSendToAS400, dtScannedParts)
                '	Else
                '		blnResult = dbLocalSQL.SqlServerInsertPartialCarton(strCartonNumber, BOM, strCurrentPartNumber, intQTY, strDateCode,
                '													Now, strBoxFormat, strCurrentCustomerNumber, dtScannedParts)
                '	End If
                'End If


                ' GG 2017/10/13 we switch to the code block above to solve the network connection issue

                If blnCompletedBox Then
                    blnResult = dbLocalSQL.SQlServerInsertCompletedCarton(Parts(intCurrentProfile).CTSSite,
                                                                            strCartonNumber,
                                                                            BOM,
                                                                            strCurrentCustomerNumber,
                                                                            strCurrentPartNumber,
                                                                            intQTY,
                                                                            strDateCode,
                                                                            Now,
                                                                            strBoxFormat,
                                                                            blnSendToAS400,
                                                                            dtScannedParts
                                                                            )
                Else
                    blnResult = dbLocalSQL.SqlServerInsertPartialCarton(strCartonNumber,
                                                                        BOM,
                                                                        strCurrentCustomerNumber,
                                                                        strCurrentPartNumber,
                                                                        intQTY,
                                                                        strDateCode,
                                                                        Now,
                                                                        strBoxFormat,
                                                                        dtScannedParts
                                                                        )
                End If

                frmPack.SendToAs400 = True

                If (ConfigurationManager.AppSettings("RepackStation") = "True") Then
                    frmMessageBox.Text = dictPhrases("PrintLabel")
                    frmMessageBox.Message = dictPhrases("PrintLabel")
                    frmMessageBox.Style = "YesNo"
                    frmMessageBox.ShowDialog()

                    If frmMessageBox.FormResult = "Yes" Then
                        blnResult = PrintLabel(True, strCartonNumber, intQTY, BOM, strDateCode)
                    End If
                Else

                    blnResult = PrintLabel(True, strCartonNumber, intQTY, BOM, strDateCode)
                End If

                If Not blnResult Then
                    DisplayUserMessage(UserMessageType.Information, dictPhrases("ErrorPrintingLabel"))
                End If
            End If

            Return blnResult

        Catch ex As Exception
            Throw New Exception(ex.Message)

        End Try
    End Function

    Function RemoveDashes(ByRef strInput As String) As String
        Dim strResult As String = ""
        Dim intLoop As Integer = 0

        strResult = ""
        For intLoop = 1 To strInput.Length
            If Mid(strInput, intLoop, 1) = "-" Then
                'skip char
            Else
                strResult = strResult & Mid(strInput, intLoop, 1)
            End If

        Next


        Return strResult
    End Function

    Function UpdateLabelBank() As Boolean
        Dim strData As String
        Dim intBoxNumbersNeeded As Integer
        Dim blnResult As Boolean = False
        'find out how many are left in the local database
        strData = dbLocalSQL.GetNumberOfLabelsInBank500()
        intBoxNumbersNeeded = 400 - Val(strData)
        If intBoxNumbersNeeded > 0 Then
            'insert into bank500
            'Now Test for Linked Server Connection to Server
            blnSqlServerRunning = dbSQLServer.TestLinkedServerConnection(strDBLOCALSQL, ConfigurationManager.AppSettings("SQLServer"))
            If blnSqlServerRunning Then
                blnResult = dbLocalSQL.AddBoxesToBANK500()
            End If
        Else
            blnResult = True
        End If

        Return blnResult
    End Function

    Sub LoadDontUseList()
        Dim intLoop As Integer
        Dim fs As FileStream
        Dim sr As StreamReader

        Dim strHeader As String = ""

        fs = New FileStream("c:\ee's\Suspect Serials.csv.txt", FileMode.Open)

        sr = New StreamReader(fs)

        ' strHeader = sr.ReadLine()
        intLoop = 1

        Do While sr.Peek() >= 0
            ReDim Preserve strDontUseArray(intLoop)
            strDontUseArray(intLoop) = sr.ReadLine()
            intLoop = intLoop + 1
        Loop

        sr.Close()
        fs.Close()
    End Sub

    'Function CheckSerialBank() As Integer
    '    Dim intCount As Integer
    '    intCount = dbLocalSQL.GetNumberOfLabelsInBank500()
    '    Return intCount

    'End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' 

    Sub LoadLanguages(intLanguageSelected As Integer)
        Dim dtLanguagePhrases As DataTable = dbLocalSQL.GetLanguagePhrases(intLanguageSelected)
        Dim dr As DataRow
        dictPhrases.Clear()
        For Each dr In dtLanguagePhrases.Rows
            dictPhrases.Add(dr("PhraseCode"), dr("LanguagePhrase"))
        Next
        dtLanguagePhrases.Dispose()
    End Sub

    Public Sub DisplayUserMessage(enumMessageValue As Integer, strMessage As String)
        Dim intMsgVal As Integer = enumMessageValue

        Select Case enumMessageValue
            Case UserMessageType.ApplicationError
                frmMessageBox.Text = dictPhrases("ApplicationError")
                PlaySomethingsWrongSound()

            Case UserMessageType.DatabaseConnectionError
                frmMessageBox.Text = dictPhrases("DatabaseError")
                PlaySomethingsWrongSound()

            Case UserMessageType.InputError
                frmMessageBox.Text = dictPhrases("InputError")
                PlaySomethingsWrongSound()

            Case Else  'UserMessageType.Information
                frmMessageBox.Text = dictPhrases("Information")

        End Select
        frmMessageBox.Style = "Ok"
        frmMessageBox.Message = strMessage
        frmMessageBox.ShowDialog()
    End Sub


    'Function removedashes(strToFIx As String) As String
    '    Dim intloop As Integer
    '    Dim strFixed As String

    '    For intloop = 1 To strToFIx.Length
    '        If Mid(strToFIx, intloop, 1) = "-" Then

    '        Else
    '            strFixed = strFixed & Mid(strToFIx, intloop, 1)
    '        End If
    '    Next
    'End Function

    'Replaced function 20APR2016 AMC
    'Function CompleteBox(ByRef blnCompletedBox As Boolean, ByRef BOM As String, ByRef intQTY As Integer) As Boolean

    '    Dim strSerial As String
    '    Dim blnResult As Boolean
    '    Dim intLoop As Integer
    '    Dim dtScannedParts As New DataTable
    '    Dim rs As ADODB.Recordset
    '    Dim strResult As String
    '    Dim strBoxFormat As String

    '    strSerial = dbLocalSQL.GetTopSerialNumber 'new box serial #
    '    blnResult = dbLocalSQL.ClearBoxSerialFromQueue(strSerial)
    '    blnResult = False

    '    strBoxFormat = dbLocalSQL.LookupBoxFormat(BOM)
    '    If blnCompletedBox Then
    '        If blnSqlServerRunning Then
    '            blnResult = dbSQLServer.InsertCompletedCarton(strSerial, BOM, strCurrentCustomerNumber, strCurrentPartNumber, intQTY, strDateCode,
    '                                                           Now, strBoxFormat)
    '        Else
    '            blnResult = dbLocalSQL.SQlServerInsertCompletedCarton(strSerial, BOM, strCurrentCustomerNumber, strCurrentPartNumber, intQTY, strDateCode,
    '                                                                   Now, strBoxFormat)
    '        End If

    '        'Send to AS400
    '        If blnAS400Running Then
    '            strResult = dbAS400.SendCompletedCartonToAS400(Parts(intCurrentProfile).CTSSite, strSerial, BOM, strCurrentPartNumber, intQTY, strDateCode,
    '                                                                   Now, strBoxFormat, strCurrentCustomerNumber)
    '            If strResult = "Good" Then
    '                'verify in database
    '                rs = dbAS400.GetCompletedCartonFromAS400(Parts(intCurrentProfile).CTSSite, strSerial, BOM, strCurrentPartNumber, intQTY, strDateCode,
    '                                                                     Now, strBoxFormat, strCurrentCustomerNumber)
    '                If rs.BOF = True And rs.EOF = True Then
    '                    blnAS400Running = False
    '                Else
    '                    blnAS400Running = True
    '                End If
    '            End If
    '        End If

    '        If Not blnAS400Running Then
    '            strResult = dbLocalSQL.AS400SendCompletedCartonToAS400(Parts(intCurrentProfile).CTSSite, strSerial, BOM, strCurrentPartNumber, intQTY, strDateCode,
    '                                                                Now, strBoxFormat, strCurrentCustomerNumber)
    '        End If

    '        If blnSqlServerRunning Then
    '            blnResult = dbSQLServer.MarkFullCartonAsOnAS400(strSerial)
    '        Else
    '            blnResult = dbLocalSQL.SqlServerMarkFullCartonAsOnAS400(strSerial)
    '        End If

    '    Else
    '        If blnSqlServerRunning Then
    '            blnResult = dbSQLServer.InsertPartialCarton(strSerial, BOM, strCurrentPartNumber, intQTY, strDateCode,
    '                                                                Now, strBoxFormat, strCurrentCustomerNumber)
    '        Else
    '            blnResult = dbLocalSQL.SqlServerInsertPartialCarton(strSerial, BOM, LabelData.CustPartNum, intQTY, strDateCode,
    '                                                             Now, strBoxFormat, strCurrentCustomerNumber)
    '        End If
    '    End If

    '    If blnResult Then blnResult = dbLocalSQL.GetScannedPartsForBOM(BOM, dtScannedParts)

    '    For intLoop = 0 To dtScannedParts.Rows.Count - 1
    '        dtScannedParts.Rows(intLoop).Item("CTNSERNUM") = Trim(strSerial)
    '        dtScannedParts.Rows(intLoop).Item("CUSTNUM") = Trim(strCurrentCustomerNumber)
    '    Next

    '    If blnSqlServerRunning Then
    '        If blnResult Then blnResult = dbSQLServer.MoveScannedPartsInCarton(dtScannedParts)
    '    Else
    '        If blnResult Then blnResult = dbLocalSQL.SqlServerMoveScannedPartsInCarton(dtScannedParts)
    '    End If
    '    If blnResult Then blnResult = dbLocalSQL.ClearScannedPartsForBOMAndCustomerPartNum(BOM, strCurrentCustomerNumber)


    '    blnResult = PrintLabel(True, strSerial, intQTY, BOM, strDateCode)

    '    Return blnResult

    'End Function
End Module
