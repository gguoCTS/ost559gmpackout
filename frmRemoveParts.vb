﻿Imports System.Configuration
'To simplify, all Container modifications are performed on the server as they are not in normal work flow.  24 JUN 2016 AMC

Public Class frmRemoveParts
    Dim tempColor As Color
    Dim strData As String
    Dim DT As DataTable
    Public strCurrentContainerNumber As String

    Private Sub frmRemoveParts_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            tempColor = BackColor
            Call UpdateFormLanguage()
            txtScanData.Enabled = False
            DT = New DataTable()
            Dim dc1 As DataColumn = New DataColumn("Serial Number")
            Dim dc2 As DataColumn = New DataColumn("BOM")
            Dim dc3 As DataColumn = New DataColumn("Carton Number")
            Dim PK(1) As DataColumn

            PK(0) = dc1
            DT.Columns.Add(dc1)
            DT.Columns.Add(dc2)
            DT.Columns.Add(dc3)
            DT.PrimaryKey = PK
            btnRemoveParts_PrintLabel.Enabled = False
            btnRemoveParts.Enabled = False
        Catch ex As Exception
            DisplayUserMessage(UserMessageType.ApplicationError, ex.Message)
            Me.BeginInvoke(New MethodInvoker(AddressOf Me.Close))
        End Try

    End Sub

    Sub UpdateFormLanguage()
        Me.Text = dictPhrases("FrmRemoveParts")
        lblScanOrTypeTheBoxNumber.Text = dictPhrases("lblScanOrTypeTheBoxNumber")
        lblScanPart.Text = dictPhrases("lblScanPart")
        lblBOM.Text = dictPhrases("BOM")
        lblContainerCountLabel.Text = dictPhrases("lblContainerCountLabel")
        lblCustomerLabel.Text = dictPhrases("lblCustomerLabel")
        lblUnpackBox.Text = dictPhrases("lblUnpackBox")
        btnRemoveParts_PrintLabel.Text = dictPhrases("btnRemoveParts_PrintLabel")
        btnRemoveParts.Text = dictPhrases("btnRemoveParts")
    End Sub

    Private Sub ScanData_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtScanData.KeyUp
        Dim blnSqlServerRunning As Boolean = False
        Dim BCDate As String = String.Empty
        Dim BCSerial As String = String.Empty
        Dim BCPartNum As String = String.Empty
        Dim BOM As String = String.Empty
        Dim dtLookup As DataTable
        Dim strData As String = String.Empty

        If e.KeyCode = Keys.Enter And txtScanData.TextLength > 0 Then
            'Decode the Scanned 2D Barcode to find the serial Number
            'Peform Lookup on the server to find Information
            Try
                strData = Trim(txtScanData.Text)
                'User typed in serial number; did not scan barcode
                If Parts(intCurrentProfile).BCSerialLength = strData.Length Then 'User typed in serial number; did not scan barcode
                    BCSerial = strData
                    BOM = lblBOMSelected.Text
                Else
                    'User scanned 2d barcode
                    BOM = DecodeBarcode(strData, BCDate, BCSerial, BCPartNum)
                End If

                If BOM = "" Then
                    DisplayUserMessage(UserMessageType.InputError, dictPhrases("BOM_Incorrect"))
                    txtScanData.Clear()
                    txtScanData.Focus()
                Else
                    If BOM = strCurrentBOM Then
                        If (DT.Rows.Contains(BCSerial)) Then
                            DisplayUserMessage(UserMessageType.InputError, dictPhrases("PartAlreadyAddedToList"))
                        Else
                            blnSqlServerRunning = dbSQLServer.TestLinkedServerConnection(strDBLOCALSQL, ConfigurationManager.AppSettings("SQLServer"))
                            If blnSqlServerRunning Then
                                dtLookup = dbSQLServer.LookupScannedPartByCartonAndSerialNumber(strCurrentContainerNumber, BCSerial)
                                If dtLookup IsNot Nothing AndAlso dtLookup.Rows.Count > 0 Then
                                    DT.Merge(dtLookup)
                                    dgScannedParts.DataSource = DT
                                    dgScannedParts.Columns(0).Width = 145
                                    dgScannedParts.Columns(1).Width = 102
                                    dgScannedParts.Columns(2).Width = 145
                                Else
                                    DisplayUserMessage(UserMessageType.InputError, dictPhrases("PartNotFoundInContainer"))
                                End If
                            Else
                                DisplayUserMessage(UserMessageType.DatabaseConnectionError, dictPhrases("ContainerNotFound") & vbCrLf & dictPhrases("SQLServerOffline"))
                            End If
                        End If
                    Else
                        DisplayUserMessage(UserMessageType.InputError, dictPhrases("BOM_Incorrect"))
                    End If
                End If

                txtScanData.Clear()
                txtScanData.Focus()

                If DT.Rows.Count > 0 Then
                    btnRemoveParts.Enabled = True
                    btnRemoveParts_PrintLabel.Enabled = True
                Else
                    btnRemoveParts.Enabled = False
                    btnRemoveParts_PrintLabel.Enabled = False
                End If

            Catch ex As Exception
                DisplayUserMessage(UserMessageType.ApplicationError, ex.Message)
            End Try

        End If
    End Sub

    Private Sub FindCartonOnServer(ByVal strScannedContainer As String)
        Dim blnSqlServerRunning As Boolean
        Dim dt As DataTable
        Dim intStartPosition As Integer = strScannedContainer.ToUpper.IndexOf("S")
        'Reference to menu item for selecting BOM
        Dim menuItem As System.Windows.Forms.ToolStripMenuItem
        menuItem = Me.MdiParent.MainMenuStrip.Items(1)
        strCurrentContainerNumber = Nothing

        If intStartPosition = -1 Then
            strCurrentContainerNumber = strScannedContainer
        Else
            strCurrentContainerNumber = strScannedContainer.Substring(intStartPosition + 1)
        End If

        blnSqlServerRunning = dbSQLServer.TestLinkedServerConnection(strDBLOCALSQL, ConfigurationManager.AppSettings("SQLServer"))
        If blnSqlServerRunning Then
            dt = dbSQLServer.FindCartonByNumber(strCurrentContainerNumber)
            If dt Is Nothing Or dt.Rows.Count = 0 Then
                'pnlBG.BackColor = Color.Red
                'Panel1.BackColor = tempColor
                DisplayUserMessage(UserMessageType.InputError, dictPhrases("ContainerNotFound"))
                txtScanData.Enabled = False
            ElseIf (strCurrentCustomerNumber <> dt.Rows(0)(0)) And (strCurrentBOM <> dt.Rows(0)(1)) Then
                lblCustomer.Text = dt.Rows(0)(0)
                lblBOMSelected.Text = dt.Rows(0)(1)
                lblContainerCount.Text = dt.Rows(0)(2)
                DisplayUserMessage(UserMessageType.InputError, dictPhrases("BOMAndCustomerNumberIncorrect"))
                Me.Close()
                menuItem.DropDownItems(0).PerformClick()
            Else
                lblCustomer.Text = dt.Rows(0)(0)
                lblBOMSelected.Text = dt.Rows(0)(1)
                lblContainerCount.Text = dt.Rows(0)(2)
                txtScanData.Enabled = True
                txtScanData.Focus()
            End If
        Else
            DisplayUserMessage(UserMessageType.DatabaseConnectionError, dictPhrases("UnableToDeleteContainer") & vbCrLf & dictPhrases("SQLServerOffline"))
        End If

    End Sub

    Private Sub btnRemoveParts_PrintLabel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveParts_PrintLabel.Click
        If strCurrentContainerNumber <> String.Empty Then
            Try
                DeleteParts(True) 'Delete Parts and print partial label
                ResetFormControls()
                DisplayUserMessage(UserMessageType.Information, dictPhrases("SuccessfullyCompleted"))

            Catch ex As Exception
                DisplayUserMessage(UserMessageType.ApplicationError, ex.Message)
            End Try
        Else
            DisplayUserMessage(UserMessageType.InputError, dictPhrases("ContainerNotFound"))
        End If
    End Sub

    'Remove Parts without printing label
    Private Sub btnRemoveParts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveParts.Click
        If strCurrentContainerNumber <> String.Empty Then
            Try
                If DT.Rows.Count > 0 Then
                    DeleteRecords(DT, True)
                    ResetFormControls()
                    DisplayUserMessage(UserMessageType.Information, dictPhrases("SuccessfullyCompleted"))
                Else
                    DisplayUserMessage(UserMessageType.InputError, dictPhrases("PartListIsEmpty"))
                End If

            Catch ex As Exception
                DisplayUserMessage(UserMessageType.ApplicationError, ex.Message)
            End Try
        Else
            DisplayUserMessage(UserMessageType.InputError, dictPhrases("ContainerNotFound"))
        End If
    End Sub

    Private Sub txtCarton_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtCarton.KeyUp
        Dim blnSqlServerRunning As Boolean = False
        If e.KeyCode = Keys.Enter And txtCarton.TextLength > 0 Then
            'Decode the Scanned 2D Barcode to find the serial Number
            'Peform Lookup on the server to find Container Information
            Try
                blnSqlServerRunning = dbSQLServer.TestLinkedServerConnection(strDBLOCALSQL, ConfigurationManager.AppSettings("SQLServer"))
                If blnSqlServerRunning Then
                    FindCartonOnServer(txtCarton.Text)
                Else
                    DisplayUserMessage(UserMessageType.DatabaseConnectionError, dictPhrases("ContainerNotFound") & vbCrLf & dictPhrases("SQLServerOffline"))
                End If

            Catch ex As Exception
                DisplayUserMessage(UserMessageType.ApplicationError, ex.Message)
            End Try
        End If
    End Sub

    Private Sub DeleteRecords(ByVal DT As DataTable,
                              ByVal blnReplace As Boolean
                              )
        dbSQLServer.DeleteScannedPartsInCarton(DT,
                                               blnReplace
                                               )
    End Sub

    Private Sub ResetFormControls()
        lblContainerCount.Text = String.Empty
        lblBOMSelected.Text = String.Empty
        lblCustomer.Text = String.Empty
        DT.Clear()
        dgScannedParts.DataSource = DT
        txtScanData.Enabled = False
        btnRemoveParts.Enabled = False
        btnRemoveParts_PrintLabel.Enabled = False
        strCurrentContainerNumber = Nothing
        txtCarton.Clear()
        txtCarton.Focus()
    End Sub

    Private Sub DeleteParts(ByVal blnPrintLabel As Boolean)
        Dim blnSqlServerRunning As Boolean = False
        If DT.Rows.Count > 0 Then
            'Test for connection to server
            blnSqlServerRunning = dbSQLServer.TestLinkedServerConnection(strDBLOCALSQL, ConfigurationManager.AppSettings("SQLServer"))
            If blnSqlServerRunning Then
                DeleteRecords(DT, False)
                'Print Label    
                If (Val(lblContainerCount.Text) - DT.Rows.Count) > 0 Then
                    If blnPrintLabel Then
                        PrintLabel(True, strCurrentContainerNumber, Val(lblContainerCount.Text) - DT.Rows.Count, BOM, String.Empty)
                    End If
                Else
                    DisplayUserMessage(UserMessageType.Information, dictPhrases("EmptyContainerDeleted"))
                End If
            Else
                DisplayUserMessage(UserMessageType.DatabaseConnectionError, dictPhrases("UnableToDeleteContainer") & vbCrLf & dictPhrases("SQLServerOffline"))
            End If
        Else
            DisplayUserMessage(UserMessageType.InputError, dictPhrases("PartListIsEmpty"))
        End If
    End Sub

    Private Sub btnUnpackContainer_Click(sender As Object, e As EventArgs) Handles btnUnpackContainer.Click
        Dim blnSqlServerRunning As Boolean = False
        frmMessageBox.Message = dictPhrases("RemoveParts")
        frmMessageBox.Text = dictPhrases("lblUnpackBox")
        frmMessageBox.Style = "YesNo"
        frmMessageBox.ShowDialog()

        If frmMessageBox.FormResult = "Yes" Then
            Try
                blnSqlServerRunning = dbSQLServer.TestLinkedServerConnection(strDBLOCALSQL, ConfigurationManager.AppSettings("SQLServer"))
                If blnSqlServerRunning Then
                    dbSQLServer.DeleteScannedPartsInCarton(strCurrentContainerNumber, strCurrentBOM)
                    ResetFormControls()
                    DisplayUserMessage(UserMessageType.Information, dictPhrases("SuccessfullyCompleted"))
                Else
                    DisplayUserMessage(UserMessageType.DatabaseConnectionError, dictPhrases("UnableToDeleteContainer") & vbCrLf & dictPhrases("SQLServerOffline"))
                End If
            Catch ex As Exception
                DisplayUserMessage(UserMessageType.ApplicationError, ex.Message)
            End Try
        End If
    End Sub


    'Private Shared _ReplacementType As String

    'Public Shared Property ReplacementType As String
    '    Get
    '        Return _ReplacementType
    '    End Get
    '    Set(ByVal value As String)
    '        If (value.Trim <> "") Then
    '            _ReplacementType = value.Trim
    '        End If
    '    End Set
    'End Property
End Class

