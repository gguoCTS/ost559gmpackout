Imports System.IO
Imports System.Configuration

Public Class frmPackWithScale
    Dim BCDate As String
    Dim BCSerial As String
    Dim BCPartNum As String
    Dim blnPackQtyLoaded As Boolean = False
    Dim tempColor As Color
    Dim strCurrentCartonNumber As String = 0
    Dim blnSendToAS400 As Boolean = True
    Dim serialPortScale As clsScale


    Private blnResumeCheckingForFile As Boolean = False

    Private Sub frmPackWithScale_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            serialPortScale = New clsScale(ConfigurationManager.AppSettings("ScaleConfigFilePath").ToString())

            UpdateFormLanguage()
            dbLocalSQL.DeleteScannedParts() 'Cleanup Local machine of any unsaved Parts
            If gStrCurrentProfileName <> "" Then
                If strCurrentBOM <> "" Then
                    Me.cboStandardBoxQty.DataSource = dbLocalSQL.LookupNormalBoxQty(Parts(intCurrentProfile).CTSSite, strCurrentCustomerNumber, strCurrentBOM)
                    'TODO: Add to the selection dropdown -->strLanguageWordsUsed(intLanguageSelected).p035_AlternateQuantity 

                    CheckForPartials() 'Check for partials on server 

                    Select Case Parts(intCurrentProfile).PartEntryMethod
                        Case "DirectScan"
                            txtScanData.Enabled = True
                            tmrCheckForFile.Enabled = False
                        Case "Flatfile"
                            tmrCheckForFile.Enabled = True
                            txtScanData.Enabled = False
                    End Select

                    If gblTestModeEnabled Or strRepackStation = "True" Then
                        'Force to DirectScan
                        txtScanData.Enabled = True
                        tmrCheckForFile.Enabled = False
                    End If
                    tempColor = Panel1.BackColor
                Else
                    DisplayUserMessage(UserMessageType.InputError, dictPhrases("BOM_SelectionRequired"))
                    Me.BeginInvoke(New MethodInvoker(AddressOf Me.Close))
                End If
            Else
                DisplayUserMessage(UserMessageType.InputError, dictPhrases("BOM_SelectionRequired"))
                Me.BeginInvoke(New MethodInvoker(AddressOf Me.Close))
            End If

        Catch ex As Exception
            DisplayUserMessage(UserMessageType.ApplicationError, ex.Message)
            Me.BeginInvoke(New MethodInvoker(AddressOf Me.Close))
        End Try
    End Sub

    Sub UpdateFormLanguage()
        ' btnOK.Text = strLanguageWordsUsed(intLanguageSelected).p005_ok
        '  lblEnterPackoutData.Text = strLanguageWordsUsed(intLanguageSelected).p063_ScanPartToPlaceIntoBox
        Me.Text = dictPhrases("frmPack")
        lblScanPart.Text = dictPhrases("lblScanPart")
        lblSerialNumberScanned.Text = dictPhrases("lblSerialNumberScanned")
        lblDateCodeScanned.Text = dictPhrases("lblDateCodeScanned")
        lblBOMScanned.Text = dictPhrases("lblBOMScanned")
        lblQuantityPackedHdr.Text = dictPhrases("lblQuantityPackedHdr")
        lblStdCartonQuantity.Text = dictPhrases("lblStdCartonQuantity")
        lblPackingList.Text = dictPhrases("lblPackingList")
        btnSavePartial.Text = dictPhrases("btnSavePartial")
        btnShipPartial.Text = dictPhrases("btnShipPartial")
    End Sub

    Private Sub ScanData_Click(ByVal sender As Object, ByVal e As EventArgs) Handles txtScanData.Click
        Call ClearScanData()
    End Sub

    Private Sub ScanData_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtScanData.KeyUp
        If e.KeyCode = Keys.Enter And txtScanData.TextLength > 0 Then
            If Parts(intCurrentProfile).PartEntryMethod = "DirectScan" Or gblTestModeEnabled Or strRepackStation = "True" Then
                Call ProcessPart(txtScanData.Text)
                txtScanData.Text = ""
            Else
                txtScanData.Text = ""
            End If

        End If
    End Sub

    'p009999991;1TFE00000002;DC174152
    Sub ProcessPart(ByRef strData As String)
        Dim strDuplicate As String
        Dim strResult As String
        Dim dt As New DataTable
        Dim blnResult As Boolean

        blnResumeCheckingForFile = False
        pnlBG.BackColor = Color.Gray
        Panel1.BackColor = tempColor
        lblSerialNumberScannedValue.Text = ""
        lblDateCodeScannedValue.Text = ""
        lblBomScannedValue.Text = ""

        Try
            strData = Trim(txtScanData.Text)
            BOM = DecodeBarcode(strData, BCDate, BCSerial, BCPartNum)

            If BOM = "" Then
                DisplayUserMessage(UserMessageType.InputError, dictPhrases("BOM_Incorrect"))
                Exit Sub
            End If

            'Decoupled code that looks for rejected parts on local machine. 26 JUN 2016 AMC
            'TODO: Enable for Actuator Line PrePack Functionality
            'For intLoop = 1 To strDontUseArray.Length - 1
            '    If BCSerial = strDontUseArray(intLoop) Then
            '        Call PlaySomethingsWrongSound()
            '        frmMessageBox.Message = strLanguageWordsUsed(intLanguageSelected).p091_PartOnDontUseList
            '        frmMessageBox.Style = "Ok"
            '        frmMessageBox.Text = strLanguageWordsUsed(intLanguageSelected).p091_PartOnDontUseList
            '        frmMessageBox.ShowDialog()
            '        Exit Sub
            '    End If
            'Next

            gstrTempData = ""
            If BOM <> strCurrentBOM Or (BCPartNum <> strCurrentPartNumber And BCPartNum <> "NotScanned") Then
                Call PlayAreYouSureSound()
                DisplayUserMessage(UserMessageType.InputError, dictPhrases("BOM_Incorrect"))
                txtScanData.Text = ""
                Exit Sub
            End If

            strDuplicate = dbLocalSQL.LookupSerialNumberinScannedParts(BCSerial)
            If strDuplicate = "Found" Then
                pnlBG.BackColor = Color.Red
                Panel1.BackColor = tempColor
                'alreadyscanned
                DisplayUserMessage(UserMessageType.InputError, dictPhrases("AlreadyScanned"))
                Call PlayAlreadyScannedSound()
                txtScanData.Text = ""
                Exit Sub
            End If

            'Now Check the Server.  If Server offline continue without server lookup
            blnSqlServerRunning = dbSQLServer.TestLinkedServerConnection(strDBLOCALSQL, ConfigurationManager.AppSettings("SQLServer"))
            If blnSqlServerRunning Then
                strDuplicate = dbSQLServer.LookupSerialNumberinScannedParts(BCSerial, dt)
                If strDuplicate = "Found" Then
                    pnlBG.BackColor = Color.Red
                    Panel1.BackColor = tempColor
                    'alreadyscanned
                    DisplayUserMessage(UserMessageType.InputError, dictPhrases("AlreadyScanned"))
                    Call PlayAlreadyScannedSound()
                    txtScanData.Text = ""
                    Exit Sub
                End If
            End If

            lblSerialNumberScannedValue.Text = BCSerial
            lblDateCodeScannedValue.Text = BCDate
            lblBomScannedValue.Text = BOM

            'ok so add to scanned parts
            Dim BoxQuantity As Integer = 0
            Integer.TryParse(lblBoxQty.Text, BoxQuantity)

            strResult = dbLocalSQL.AddSerialNumberToScannedParts(BCSerial, BOM, BCDate, BoxQuantity, strCurrentCartonNumber)
            If strResult <> "True" Then
                pnlBG.BackColor = Color.Red
                Panel1.BackColor = tempColor
                DisplayUserMessage(UserMessageType.ApplicationError, dictPhrases("IncorrectPartSerialNumber"))
                txtScanData.Text = ""
                Exit Sub
            End If

            'Check for full container
            dt = dbLocalSQL.LookupScannedPartsbyBOM(BOM)

            lblPartsScannedQty.Text = dt.Rows(0).Item("Rowcount")
            intCartonCount = lblPartsScannedQty.Text
            strMaxDateCode = dt.Rows(0).Item("maxDateCode")
            strMinDateCode = dt.Rows(0).Item("minDateCode")

            If Val(strMaxDateCode) = Val(strMinDateCode) Then
                strDateCode = strMinDateCode
            Else
                strDateCode = strMinDateCode & "/" & strMaxDateCode
            End If

            If Val(lblPartsScannedQty.Text) < Val(lblBoxQty.Text) And lblBoxQty.Text > 0 Then
                pnlBG.BackColor = Color.Green
                Panel1.BackColor = tempColor
                GetPackingList()
                Call ClearScanData()
                Call PlayGoodSound()
                If Parts(intCurrentProfile).PartEntryMethod = "Flatfile" And Not gblTestModeEnabled And strRepackStation <> "True" Then
                    blnResumeCheckingForFile = True
                End If
                Application.DoEvents() 'TODO:Enable timers and test logic for prepack
                Exit Sub
            Else
                PreviousBOM = ""
                'Full Box
                lblPartsScannedQty.Text = 0
                pnlBG.BackColor = Color.Green
                Panel1.BackColor = tempColor
                Call ClearScanData()
                Call PlayGoodSound()
                blnResult = CompleteBox(strCurrentCartonNumber,
                                        True,
                                        blnSendToAS400,
                                        BOM,
                                        intCartonCount,
                                        strDateCode
                                        )

                strCurrentCartonNumber = 0

                If Not blnResult Then
                    'change bg color
                    pnlBG.BackColor = Color.Red
                    Panel1.BackColor = tempColor
                Else
                    dgPackingList.DataSource = String.Empty
                End If

                UpdateLabelBank()

                If Parts(intCurrentProfile).PartEntryMethod = "Flatfile" And Not gblTestModeEnabled And strRepackStation <> "True" Then
                    blnResumeCheckingForFile = True
                End If
            End If

        Catch ex As Exception
            'Display error message
            pnlBG.BackColor = Color.Red
            Panel1.BackColor = tempColor
            DisplayUserMessage(UserMessageType.ApplicationError, ex.Message)
        End Try
    End Sub

    Sub ClearScanData()
        txtScanData.Clear()
        txtScanData.SelectAll()
    End Sub

    Sub CheckForPartials()
        Dim intQty_Local As Integer = 0
        Dim intQty_Server As Integer = 0
        Dim dtScannedPartsFromServer As DataTable = Nothing
        Dim blnPartialFound As Boolean = False
        'Dim strResponse As String

        'Test linked server prior to making lookup.  ***
        blnSqlServerRunning = dbSQLServer.TestLinkedServerConnection(strDBLOCALSQL, ConfigurationManager.AppSettings("SQLServer"))
        If blnSqlServerRunning Then
            blnPartialFound = dbSQLServer.LookupPartialByBOMandCustNo(strCurrentBOM, strCurrentCustomerNumber)
            If blnPartialFound Then
                'blnResult = MsgBox("Partial Container found on the server.  Do you want to use it?", MessageBoxButtons.YesNo, "Partial Container Found")
                frmMessageBox.Text = dictPhrases("PartialContainerFound")
                frmMessageBox.Message = dictPhrases("DoYouWantToUse")
                frmMessageBox.Style = "YesNo"
                frmMessageBox.ShowDialog()
                If frmMessageBox.FormResult = "Yes" Then
                    strCurrentCartonNumber = InputBox(dictPhrases("ScanOrTypeContainerNumber"), String.Empty)
                    If (strCurrentCartonNumber <> String.Empty) Then
                        'dtScannedPartsFromServer = dbSQLServer.GetPartialByBOMandCarton(strCurrentBOM,
                        '                                                                strCurrentCartonNumber,
                        '                                                                blnSendToAS400,
                        '                                                                Me
                        '                                                                )
                    End If
                    If dtScannedPartsFromServer IsNot Nothing AndAlso dtScannedPartsFromServer.Rows.Count > 0 Then
                        'Write Partial-box records from server to local machine
                        dbLocalSQL.MoveScannedPartsFromCarton(dtScannedPartsFromServer)
                        lblPartsScannedQty.Text = dtScannedPartsFromServer.Rows.Count
                        GetPackingList()
                    Else
                        DisplayUserMessage(UserMessageType.InputError, dictPhrases("ContainerNotFound"))
                    End If
                End If
            End If
        Else
            DisplayUserMessage(UserMessageType.DatabaseConnectionError, dictPhrases("ContainerNotFound") & vbCrLf & dictPhrases("SQLServerOffline"))
        End If

    End Sub

    Private Sub btnSavePartial_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSavePartial.Click
        Dim blnResult As Boolean = False
        Dim blnInsert As Boolean = False
        'TODO: change Val function to use tryparse AMC
        If Val(lblPartsScannedQty.Text) > 0 Then
            frmMessageBox.Text = dictPhrases("Save") 'Button Text used to populate form controls
            frmMessageBox.Message = dictPhrases("SavePartial")
            frmMessageBox.Style = "YesNo"
            frmMessageBox.ShowDialog()
            If frmMessageBox.FormResult = "Yes" Then
                If strCurrentBOM <> "" Then
                    Try
                        Call ProcessPartial(strCurrentBOM, False)
                    Catch ex As Exception
                        pnlBG.BackColor = Color.Red
                        Panel1.BackColor = tempColor
                        DisplayUserMessage(UserMessageType.ApplicationError, ex.Message)
                    End Try
                Else
                    pnlBG.BackColor = Color.Red
                    Panel1.BackColor = tempColor
                    DisplayUserMessage(UserMessageType.InputError, dictPhrases("BOM_Incorrect"))
                End If
            End If
        Else
            DisplayUserMessage(UserMessageType.ApplicationError, dictPhrases("NoPartsToPack"))
        End If
    End Sub

    Private Sub btnShipPartial_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnShipPartial.Click
        Dim blnResult As Boolean = False
        Dim blnInsert As Boolean = False

        If Val(lblPartsScannedQty.Text) > 0 Then
            frmMessageBox.Text = dictPhrases("Save")
            frmMessageBox.Message = dictPhrases("btnShipPartial")
            frmMessageBox.Style = "YesNo"
            frmMessageBox.ShowDialog()
            If frmMessageBox.FormResult = "Yes" Then
                If strCurrentBOM <> "" Then
                    Call ProcessPartial(strCurrentBOM, True)
                Else
                    pnlBG.BackColor = Color.Red
                    Panel1.BackColor = tempColor
                    DisplayUserMessage(UserMessageType.InputError, dictPhrases("BOM_Incorrect"))
                End If
            End If
        Else
            DisplayUserMessage(UserMessageType.InputError, dictPhrases("NoPartsToPack"))
        End If
    End Sub

    Private Sub ProcessPartial(ByVal BOM As String, ByVal blnCompletedBox As Boolean)
        Dim dt As DataTable
        Dim blnResult As Boolean = False

        dt = dbLocalSQL.LookupScannedPartsbyBOM(BOM)
        If dt.Rows.Count = 0 Then
            pnlBG.BackColor = Color.Red
            Panel1.BackColor = tempColor
            DisplayUserMessage(UserMessageType.InputError, dictPhrases("PartNotFound"))
        Else
            blnResult = CompleteBox(strCurrentCartonNumber,
                                    blnCompletedBox,
                                    blnSendToAS400,
                                    BOM,
                                    Val(Me.lblPartsScannedQty.Text),
                                    strDateCode
                                    )
            If blnResult Then
                PreviousBOM = ""
                strCurrentCartonNumber = "0"
                lblPartsScannedQty.Text = "0"
                pnlBG.BackColor = Color.Green
                Panel1.BackColor = tempColor
                dgPackingList.DataSource = String.Empty
                UpdateLabelBank()
                'TODO: Check this functionality 02MAY2016 AMC 
                If Parts(intCurrentProfile).PartEntryMethod = "Flatfile" And Not gblTestModeEnabled And strRepackStation <> "True" Then
                    blnResumeCheckingForFile = True
                End If
            End If
        End If

    End Sub

    Private Sub GetPackingList()
        Try
            Me.dgPackingList.DataSource = dbLocalSQL.GetScannedPartList()
            Me.dgPackingList.Columns(0).Width = 50
            Me.dgPackingList.Columns(1).Width = 155
            Me.dgPackingList.Columns(2).Width = 140

        Catch ex As Exception
            DisplayUserMessage(UserMessageType.ApplicationError, ex.Message)
        End Try
    End Sub

    Private Sub cboStandardBoxQty_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStandardBoxQty.SelectedIndexChanged
        Dim strResponse As String
        Dim intBoxQty As Integer

        If (Me.cboStandardBoxQty.Items.Count > 1) Then
            If (Me.cboStandardBoxQty.SelectedValue <> "") Then
                If Val(Me.cboStandardBoxQty.SelectedValue) >= Val(Me.lblPartsScannedQty.Text) Then
                    Me.lblBoxQty.Text = Me.cboStandardBoxQty.SelectedValue
                Else
                    DisplayUserMessage(UserMessageType.InputError, dictPhrases("StandardContainerQuantityExceeded"))
                End If
            Else
                strResponse = InputBox(dictPhrases("TypeNumberOfPartsToPack"), dictPhrases("lblStdBoxQty"), "1")
                intBoxQty = Val(strResponse)   'TODO: change to tryparse
                If intBoxQty >= Val(Me.lblPartsScannedQty.Text) Then
                    Me.lblBoxQty.Text = strResponse
                Else
                    DisplayUserMessage(UserMessageType.InputError, dictPhrases("StandardContainerQuantityExceeded"))
                End If
            End If
        Else
            DisplayUserMessage(UserMessageType.ApplicationError, dictPhrases("ErrorLoadingStandardContainerQuantity"))
        End If

        blnPackQtyLoaded = True
        txtScanData.Focus()
    End Sub

    Private Function ValidateBoxWeight() As Boolean
        Dim blnFunctionResult As Boolean = False
        'Dim dtBoxWeightInformation = dbLocalSQL.GetScannedPartsForBOM(
        Return blnFunctionResult
    End Function

    Public Function LoadBoxWeightInformation() As Boolean
        Dim blnFunctionResult As Boolean = False
        'SecurityIDType()
        Return blnFunctionResult
    End Function

    Private Sub frmPackWithScale_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If Val(Me.lblPartsScannedQty.Text) > 0 Then
            If (MsgBox("Partial Carton found.  Do you want to save it?", MessageBoxButtons.YesNo, "Partial Carton Found") = Windows.Forms.DialogResult.Yes) Then
                e.Cancel = True
            End If
        End If
    End Sub

End Class