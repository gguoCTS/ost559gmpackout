Imports System.Data.SqlClient

Public Class modDBLocalSQL
    Private mConnection As SqlConnection


    Function GetBOMNumberFromCustomerPartNumber(ByRef PartNum As String) As String

        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dbDataTable As New DataTable
        Dim strData As String

        'strSQLQuery = "SELECT Part_Number FROM LabelDefn "
        'strSQLQuery = strSQLQuery & "WHERE Company = " & CTSSite & " "
        'strSQLQuery = strSQLQuery & "AND Cust_Part = '" & PartNum & "' "
        'strSQLQuery = strSQLQuery & "AND Cust_No = " & CustomerNumber

        'cmd.CommandText = strSQLQuery

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.LookupPartNumber", Connection)
            dbcommand.CommandTimeout = 60
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Company", Parts(intCurrentProfile).CTSSite))
            dbcommand.Parameters.Add(New SqlParameter("@Cust_Part", PartNum))


            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If


            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

            strData = dbDataTable.Rows(0).Item(0).ToString

            Return strData

        Catch ex As SqlException
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return Nothing
        Catch ex As Exception
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return Nothing

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try

    End Function


    Function GetBOMNumberFromCTSPartNumber(ByRef PartNum As String) As String

        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dbDataTable As New DataTable
        Dim strData As String

        'strSQLQuery = "SELECT Part_Number FROM LabelDefn "
        'strSQLQuery = strSQLQuery & "WHERE Company = " & CTSSite & " "
        'strSQLQuery = strSQLQuery & "AND Cust_Part = '" & PartNum & "' "
        'strSQLQuery = strSQLQuery & "AND Cust_No = " & CustomerNumber

        'cmd.CommandText = strSQLQuery

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.LookupPartNumberFromCTSPartNumber", Connection)
            dbcommand.CommandTimeout = 60
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Company", Parts(intCurrentProfile).CTSSite))
            dbcommand.Parameters.Add(New SqlParameter("@Cust_Part", PartNum))


            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If


            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

            strData = dbDataTable.Rows(0).Item(0).ToString

            Return strData

        Catch ex As SqlException
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return Nothing
        Catch ex As Exception
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return Nothing

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try

    End Function


    Function GetNumberOfLabelsInBank500() As String
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dbDataTable As New DataTable
        Dim strData As String

        'strSQLQuery = "SELECT Part_Number FROM LabelDefn "
        'strSQLQuery = strSQLQuery & "WHERE Company = " & CTSSite & " "
        'strSQLQuery = strSQLQuery & "AND Cust_Part = '" & PartNum & "' "
        'strSQLQuery = strSQLQuery & "AND Cust_No = " & CustomerNumber

        'cmd.CommandText = strSQLQuery

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.GetNumberOfLabelsInBank500", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

            strData = dbDataTable.Rows(0).Item(0).ToString
            Return strData

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try
    End Function


    'AMC 14 AUG 2016  This replaces methods that manage Bank500. Mehods replaced are:
    ' local machine: AddBoxesToBANK500 
    ' server: dbSQLServer.GetLastCartonNumber
    ' server: dbSQLServer.UpdateLastCartonNumber(strNewData)
    Function AddBoxesToBANK500() As Boolean
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name
        Dim dbcommand As SqlCommand = Nothing
        Dim blnTaskComplete As Boolean = False

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.pspPOA_GetBank500LabelsFromServer", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbcommand.ExecuteNonQuery()
            blnTaskComplete = True

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try
        Return blnTaskComplete
    End Function


    Function LookupSerialNumberinScannedParts(ByRef SerialNumber As String) As String
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name
        'output 'Found' or 'Not Found'
        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dbDataTable As New DataTable
        Dim strData As String = String.Empty

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.LookupSerialNumberScanned", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Serial", SerialNumber))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

            If dbDataTable.Rows.Count > 0 Then
                Return "Found"
            Else
                Return "Not Found"
            End If

            Return strData

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try

    End Function


    Function AddSerialNumberToScannedParts(ByRef SerialNumber As String,
                                           ByRef BOM As String,
                                           ByRef DateCode As String,
                                           ByVal BoxQuantity As Integer,
                                           ByVal CartonNumber As String) As Boolean
        'output 'True ' for OK insert or 'False' for issue
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dbDataTable As New DataTable

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.InsertSerialNumberScanned", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Serial", SerialNumber))
            dbcommand.Parameters.Add(New SqlParameter("@Bom", BOM))
            dbcommand.Parameters.Add(New SqlParameter("@DateCode", DateCode))
            dbcommand.Parameters.Add(New SqlParameter("@DateScanned", Now))
            dbcommand.Parameters.Add(New SqlParameter("@Cust_No", strCurrentCustomerNumber))
            dbcommand.Parameters.Add(New SqlParameter("@boxQuantity", BoxQuantity))
            dbcommand.Parameters.Add(New SqlParameter("@cartonNumber", CartonNumber))
            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

            Return True

        Catch ex As SqlException
            Throw New Exception(ex.Message.ToString())

        Catch ex As Exception
            Throw New Exception(ex.Message.ToString())

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try
    End Function


    Function LookupScannedPartsbyBOM(ByRef BOM As String) As DataTable
        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dbDataTable As New DataTable

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.LookupBOMNumberScanned", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Bom", BOM))
            dbcommand.Parameters.Add(New SqlParameter("@Cust_No", strCurrentCustomerNumber))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If


            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

        Catch ex As SqlException
            Throw New Exception(ex.Message)

        Catch ex As Exception
            Throw New Exception(ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try

        Return dbDataTable

    End Function


    Function LookupPartialByBOMandCustNo(BOM As String, strCurrentCustomerNumber As String) As DataTable

        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dbDataTable As New DataTable

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.pspPOA_LookupPartialByBOMandCustNo", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Bom", BOM))
            dbcommand.Parameters.Add(New SqlParameter("@Cust_No", strCurrentCustomerNumber))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

        Catch ex As SqlException
            Throw New Exception(ex.Message)

        Catch ex As Exception
            Throw New Exception(ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try

        Return dbDataTable



    End Function


    Function LookupBoxFormat(ByRef BOM As String) As String
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        'output 'Found' or 'Not Found'
        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dbDataTable As New DataTable
        Dim strData As String

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.LookupBoxFormat", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Company", Parts(intCurrentProfile).CTSSite))
            dbcommand.Parameters.Add(New SqlParameter("@Bom", BOM))
            dbcommand.Parameters.Add(New SqlParameter("@Cust_No", strCurrentCustomerNumber))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

            strData = Trim(dbDataTable.Rows(0).Item("Box_Lbl_Fmt"))

            Return strData

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try

    End Function


    Function LookupNormalBoxQty(ByVal strCompany As String,
                                ByVal strCustomerNumber As String,
                                ByVal strBOM As String) As DataTable
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        'output 'Found' or 'Not Found'
        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dbDataTable As New DataTable

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.LookupNormalBoxQty", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Company", strCompany))
            dbcommand.Parameters.Add(New SqlParameter("@Cust_No", strCustomerNumber))
            dbcommand.Parameters.Add(New SqlParameter("@Bom", strBOM))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try

        Return dbDataTable
    End Function


    Function LookupSpecialBoxQty() As String
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        'output 'Found' or 'Not Found'
        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dbDataTable As New DataTable
        Dim strData As String

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.LookupSpecialBoxQty", Connection)
            dbcommand.CommandTimeout = 60
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Company", Parts(intCurrentProfile).CTSSite))
            dbcommand.Parameters.Add(New SqlParameter("@Bom", BOM))
            dbcommand.Parameters.Add(New SqlParameter("@Cust_No", strCurrentCustomerNumber))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

            strData = Trim(dbDataTable.Rows(0).Item("Std_Pack_Qty_2"))

            Return strData

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try
    End Function


    Function GetBOMList() As DataTable
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dbDataTable As New DataTable

        'ctxt = "SELECT * FROM LabelDefn "
        'ctxt = ctxt & "WHERE Company = " & CTSSite & " AND "
        'ctxt = ctxt & "Cust_No = " & Customer & " AND "
        'ctxt = ctxt & "Part_Number = '" & PartNum & "' "

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.GetBOMList", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

            Return dbDataTable

        Catch ex As SqlException
            Throw New Exception(ex.Message)

        Catch ex As Exception
            Throw New Exception(ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try
    End Function


    Function GetBOMListByProfile(ByVal strCTS_Site As String, ByVal gStrCurrentProfileName As String) As DataTable
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dbDataTable As New DataTable

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.GetBOMListByProfile", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@site", strCTS_Site))
            dbcommand.Parameters.Add(New SqlParameter("@profilename", gStrCurrentProfileName))


            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try

        Return dbDataTable

    End Function


    Function GetLabelData() As DataTable
        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dbDataTable As New DataTable

        'ctxt = "SELECT * FROM LabelDefn "
        'ctxt = ctxt & "WHERE Company = " & CTSSite & " AND "
        'ctxt = ctxt & "Cust_No = " & Customer & " AND "
        'ctxt = ctxt & "Part_Number = '" & PartNum & "' "

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.GetLabelData", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If


            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)


            Return dbDataTable

        Catch ex As SqlException
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return Nothing
        Catch ex As Exception
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return Nothing

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try

    End Function


    Function GetTopSerialNumber() As String
        Dim dbcommand As SqlCommand = Nothing
        Dim strSerial As String = Nothing

        'ctxt = "select top 1 serial from BANK500"

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.GetTopSerialNumber", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            strSerial = dbcommand.ExecuteScalar

        Catch ex As SqlException
            Throw New Exception(ex.Message)

        Catch ex As Exception
            Throw New Exception(ex.Message)
        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try

        Return strSerial
    End Function


    Function UpdateBOMList(ServerName As String,
                           ServerDatabaseName As String) As Boolean
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name
        Dim dbcommand As SqlCommand = Nothing
        Dim taskComplete As Boolean = False

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.UpdTestStationBOMLookup", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@ServerName", ServerName))
            dbcommand.Parameters.Add(New SqlParameter("@serverDatabaseName", ServerDatabaseName))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbcommand.ExecuteNonQuery()
            taskComplete = True

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try
        Return taskComplete
    End Function


    Function UpdateSystemProfiles(ServerName As String,
                                  ServerDatabaseName As String) As Boolean
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name
        Dim dbcommand As SqlCommand = Nothing
        Dim taskComplete As Boolean = False

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.UpdTestStationSystemProfiles", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@ServerName", ServerName))
            dbcommand.Parameters.Add(New SqlParameter("@serverDatabaseName", ServerDatabaseName))
            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbcommand.ExecuteNonQuery()
            taskComplete = True

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try
        Return taskComplete
    End Function


    Function UpdateLabelData(ServerName As String,
                             ServerDatabaseName As String) As Boolean
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name
        Dim dbcommand As SqlCommand = Nothing
        Dim taskComplete As Boolean = False

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.UpdTestStationLabelDefinitions", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@ServerName", ServerName))
            dbcommand.Parameters.Add(New SqlParameter("@serverDatabaseName", ServerDatabaseName))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbcommand.ExecuteNonQuery()

            taskComplete = True

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try
        Return taskComplete
    End Function


    Function GetSystemProfiles(ByRef dt As DataTable) As Boolean
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name
        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim taskComplete As Boolean = False

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.GetSystemProfiles", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dt)

            taskComplete = True

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try
        Return taskComplete
    End Function


    Function ClearBoxSerialFromQueue(ByVal Serial As String) As Boolean
        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.DeleteCartonSerialNumberFromBank500", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Serial", Serial))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbcommand.ExecuteNonQuery()

            Return True

        Catch ex As SqlException
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Catch ex As Exception
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try


    End Function


    Function GetScannedPartsForBOM(ByRef BOM As String, ByRef Dt As DataTable) As Boolean
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim taskComplete As Boolean = False

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.GetScannedPartsForBOM", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@BOM", BOM))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(Dt)

            taskComplete = True

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try

        Return taskComplete
    End Function


    Function MoveScannedPartsFromCarton(ByRef Dt As DataTable) As Boolean
        'insert datatable into local database
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable
        Dim blnTaskComplete As Boolean = False

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.MoveScannedPartsFromCarton", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.AddWithValue("@tblScannedParts", Dt)

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbcommand.ExecuteNonQuery()


            blnTaskComplete = True

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try

        Return blnTaskComplete

    End Function


    Function SQlServerInsertCompletedCarton(ByVal Company As String,
                                            ByVal strSerial As String,
                                            ByVal BOM As String,
                                            ByVal Cust_No As String,
                                            ByVal CustPartNum As String,
                                            ByVal Qty As String,
                                            ByVal strDateCode As String,
                                            ByVal PackTime As Date,
                                            ByVal BoxLabelFormat As String,
                                            ByVal SendToAS400 As Boolean,
                                            ByVal dtScannedParts As DataTable
                                           ) As Boolean
        'insert carton into carton history
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable
        Dim taskComplete As Boolean = False

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.SQLServerInsertFullCarton", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Company", Company))
            dbcommand.Parameters.Add(New SqlParameter("@Serial", strSerial))
            dbcommand.Parameters.Add(New SqlParameter("@BOM", BOM))
            dbcommand.Parameters.Add(New SqlParameter("@CustPartNum", CustPartNum))
            dbcommand.Parameters.Add(New SqlParameter("@BoxQty", Qty))
            dbcommand.Parameters.Add(New SqlParameter("@DateCode", strDateCode))
            dbcommand.Parameters.Add(New SqlParameter("@PackDate", PackTime))
            dbcommand.Parameters.Add(New SqlParameter("@BoxFormat", BoxLabelFormat))
            dbcommand.Parameters.Add(New SqlParameter("@CustNum", Cust_No))
            dbcommand.Parameters.Add(New SqlParameter("@sendToAS400", SendToAS400))
            dbcommand.Parameters.Add(New SqlParameter("@tblScannedParts", dtScannedParts))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbcommand.ExecuteNonQuery()

            taskComplete = True

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try

        Return taskComplete

    End Function


    Function SqlServerInsertPartialCarton(ByVal strSerial As String,
    ByVal BOM As String, ByVal Cust_No As String, ByVal CustPartNum As String, ByVal Qty As String,
    ByVal strDateCode As String, ByVal PackTime As Date, ByVal BoxLabelFormat As String,
    ByVal dtScannedParts As DataTable) As Boolean

        'insert carton into carton history
        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable
        Dim taskComplete As Boolean = False

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.sqlserverInsertPartialCarton", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Serial", strSerial))
            dbcommand.Parameters.Add(New SqlParameter("@BOM", BOM))
            dbcommand.Parameters.Add(New SqlParameter("@CustPartNum", CustPartNum))
            dbcommand.Parameters.Add(New SqlParameter("@BoxQty", Qty))
            dbcommand.Parameters.Add(New SqlParameter("@DateCode", strDateCode))
            dbcommand.Parameters.Add(New SqlParameter("@PackDate", PackTime))
            dbcommand.Parameters.Add(New SqlParameter("@BoxFormat", BoxLabelFormat))
            dbcommand.Parameters.Add(New SqlParameter("@CustNum", Cust_No))
            dbcommand.Parameters.Add(New SqlParameter("@tblScannedParts", dtScannedParts))

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbcommand.ExecuteNonQuery()

            taskComplete = True

        Catch ex As SqlException
            Throw New Exception(ex.Message)

        Catch ex As Exception
            Throw New Exception(ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try

        Return taskComplete
    End Function


    'Function SqlServerMoveScannedPartsInCarton(ByRef Dt As DataTable) As Boolean
    '    'Get scanned parts out of local scanned table and move to server with box number
    '    Dim dbcommand As SqlCommand = Nothing
    '    Dim dbDataTable As New DataTable
    '    Dim taskComplete As Boolean = False

    '    Try
    '        Connection = New SqlConnection(strDBLOCALSQL)
    '        dbcommand = New SqlCommand("dbo.SqlServerMoveScannedPartsInCarton", Connection)
    '        dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
    '        dbcommand.Parameters.Add(New SqlParameter("@tblScannedParts", Dt))
    '        'dbcommand.Parameters.Add(New SqlParameter("@CustNum", Customer_No))
    '        'dbcommand.Parameters.Add(New SqlParameter("@CartonSerialNumber", CartonSerial))

    '        If dbcommand.Connection.State <> ConnectionState.Open Then
    '            dbcommand.Connection.Open()
    '        End If

    '        dbcommand.ExecuteNonQuery()
    '        taskComplete = True

    '    Catch ex As SqlException
    '        Throw New Exception(ex.Message)

    '    Catch ex As Exception
    '        Throw New Exception(ex.Message)

    '    Finally
    '        If Not dbcommand Is Nothing Then
    '            If dbcommand.Connection.State <> ConnectionState.Closed Then
    '                dbcommand.Connection.Close()
    '            End If
    '        End If

    '    End Try

    '    Return taskComplete
    'End Function


    Function GetSQLServerCartonLimboData(ByRef Dt As DataTable) As Boolean
        'Get scanned parts out of local scanned table and move to server with box number
        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable

        Dim dbAdpt As SqlDataAdapter

        'ctxt = "UPDATE ScndPart SET CustNum = '" & Customer & "', "
        'ctxt = ctxt & "CtnSerNum ='" & xCartonSerialNum & "' "
        'ctxt = ctxt & "WHERE BOM = '" & xBOM & "'"
        'ctxt = ctxt & "AND CtnSerNum = '0'"

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.SQLServerGetCartonLimboData", Connection)
            dbcommand.CommandTimeout = 60
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use


            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)

            dbAdpt.Fill(Dt)

            Return True

        Catch ex As SqlException
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Catch ex As Exception
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try


    End Function


    Function ClearSQLServerCartonLimboData() As Boolean
        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable
        Dim intRowCount As Int32

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.SqlServerClearCartonLimbo", Connection)
            dbcommand.CommandTimeout = 60
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            intRowCount = dbcommand.ExecuteNonQuery()

            Return intRowCount

        Catch ex As SqlException
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Catch ex As Exception
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try
    End Function


    Function GetSQLServerCartonHistoryData(ByRef Dt As DataTable) As Boolean
        'Get scanned parts out of local scanned table and move to server with box number
        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable

        Dim dbAdpt As SqlDataAdapter

        'ctxt = "UPDATE ScndPart SET CustNum = '" & Customer & "', "
        'ctxt = ctxt & "CtnSerNum ='" & xCartonSerialNum & "' "
        'ctxt = ctxt & "WHERE BOM = '" & xBOM & "'"
        'ctxt = ctxt & "AND CtnSerNum = '0'"

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.SQLServerGetCartonHistoryData", Connection)
            dbcommand.CommandTimeout = 60
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use


            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(Dt)

            Return True

        Catch ex As SqlException
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Catch ex As Exception
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try


    End Function


    Function ClearSQLServerCartonHistoryData() As Boolean
        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable
        Dim intRowCount As Int32

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.SqlServerClearCartonHistory", Connection)
            dbcommand.CommandTimeout = 60
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            intRowCount = dbcommand.ExecuteNonQuery()

            Return intRowCount

        Catch ex As SqlException
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Catch ex As Exception
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try
    End Function


    Function GetSQLServerScannedParts(ByRef Dt As DataTable) As Boolean
        'Get scanned parts out of local scanned table and move to server with box number
        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable

        Dim dbAdpt As SqlDataAdapter

        'ctxt = "UPDATE ScndPart SET CustNum = '" & Customer & "', "
        'ctxt = ctxt & "CtnSerNum ='" & xCartonSerialNum & "' "
        'ctxt = ctxt & "WHERE BOM = '" & xBOM & "'"
        'ctxt = ctxt & "AND CtnSerNum = '0'"

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.SQLServerGetScannedParts", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use


            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(Dt)

            Return True

        Catch ex As SqlException
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Catch ex As Exception
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try


    End Function


    Function ClearSQLServerScannedParts() As Boolean
        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable
        Dim blntaskComplete As Boolean = False

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.SqlServerClearScannedParts", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbcommand.ExecuteNonQuery()
            blntaskComplete = True

        Catch ex As SqlException
            Throw New Exception(ex.Message)

        Catch ex As Exception
            Throw New Exception(ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try

        Return blntaskComplete
    End Function


    'Added function 27 APR 2016  AMC
    Function DeleteScannedParts() As Boolean
        Dim dbcommand As SqlCommand = Nothing
        Dim blntaskComplete As Boolean = False

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.pspPOA_DeleteScannedParts", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbcommand.ExecuteNonQuery()
            blntaskComplete = True

        Catch ex As SqlException
            Throw New Exception(ex.Message)

        Catch ex As Exception
            Throw New Exception(ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try

        Return blntaskComplete

    End Function

    'Added function 05 MAY 2016  AMC
    Function GetLabelDataByPartNumber(ByVal Company As Short,
                                  ByVal PartNumber As String,
                                  ByVal strCurrentCustomerNumber As String) As DataTable

        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dbDataTable As New DataTable


        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.GetLabelDataByPartNum", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            dbcommand.Parameters.Add(New SqlParameter("@Company", Company))
            dbcommand.Parameters.Add(New SqlParameter("@PartNum", PartNumber))
            dbcommand.Parameters.Add(New SqlParameter("@CustNum", strCurrentCustomerNumber))
            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)


        Catch ex As SqlException
            Throw New Exception(ex.Message)

        Catch ex As Exception
            Throw New Exception(ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try

        Return dbDataTable
    End Function


    Function GetAS400CartonData(ByRef Dt As DataTable) As Boolean
        'Get scanned parts out of local scanned table and move to server with box number
        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable

        Dim dbAdpt As SqlDataAdapter

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.AS400GetCartonData", Connection)
            dbcommand.CommandTimeout = 60
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use


            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(Dt)

            Return True

        Catch ex As SqlException
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Catch ex As Exception
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try


    End Function


    Function ClearAS400CartonData() As Boolean
        Dim dbcommand As SqlCommand = Nothing
        Dim dbDataTable As New DataTable
        Dim intRowCount As Int32

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.AS400ClearData", Connection)
            dbcommand.CommandTimeout = 60
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            intRowCount = dbcommand.ExecuteNonQuery()

            Return intRowCount

        Catch ex As SqlException
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False
        Catch ex As Exception
            If dbcommand.Connection.State <> ConnectionState.Closed Then
                dbcommand.Connection.Close()
            End If
            ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
            Return False

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If

        End Try
    End Function


    Public Property Connection() As SqlConnection
        Get
            Return mConnection
        End Get
        Set(ByVal value As SqlConnection)
            mConnection = value
        End Set
    End Property


    Function GetScannedPartList() As DataTable
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name
        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dbDataTable As New DataTable

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.pspPOA_GetScannedPartList", Connection)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dbDataTable)

        Catch ex As SqlException
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Catch ex As Exception
            Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try

        Return dbDataTable
    End Function


    Function GetLanguagePhrases(intLanguageID As Integer) As DataTable
        Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name

        Dim dbcommand As SqlCommand = Nothing
        Dim dbAdpt As SqlDataAdapter
        Dim dt As New DataTable

        Try
            Connection = New SqlConnection(strDBLOCALSQL)
            dbcommand = New SqlCommand("dbo.pspPOA_LkpLanguagePhrases", Connection)
            dbcommand.Parameters.AddWithValue("@languageID", intLanguageID)
            dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use

            If dbcommand.Connection.State <> ConnectionState.Open Then
                dbcommand.Connection.Open()
            End If

            dbAdpt = New SqlDataAdapter(dbcommand)
            dbAdpt.Fill(dt)

        Catch ex As SqlException
            Throw New Exception(ex.Message)

        Catch ex As Exception
            Throw New Exception(ex.Message)

        Finally
            If Not dbcommand Is Nothing Then
                If dbcommand.Connection.State <> ConnectionState.Closed Then
                    dbcommand.Connection.Close()
                End If
            End If
        End Try

        Return dt
    End Function
End Class

'*Removed Function; combined with Function SqlServerMoveScannedPartsInCarton
'The deletion is performed in SQL server under a transaction.  21APR2016 AMC
'Function ClearScannedPartsForBOMAndCustomerPartNum(ByRef BOM As String, ByRef Cust_Num As String) As Boolean

'    Dim dbcommand As SqlCommand = Nothing




'    Try
'        Connection = New SqlConnection(strDBLOCALSQL)
'        dbcommand = New SqlCommand("dbo.ClearScannedPartsForBOM", Connection)
'        dbcommand.CommandTimeout = 60
'        dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
'        dbcommand.Parameters.Add(New SqlParameter("@BOM", BOM))
'        dbcommand.Parameters.Add(New SqlParameter("@Cust_no", Cust_Num))

'        If dbcommand.Connection.State <> ConnectionState.Open Then
'            dbcommand.Connection.Open()
'        End If



'        dbcommand.ExecuteNonQuery()

'        Return True

'    Catch ex As SqlException
'        If dbcommand.Connection.State <> ConnectionState.Closed Then
'            dbcommand.Connection.Close()
'        End If
'        '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
'        Return False
'    Catch ex As Exception
'        If dbcommand.Connection.State <> ConnectionState.Closed Then
'            dbcommand.Connection.Close()
'        End If
'        ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
'        Return False

'    Finally
'        If Not dbcommand Is Nothing Then
'            If dbcommand.Connection.State <> ConnectionState.Closed Then
'                dbcommand.Connection.Close()
'            End If
'        End If

'    End Try

'End Function

'Removed function call 20APR2016  AMC
'Function SqlServerMarkFullCartonAsOnAS400(ByVal carton As String) As Boolean
'    Dim dbcommand As SqlCommand = Nothing
'    Dim dbDataTable As New DataTable
'    Dim intRowCount As Int32

'    Try
'        Connection = New SqlConnection(strDBLOCALSQL)
'        dbcommand = New SqlCommand("dbo.SqlServerMarkFullCartonAsOnAS400", Connection)
'        dbcommand.CommandTimeout = 60
'        dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
'        dbcommand.Parameters.Add(New SqlParameter("@Serial", carton))

'        If dbcommand.Connection.State <> ConnectionState.Open Then
'            dbcommand.Connection.Open()
'        End If

'        intRowCount = dbcommand.ExecuteNonQuery()

'        Return intRowCount

'    Catch ex As SqlException
'        If dbcommand.Connection.State <> ConnectionState.Closed Then
'            dbcommand.Connection.Close()
'        End If
'        '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
'        Return False
'    Catch ex As Exception
'        If dbcommand.Connection.State <> ConnectionState.Closed Then
'            dbcommand.Connection.Close()
'        End If
'        ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
'        Return False

'    Finally
'        If Not dbcommand Is Nothing Then
'            If dbcommand.Connection.State <> ConnectionState.Closed Then
'                dbcommand.Connection.Close()
'            End If
'        End If

'    End Try
'End Function

'*Removed Function; combined with Function SQlServerInsertCompletedCarton
'The insert is performed in SQL server under a transaction.  21APR2016 AMC
'Function AS400SendCompletedCartonToAS400(ByVal CTSSite As String, ByVal strSerial As String,
'        ByVal BOM As String, ByVal CustPartNum As String, ByVal Qty As String,
'        ByVal strDateCode As String, ByVal PackTime As Date, ByVal BoxLabelFormat As String,
'        ByVal Cust_No As String) As Boolean

'    Dim dbcommand As SqlCommand = Nothing
'    Dim dbDataTable As New DataTable
'    Dim sendComplete As Boolean = False

'    Try
'        Connection = New SqlConnection(strDBLOCALSQL)
'        dbcommand = New SqlCommand("dbo.AS400SendCompletedCartonToAS400", Connection)
'        dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
'        dbcommand.Parameters.Add(New SqlParameter("@Company", CTSSite))
'        dbcommand.Parameters.Add(New SqlParameter("@Plant", 0))
'        dbcommand.Parameters.Add(New SqlParameter("@Serial", strSerial))
'        dbcommand.Parameters.Add(New SqlParameter("@Part_Number", BOM))
'        dbcommand.Parameters.Add(New SqlParameter("@Cust_No", Cust_No))
'        dbcommand.Parameters.Add(New SqlParameter("@Cust_Part", CustPartNum))
'        dbcommand.Parameters.Add(New SqlParameter("@Ctn_Qty", Qty))
'        dbcommand.Parameters.Add(New SqlParameter("@Date_Code", strDateCode))
'        dbcommand.Parameters.Add(New SqlParameter("@Ctn_Status", "P"))
'        dbcommand.Parameters.Add(New SqlParameter("@Upload_Flg", 0))
'        dbcommand.Parameters.Add(New SqlParameter("@Box_Lbl_Fmt", BoxLabelFormat))

'        If dbcommand.Connection.State <> ConnectionState.Open Then
'            dbcommand.Connection.Open()
'        End If

'        dbcommand.ExecuteNonQuery()

'        sendComplete = True

'    Catch ex As SqlException
'        Throw New Exception(ex.Message)

'    Catch ex As Exception
'        Throw New Exception(ex.Message)

'    Finally
'        If Connection.State <> ConnectionState.Closed Then
'            Connection.Close()
'        End If
'    End Try
'    Return sendComplete
'End Function

'Function LookupBoxDatabyScannedPart(ByRef SerialNumber As String) As DataTable
'    Dim dbcommand As SqlCommand = Nothing
'    Dim dbAdpt As SqlDataAdapter
'    Dim dbDataTable As New DataTable

'    ' ctxt = "SELECT COUNT(1) AS CC, MAX(DateCode) AS MAXDC, "
'    'ctxt = ctxt & "MIN(DateCode) AS MINDC FROM ScndPart "
'    'ctxt = ctxt & "WHERE BOM = '" & PartNum & "' "
'    'ctxt = ctxt & "AND CustNum = '0'"

'    Try
'        Connection = New SqlConnection(strDBLOCALSQL)
'        dbcommand = New SqlCommand("dbo.LookupBoxDatabyScannedPart", Connection)
'        dbcommand.CommandTimeout = 60
'        dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
'        dbcommand.Parameters.Add(New SqlParameter("@SerialNumber", SerialNumber))

'        If dbcommand.Connection.State <> ConnectionState.Open Then
'            dbcommand.Connection.Open()
'        End If


'        dbAdpt = New SqlDataAdapter(dbcommand)
'        dbAdpt.Fill(dbDataTable)


'        Return dbDataTable

'    Catch ex As SqlException
'        If dbcommand.Connection.State <> ConnectionState.Closed Then
'            dbcommand.Connection.Close()
'        End If
'        '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
'        Return Nothing
'    Catch ex As Exception
'        If dbcommand.Connection.State <> ConnectionState.Closed Then
'            dbcommand.Connection.Close()
'        End If
'        ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
'        Return Nothing

'    Finally
'        If Not dbcommand Is Nothing Then
'            If dbcommand.Connection.State <> ConnectionState.Closed Then
'                dbcommand.Connection.Close()
'            End If
'        End If

'    End Try


'End Function

'Function GetLabelDataByPartNumber(ByVal Company As Short,
'                                  ByVal PartNumber As String,
'                                  ByVal strCurrentCustomerNumber As String) As DataTable

'    Dim dbcommand As SqlCommand = Nothing
'    Dim dbAdpt As SqlDataAdapter
'    Dim dbDataTable As New DataTable

'    'ctxt = "SELECT * FROM LabelDefn "
'    'ctxt = ctxt & "WHERE Company = " & CTSSite & " AND "
'    'ctxt = ctxt & "Cust_No = " & Customer & " AND "
'    'ctxt = ctxt & "Part_Number = '" & PartNum & "' "

'    Try
'        Connection = New SqlConnection(strDBLOCALSQL)
'        dbcommand = New SqlCommand("dbo.GetLabelDataByPartNum", Connection)
'        dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
'        dbcommand.Parameters.Add(New SqlParameter("@Company", Company))
'        dbcommand.Parameters.Add(New SqlParameter("@PartNum", PartNumber))
'        dbcommand.Parameters.Add(New SqlParameter("@CustNum", strCurrentCustomerNumber))
'        If dbcommand.Connection.State <> ConnectionState.Open Then
'            dbcommand.Connection.Open()
'        End If

'        dbAdpt = New SqlDataAdapter(dbcommand)
'        dbAdpt.Fill(dbDataTable)


'    Catch ex As SqlException
'        Throw New Exception(ex.Message)

'    Catch ex As Exception
'        Throw New Exception(ex.Message)

'    Finally
'        If Not dbcommand Is Nothing Then
'            If dbcommand.Connection.State <> ConnectionState.Closed Then
'                dbcommand.Connection.Close()
'            End If
'        End If
'    End Try

'    Return dbDataTable
'End Function

'Replaced Function 20APR2016 AMC
'Function GetTopSerialNumber() As String

'    Dim dbcommand As SqlCommand = Nothing
'    Dim dbAdpt As SqlDataAdapter
'    Dim dbDataTable As New DataTable
'    Dim strSerial As String

'    'ctxt = "select top 1 serial from BANK500"

'    Try
'        Connection = New SqlConnection(strDBLOCALSQL)
'        dbcommand = New SqlCommand("dbo.GetTopSerialNumber", Connection)
'        dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use

'        If dbcommand.Connection.State <> ConnectionState.Open Then
'            dbcommand.Connection.Open()
'        End If


'        dbAdpt = New SqlDataAdapter(dbcommand)
'        dbAdpt.Fill(dbDataTable)

'        strSerial = dbDataTable.Rows(0).Item("Serial")

'        Return strSerial

'    Catch ex As SqlException
'        If dbcommand.Connection.State <> ConnectionState.Closed Then
'            dbcommand.Connection.Close()
'        End If
'        '  Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
'        Return Nothing
'    Catch ex As Exception
'        If dbcommand.Connection.State <> ConnectionState.Closed Then
'            dbcommand.Connection.Close()
'        End If
'        ' Me.mAnomaly = New Anomaly(6001, "Database", "GetExternalTestMethodsDataTable", ex.Message, Me)
'        Return Nothing

'    Finally
'        If Not dbcommand Is Nothing Then
'            If dbcommand.Connection.State <> ConnectionState.Closed Then
'                dbcommand.Connection.Close()
'            End If
'        End If

'    End Try

'End Function

'Get Next Carton Serial Number from Bank500
'Remove the 

'Function AddBoxesToBANK500(ByRef intStart As Integer, ByRef intEnd As Integer) As Boolean
'    Dim _className As String = Me.GetType().FullName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name
'    Dim dbcommand As SqlCommand = Nothing
'    Dim dbAdpt As SqlDataAdapter
'    Dim dbDataTable As New DataTable

'    Try
'        Connection = New SqlConnection(strDBLOCALSQL)
'        dbcommand = New SqlCommand("dbo.AddBoxesToBANK500", Connection)
'        dbcommand.CommandType = CommandType.StoredProcedure   ' Sets the command type to use
'        dbcommand.Parameters.Add(New SqlParameter("@StartNumber", intStart))
'        dbcommand.Parameters.Add(New SqlParameter("@QtyToAdd", intEnd))

'        If dbcommand.Connection.State <> ConnectionState.Open Then
'            dbcommand.Connection.Open()
'        End If

'        dbAdpt = New SqlDataAdapter(dbcommand)
'        dbAdpt.Fill(dbDataTable)
'        Return True

'    Catch ex As SqlException
'        Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

'    Catch ex As Exception
'        Throw New Exception("Method Name: " + _className + ".  " + ex.Message)

'    Finally
'        If Not dbcommand Is Nothing Then
'            If dbcommand.Connection.State <> ConnectionState.Closed Then
'                dbcommand.Connection.Close()
'            End If
'        End If

'    End Try
'End Function