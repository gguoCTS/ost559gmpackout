﻿Public Class frmPickLanguage
    Private mIntLanguageSelected As Integer

    Private Sub btnUSAFlag_Click(sender As Object, e As EventArgs) Handles btnUSAFlag.Click
        strLanguageUsed = "English"
        mIntLanguageSelected = 1
        Me.Hide()
    End Sub

    Private Sub btnMexFlag_Click(sender As Object, e As EventArgs) Handles btnMexFlag.Click
        strLanguageUsed = "Spanish"
        mIntLanguageSelected = 2
        Me.Hide()
    End Sub

    Private Sub btnCzechFlag_Click(sender As Object, e As EventArgs) Handles btnCzechFlag.Click
        strLanguageUsed = "Czech"
        mIntLanguageSelected = 3
        Me.Hide()
    End Sub

    Private Sub btnChineseFlag_Click(sender As Object, e As EventArgs) Handles btnChineseFlag.Click
        strLanguageUsed = "Chinese"
        mIntLanguageSelected = 4
        Me.Hide()
    End Sub

    Public Property LanguageSelected() As Integer
        Get
            Return mIntLanguageSelected
        End Get
        Set(value As Integer)
            mIntLanguageSelected = value
        End Set
    End Property
End Class