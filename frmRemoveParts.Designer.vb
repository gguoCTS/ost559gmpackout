﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRemoveParts
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRemoveParts))
        Me.btnRemoveParts_PrintLabel = New System.Windows.Forms.Button()
        Me.dgScannedParts = New System.Windows.Forms.DataGridView()
        Me.btnRemoveParts = New System.Windows.Forms.Button()
        Me.lblScanPart = New System.Windows.Forms.Label()
        Me.txtScanData = New System.Windows.Forms.TextBox()
        Me.lblScanOrTypeTheBoxNumber = New System.Windows.Forms.Label()
        Me.txtCarton = New System.Windows.Forms.TextBox()
        Me.lblCustomer = New System.Windows.Forms.Label()
        Me.lblCustomerLabel = New System.Windows.Forms.Label()
        Me.lblBOMSelected = New System.Windows.Forms.Label()
        Me.lblBOM = New System.Windows.Forms.Label()
        Me.lblOne = New System.Windows.Forms.Label()
        Me.lblTwo = New System.Windows.Forms.Label()
        Me.lblContainerCount = New System.Windows.Forms.Label()
        Me.lblContainerCountLabel = New System.Windows.Forms.Label()
        Me.lblUnpackBox = New System.Windows.Forms.Label()
        Me.btnUnpackContainer = New System.Windows.Forms.Button()
        CType(Me.dgScannedParts, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnRemoveParts_PrintLabel
        '
        Me.btnRemoveParts_PrintLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btnRemoveParts_PrintLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnRemoveParts_PrintLabel.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRemoveParts_PrintLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnRemoveParts_PrintLabel.Location = New System.Drawing.Point(147, 534)
        Me.btnRemoveParts_PrintLabel.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnRemoveParts_PrintLabel.Name = "btnRemoveParts_PrintLabel"
        Me.btnRemoveParts_PrintLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnRemoveParts_PrintLabel.Size = New System.Drawing.Size(371, 89)
        Me.btnRemoveParts_PrintLabel.TabIndex = 3
        Me.btnRemoveParts_PrintLabel.Text = "Remove Parts and Print Partial Label"
        Me.btnRemoveParts_PrintLabel.UseVisualStyleBackColor = False
        '
        'dgScannedParts
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgScannedParts.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgScannedParts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgScannedParts.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgScannedParts.Location = New System.Drawing.Point(199, 263)
        Me.dgScannedParts.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.dgScannedParts.Name = "dgScannedParts"
        Me.dgScannedParts.RowTemplate.Height = 24
        Me.dgScannedParts.Size = New System.Drawing.Size(584, 251)
        Me.dgScannedParts.TabIndex = 23
        '
        'btnRemoveParts
        '
        Me.btnRemoveParts.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.btnRemoveParts.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnRemoveParts.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRemoveParts.ForeColor = System.Drawing.SystemColors.ControlText
        Me.btnRemoveParts.Location = New System.Drawing.Point(599, 534)
        Me.btnRemoveParts.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnRemoveParts.Name = "btnRemoveParts"
        Me.btnRemoveParts.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnRemoveParts.Size = New System.Drawing.Size(384, 89)
        Me.btnRemoveParts.TabIndex = 4
        Me.btnRemoveParts.Text = "Remove Parts"
        Me.btnRemoveParts.UseVisualStyleBackColor = False
        '
        'lblScanPart
        '
        Me.lblScanPart.AutoSize = True
        Me.lblScanPart.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScanPart.Location = New System.Drawing.Point(235, 197)
        Me.lblScanPart.Name = "lblScanPart"
        Me.lblScanPart.Size = New System.Drawing.Size(187, 25)
        Me.lblScanPart.TabIndex = 28
        Me.lblScanPart.Text = "Scan part to remove"
        '
        'txtScanData
        '
        Me.txtScanData.AcceptsReturn = True
        Me.txtScanData.BackColor = System.Drawing.SystemColors.Window
        Me.txtScanData.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtScanData.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtScanData.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtScanData.Location = New System.Drawing.Point(199, 226)
        Me.txtScanData.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtScanData.MaxLength = 0
        Me.txtScanData.Name = "txtScanData"
        Me.txtScanData.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtScanData.Size = New System.Drawing.Size(580, 26)
        Me.txtScanData.TabIndex = 2
        '
        'lblScanOrTypeTheBoxNumber
        '
        Me.lblScanOrTypeTheBoxNumber.AutoSize = True
        Me.lblScanOrTypeTheBoxNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblScanOrTypeTheBoxNumber.Location = New System.Drawing.Point(239, 11)
        Me.lblScanOrTypeTheBoxNumber.Name = "lblScanOrTypeTheBoxNumber"
        Me.lblScanOrTypeTheBoxNumber.Size = New System.Drawing.Size(148, 25)
        Me.lblScanOrTypeTheBoxNumber.TabIndex = 30
        Me.lblScanOrTypeTheBoxNumber.Text = "Scan Container"
        '
        'txtCarton
        '
        Me.txtCarton.AcceptsReturn = True
        Me.txtCarton.BackColor = System.Drawing.SystemColors.Window
        Me.txtCarton.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtCarton.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCarton.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtCarton.Location = New System.Drawing.Point(203, 41)
        Me.txtCarton.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.txtCarton.MaxLength = 0
        Me.txtCarton.Name = "txtCarton"
        Me.txtCarton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtCarton.Size = New System.Drawing.Size(580, 26)
        Me.txtCarton.TabIndex = 1
        '
        'lblCustomer
        '
        Me.lblCustomer.BackColor = System.Drawing.Color.White
        Me.lblCustomer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblCustomer.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCustomer.Location = New System.Drawing.Point(423, 106)
        Me.lblCustomer.Name = "lblCustomer"
        Me.lblCustomer.Size = New System.Drawing.Size(153, 34)
        Me.lblCustomer.TabIndex = 34
        Me.lblCustomer.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblCustomerLabel
        '
        Me.lblCustomerLabel.AutoSize = True
        Me.lblCustomerLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCustomerLabel.Location = New System.Drawing.Point(420, 81)
        Me.lblCustomerLabel.Name = "lblCustomerLabel"
        Me.lblCustomerLabel.Size = New System.Drawing.Size(97, 25)
        Me.lblCustomerLabel.TabIndex = 33
        Me.lblCustomerLabel.Text = "Customer"
        '
        'lblBOMSelected
        '
        Me.lblBOMSelected.BackColor = System.Drawing.Color.White
        Me.lblBOMSelected.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblBOMSelected.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBOMSelected.Location = New System.Drawing.Point(255, 107)
        Me.lblBOMSelected.Name = "lblBOMSelected"
        Me.lblBOMSelected.Size = New System.Drawing.Size(145, 34)
        Me.lblBOMSelected.TabIndex = 32
        Me.lblBOMSelected.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblBOM
        '
        Me.lblBOM.AutoSize = True
        Me.lblBOM.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblBOM.Location = New System.Drawing.Point(255, 81)
        Me.lblBOM.Name = "lblBOM"
        Me.lblBOM.Size = New System.Drawing.Size(58, 25)
        Me.lblBOM.TabIndex = 31
        Me.lblBOM.Text = "BOM"
        '
        'lblOne
        '
        Me.lblOne.AutoSize = True
        Me.lblOne.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOne.Location = New System.Drawing.Point(197, 11)
        Me.lblOne.Name = "lblOne"
        Me.lblOne.Size = New System.Drawing.Size(41, 25)
        Me.lblOne.TabIndex = 37
        Me.lblOne.Text = "#1)"
        '
        'lblTwo
        '
        Me.lblTwo.AutoSize = True
        Me.lblTwo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTwo.Location = New System.Drawing.Point(191, 197)
        Me.lblTwo.Name = "lblTwo"
        Me.lblTwo.Size = New System.Drawing.Size(41, 25)
        Me.lblTwo.TabIndex = 38
        Me.lblTwo.Text = "#2)"
        '
        'lblContainerCount
        '
        Me.lblContainerCount.BackColor = System.Drawing.Color.White
        Me.lblContainerCount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblContainerCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContainerCount.Location = New System.Drawing.Point(625, 106)
        Me.lblContainerCount.Name = "lblContainerCount"
        Me.lblContainerCount.Size = New System.Drawing.Size(129, 34)
        Me.lblContainerCount.TabIndex = 40
        Me.lblContainerCount.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblContainerCountLabel
        '
        Me.lblContainerCountLabel.AutoSize = True
        Me.lblContainerCountLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblContainerCountLabel.Location = New System.Drawing.Point(619, 81)
        Me.lblContainerCountLabel.Name = "lblContainerCountLabel"
        Me.lblContainerCountLabel.Size = New System.Drawing.Size(155, 25)
        Me.lblContainerCountLabel.TabIndex = 39
        Me.lblContainerCountLabel.Text = "Container Count"
        '
        'lblUnpackBox
        '
        Me.lblUnpackBox.AutoSize = True
        Me.lblUnpackBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUnpackBox.Location = New System.Drawing.Point(856, 140)
        Me.lblUnpackBox.Name = "lblUnpackBox"
        Me.lblUnpackBox.Size = New System.Drawing.Size(169, 25)
        Me.lblUnpackBox.TabIndex = 42
        Me.lblUnpackBox.Text = "Unpack Container"
        '
        'btnUnpackContainer
        '
        Me.btnUnpackContainer.BackgroundImage = CType(resources.GetObject("btnUnpackContainer.BackgroundImage"), System.Drawing.Image)
        Me.btnUnpackContainer.Location = New System.Drawing.Point(889, 53)
        Me.btnUnpackContainer.Margin = New System.Windows.Forms.Padding(3, 2, 3, 2)
        Me.btnUnpackContainer.Name = "btnUnpackContainer"
        Me.btnUnpackContainer.Size = New System.Drawing.Size(93, 85)
        Me.btnUnpackContainer.TabIndex = 43
        Me.btnUnpackContainer.UseVisualStyleBackColor = True
        '
        'frmRemoveParts
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1117, 649)
        Me.Controls.Add(Me.btnUnpackContainer)
        Me.Controls.Add(Me.lblUnpackBox)
        Me.Controls.Add(Me.lblContainerCount)
        Me.Controls.Add(Me.lblContainerCountLabel)
        Me.Controls.Add(Me.lblTwo)
        Me.Controls.Add(Me.lblOne)
        Me.Controls.Add(Me.lblCustomer)
        Me.Controls.Add(Me.lblCustomerLabel)
        Me.Controls.Add(Me.lblBOMSelected)
        Me.Controls.Add(Me.lblBOM)
        Me.Controls.Add(Me.lblScanOrTypeTheBoxNumber)
        Me.Controls.Add(Me.txtCarton)
        Me.Controls.Add(Me.lblScanPart)
        Me.Controls.Add(Me.txtScanData)
        Me.Controls.Add(Me.btnRemoveParts)
        Me.Controls.Add(Me.btnRemoveParts_PrintLabel)
        Me.Controls.Add(Me.dgScannedParts)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "frmRemoveParts"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Remove Parts From Container"
        CType(Me.dgScannedParts, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents btnRemoveParts_PrintLabel As Button
    Friend WithEvents dgScannedParts As DataGridView
    Public WithEvents btnRemoveParts As Button
    Friend WithEvents lblScanPart As System.Windows.Forms.Label
    Public WithEvents txtScanData As System.Windows.Forms.TextBox
    Friend WithEvents lblScanOrTypeTheBoxNumber As System.Windows.Forms.Label
    Public WithEvents txtCarton As System.Windows.Forms.TextBox
    Friend WithEvents lblCustomer As System.Windows.Forms.Label
    Friend WithEvents lblCustomerLabel As System.Windows.Forms.Label
    Friend WithEvents lblBOMSelected As System.Windows.Forms.Label
    Friend WithEvents lblBOM As System.Windows.Forms.Label
    Friend WithEvents lblOne As System.Windows.Forms.Label
    Friend WithEvents lblTwo As System.Windows.Forms.Label
    Friend WithEvents lblContainerCount As System.Windows.Forms.Label
    Friend WithEvents lblContainerCountLabel As System.Windows.Forms.Label
    Friend WithEvents lblUnpackBox As System.Windows.Forms.Label
    Friend WithEvents btnUnpackContainer As System.Windows.Forms.Button
End Class
